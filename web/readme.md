# KP Dashboard v2  For ***Staging Server***

## Getting started

```
git clone https://gitlab.com/tariqul/kp_dashboard_v2
cd kp_dashboard_v2
add .env file
composer install
npm install
php artisan key:generate
php artisan migrate
php artisan db:seed
php artisan passport:install
php artisan passport:client --personal
php artisan serve

npm run dev
# or
npm run watch
# or for a production build of assets
npm run prod
# see Laravel Mix documentation
```

## Deployment
ssh -i aws_adam_parker.pem ubuntu@app.kpdashboard.com
cd /var/www/kp_dashboard_v2
git pull
yarn (if new package was added)
yarn prod
composer install (if new package was added)
php artisan migrate (if new migration was added)

## Technology Stack

- Laravel 5.7
- MySQL/MariaDB
- VueJS 2.5
- Vuetify
