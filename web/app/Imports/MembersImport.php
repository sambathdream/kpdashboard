<?php

namespace App\Imports;

use App\Models\Client;
use App\Models\District;
use App\Models\Member;
use Illuminate\Validation\Rule;
use Maatwebsite\Excel\Concerns\Importable;
use Maatwebsite\Excel\Concerns\SkipsFailures;
use Maatwebsite\Excel\Concerns\SkipsOnFailure;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\WithValidation;
use Maatwebsite\Excel\Events\BeforeImport;
use Maatwebsite\Excel\Validators\Failure;

class MembersImport implements ToModel, SkipsOnFailure, WithEvents, WithHeadingRow, WithValidation
{
    use Importable, SkipsFailures;

    protected $importedCount;
    protected $client;

    public function __construct($clientId)
    {
        $this->importedCount = 0;
        $this->client = Client::find($clientId);
    }

    public function getImportedCount()
    {
        return $this->importedCount;
    }

    public static function beforeImport(BeforeImport $event)
    {
        $worksheet = $event->reader->getActiveSheet();
        $highestRow = $worksheet->getHighestRow();
        if ($highestRow < 2) {
            $error = \Illuminate\Validation\ValidationException::withMessages([]);
            $failure = new Failure(1, 'rows', [0 => 'Now enough rows!']);
            $failures = [0 => $failure];
            throw new ValidationException($error, $failures);
        }
    }

    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        $stateAbbr = $this->client->state->state_abbrev;
        $this->importedCount += 1;
        $member = Member::where('client_id', $this->client->id)
            ->where('email', $row['email'])
            ->first();
        if ($member) {
            $prefixList = ['home_', 'work_', 'work2_', 'work3_', 'work4_'];
            foreach ($prefixList as $prefix) {
                $addressKey = $prefix.'address';
                $addressLat = $prefix.'lat';
                $addressLng = $prefix.'lng';
                $senDistrictKey = $prefix.'sen_district';
                $houseDistrictKey = $prefix.'house_district';
                $conDistrictKey = $prefix.'con_district';
                $federalSenate = $prefix.'federal_senate';
                if (array_key_exists($addressKey, $row) && $row[$addressKey] && $row[$addressKey] !== $member->{$addressKey}) {
                    $response = \AppHelper::searchDistrictByAddress($row[$addressKey]);
                    if (!array_key_exists('results', $response) || !array_key_exists(0, $response['results'])) {
                        $row[$addressKey] = $member->{$addressKey};
                        continue;
                    }
                    $response = $response['results'][0];
                    if (strtolower($response['address_components']['state']) !== $stateAbbr) {
                        $row[$addressKey] = $member->{$addressKey};
                        continue;
                    }
                    $stateLegislativeDistricts = $response['fields']['state_legislative_districts'];
                    if (array_key_exists('senate', $stateLegislativeDistricts)) {
                        $length = strlen((string)$this->client->state->sen_districts);
                        $districtNumber = $stateLegislativeDistricts['senate']['district_number'];
                        $districtNumber = str_pad($districtNumber, $length, '0', STR_PAD_LEFT);
                        $row[$senDistrictKey] = 'S'.$districtNumber;
                    }
                    if (array_key_exists('house', $stateLegislativeDistricts)) {
                        $length = strlen((string)$this->client->state->house_districts);
                        $districtNumber = $stateLegislativeDistricts['house']['district_number'];
                        $districtNumber = str_pad($districtNumber, $length, '0', STR_PAD_LEFT);
                        $row[$houseDistrictKey] = 'H'.$districtNumber;
                    }
                    $row[$addressKey] = $response['formatted_address'];
                    $row[$addressLat] = $response['location']['lat'];
                    $row[$addressLng] = $response['location']['lng'];
                    
                    $congressionalDistricts = $response['fields']['congressional_districts'];
                    if (count($congressionalDistricts) > 0) {
                        $ocdId = 'ocd-division/country:us/state:'.$stateAbbr.'/cd:'.$congressionalDistricts[0]['district_number'];
                        $district = District::where('ocd_id', $ocdId)->first();
                        $row[$conDistrictKey] = $district->district;
                    }
                    $row[$federalSenate] = $response['address_components']['state'];
                } else {
                    $row[$addressKey] = $member->{$addressKey};
                }
            }
            $member->fill($row);
            return $member;
        } else {
            $prefixList = ['home_', 'work_', 'work2_', 'work3_', 'work4_'];
            foreach ($prefixList as $prefix) {
                $addressKey = $prefix.'address';
                $addressLat = $prefix.'lat';
                $addressLng = $prefix.'lng';
                $senDistrictKey = $prefix.'sen_district';
                $houseDistrictKey = $prefix.'house_district';
                $conDistrictKey = $prefix.'con_district';
                $federalSenate = $prefix.'federal_senate';
                if (array_key_exists($addressKey, $row) && $row[$addressKey]) {
                    $response = \AppHelper::searchDistrictByAddress($row[$addressKey]);
                    if (!array_key_exists('results', $response) || !array_key_exists(0, $response['results'])) {
                        $row[$addressKey] = null;
                        continue;
                    }
                    $response = $response['results'][0];
                    if (strtolower($response['address_components']['state']) !== $stateAbbr) {
                        $row[$addressKey] = null;
                        continue;
                    }
                    $stateLegislativeDistricts = $response['fields']['state_legislative_districts'];
                    if (array_key_exists('senate', $stateLegislativeDistricts)) {
                        $length = strlen((string)$this->client->state->sen_districts);
                        $districtNumber = $stateLegislativeDistricts['senate']['district_number'];
                        $districtNumber = str_pad($districtNumber, $length, '0', STR_PAD_LEFT);
                        $row[$senDistrictKey] = 'S'.$districtNumber;
                    }
                    if (array_key_exists('house', $stateLegislativeDistricts)) {
                        $length = strlen((string)$this->client->state->house_districts);
                        $districtNumber = $stateLegislativeDistricts['house']['district_number'];
                        $districtNumber = str_pad($districtNumber, $length, '0', STR_PAD_LEFT);
                        $row[$houseDistrictKey] = 'H'.$districtNumber;
                    }
                    $row[$addressKey] = $response['formatted_address'];
                    $row[$addressLat] = $response['location']['lat'];
                    $row[$addressLng] = $response['location']['lng'];
                    
                    $congressionalDistricts = $response['fields']['congressional_districts'];
                    if (count($congressionalDistricts) > 0) {
                        $ocdId = 'ocd-division/country:us/state:'.$stateAbbr.'/cd:'.$congressionalDistricts[0]['district_number'];
                        $district = District::where('ocd_id', $ocdId)->first();
                        $row[$conDistrictKey] = $district->district;
                    }
                    $row[$federalSenate] = $response['address_components']['state'];
                }
            }
            $row['client_id'] = $this->client->id;
            return new Member($row);
        }
    }

    public function registerEvents(): array
    {
        return [];
    }

    public function rules(): array
    {
        return [
            'active' => 'nullable|numeric',
            'firstname' => 'required|string',
            'lastname' => 'required|string',
            'nickname' => 'required|string',
            'cell' => 'nullable',
            'office_phone' => 'nullable',
            'email' => 'required|string',
            'eligible' => 'nullable|numeric',
            'coordinator' => 'nullable|numeric',
            'dob' => 'nullable|date',
            'grad_year' => 'nullable|numeric',
            'notes' => 'nullable|string',
            'home_address' => 'nullable|string',
            'work_address' => 'nullable|string',
            'work2_address' => 'nullable|string',
            'work3_address' => 'nullable|string',
            'work4_address' => 'nullable|string',
        ];
    }
}
