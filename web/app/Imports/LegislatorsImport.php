<?php

namespace App\Imports;

use App\Models\Legislator;
use Illuminate\Validation\Rule;
use Maatwebsite\Excel\Concerns\Importable;
use Maatwebsite\Excel\Concerns\SkipsFailures;
use Maatwebsite\Excel\Concerns\SkipsOnFailure;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\WithValidation;
use Maatwebsite\Excel\Events\BeforeImport;
use Maatwebsite\Excel\Validators\Failure;

class LegislatorsImport implements ToModel, SkipsOnFailure, WithHeadingRow, WithValidation
{
    use Importable, SkipsFailures;

    protected $importedCount;
    protected $stateId;

    public function __construct($stateId)
    {
        $this->importedCount = 0;
        $this->stateId = $stateId;
    }

    public function getImportedCount()
    {
        return $this->importedCount;
    }

    public static function beforeImport(BeforeImport $event)
    {
        $worksheet = $event->reader->getActiveSheet();
        $highestRow = $worksheet->getHighestRow();
        if ($highestRow < 2) {
            $error = \Illuminate\Validation\ValidationException::withMessages([]);
            $failure = new Failure(1, 'rows', [0 => 'No enough rows!']);
            $failures = [0 => $failure];
            throw new ValidationException($error, $failures);
        }
    }

    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        $this->importedCount += 1;
        $row['state_id'] = $this->stateId;
        $row['elected_at'] = $row['elected_at'] ? $row['elected_at'] : '';
        $row['abdicated_at'] = $row['abdicated_at'] ? $row['abdicated_at'] : '';
        $legislator = new Legislator;
        $legislator->fill($row);
        return $legislator;
    }

    public function rules(): array
    {
        return [
            'firstname' => 'required|string',
            'lastname' => 'required|string',
            'nickname' => 'required|string',
            'fullname' => 'required|string',
            'party' => 'nullable|string',
            'district_id' => 'required|numeric',
            'official_email' => 'nullable|string',
            'official_phone' => 'nullable|string',
            'official_fax' => 'nullable|string',
            'official_address' => 'nullable|string',
            'state_room' => 'nullable|string',
            'aide_name' => 'nullable|string',
            'official_image' => 'nullable|string',
            'occupation' => 'nullable|string',
            'personal_email' => 'nullable|string',
            'local_phone' => 'nullable|string',
            'local_address1' => 'nullable|string',
            'local_address2' => 'nullable|string',
            'local_city_st_zip' => 'nullable|string',
            'elected_at' => 'nullable|string',
            'abdicated_at' => 'nullable|string',
            'website_url' => 'nullable|string',
            'twitter_url' => 'nullable|string',
            'facebook_url' => 'nullable|string',
        ];
    }
}
