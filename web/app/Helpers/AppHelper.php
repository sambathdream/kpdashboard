<?php

namespace App\Helpers;

use Illuminate\Support\Facades\Facade;

class AppHelper extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'AppHelper';
    }

    // General
    public static function getDomesticPhoneNumber($phoneNumber)
    {
        $phoneNumber = preg_replace('/[^0-9]/', '', $phoneNumber);
        if (strlen($phoneNumber) === 11) {
            $phoneNumber = substr($phoneNumber, 1, 10);
        }
        if (strlen($phoneNumber) !== 10) return null;
        return '('.substr($phoneNumber, 0, 3).') '.substr($phoneNumber, 3, 3).'-'.substr($phoneNumber, 6, 4);
    }

    public static function getInternationalPhoneNumber($phoneNumber)
    {
        $phoneNumber = preg_replace('/[^0-9]/', '', $phoneNumber);
        if (strlen($phoneNumber) === 11) {
            $phoneNumber = substr($phoneNumber, 1, 10);
        }
        if (strlen($phoneNumber) !== 10) return null;
        return '+1'.$phoneNumber;
    }

    public static function getFinalMessage($message, $legislator, $member, $query = '')
    {
        if ($member && $legislator && $legislator->district->district) {
            if ($legislator->district->district[0] === 'H') {
                $message = preg_replace('/{leg_salutation}/', $member->client->formal_house.' '.$legislator->lastname, $message);
            }
            if ($legislator->district->district[0] === 'S') {
                $message = preg_replace('/{leg_salutation}/', $member->client->formal_senate.' '.$legislator->lastname, $message);
            }
        }
        if ($member) {
            $message = preg_replace('/{kp_nick}/', $member->nickname, $message);
            $message = preg_replace('/{kp_first}/', $member->firstname, $message);
            $message = preg_replace('/{kp_last}/', $member->lastname, $message);
            $message = preg_replace('/{ez_login}/', 'https://kpda.sh/ez/'.$member->token.$query, $message);
            $message = preg_replace('/{member_nick}/', $member->nickname, $message);
        }
        if ($legislator) {
            $message = preg_replace('/{leg_nick}/', $legislator->nickname, $message);
            $message = preg_replace('/{leg_last}/', $legislator->lastname, $message);
            $message = preg_replace('/{leg_full}/', $legislator->fullname, $message);
            $message = preg_replace('/{leg_email}/', $legislator->official_email, $message);
            $message = preg_replace('/{leg_ph}/', $legislator->official_phone, $message);
            $message = preg_replace('/{district}/', $legislator->district->district, $message);
        }
        return $message;
    }

    public static function searchDistrictByAddress($address)
    {
        $geocodeApiKey = env('GEOCODE_API_KEY');
        $url = 'https://api.geocod.io/v1.3/geocode?q='.urlencode($address).'&fields=cd,stateleg&api_key='.$geocodeApiKey;
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        $response = curl_exec($curl);
        $response = json_decode($response, true);
        return $response;
    }
}
