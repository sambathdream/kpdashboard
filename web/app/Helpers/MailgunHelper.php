<?php

namespace App\Helpers;

use Bogardo\Mailgun\Facades\Mailgun;
use Illuminate\Support\Facades\Facade;

class MailgunHelper extends Facade
{
    public static function send($template, $to, $subject = '', $payload = [])
    {
        $from = env('MAILGUN_FROM_NAME') . ' <' . env('MAILGUN_FROM_ADDRESS') . '>';
        $data = [
            'from' => $from,
            'to' => $to,
            'subject' => $subject,
            'template' => $template,
            'h:X-Mailgun-Variables' => json_encode($payload)
        ];

        $endpoint = env('MAILGUN_DOMAIN') . '/messages';
        return Mailgun::api()->post($endpoint, $data);
    }
}
