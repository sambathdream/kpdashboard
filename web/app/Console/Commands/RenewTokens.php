<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\Member;

class RenewTokens extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'tokens:renew';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Renew member token';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $members = Member::where('active', 1)->get();
        foreach ($members as $member) {
            $isRequired = false;
            if (!$member->token) {
                $isRequired = true;
            }
            if ($member->token_used_at) {
                $now = strtotime(date('Y-m-d h:i:s'));
                $token_used_at = strtotime($member->token_used_at);
                $timeSpent = ($now - $token_used_at) / 60;
                $isRequired = ($timeSpent >= 120);
            }
            if ($isRequired) {
                $member->token = str_random(8);
                $member->password = bcrypt($member->token);
                $member->token_used_at = null;
                $member->save();
            }
        }
    }
}
