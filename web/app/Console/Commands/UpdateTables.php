<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Imports\LegislatorsImport;
use App\Models\District;
use App\Models\Legislator;
use App\Models\Member;
use App\Models\State;
use Maatwebsite\Excel\Facades\Excel;

class UpdateTables extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'tables:update';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update tables';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        // Legislators table (Update district_id)
        // $rows = Excel::toArray(null, 'imports/legislators/legislators.csv', 'local');
        // foreach ($rows[0] as $index => $row) {
        //     if ($index === 0) {
        //         continue;
        //     }
        //     $legislator = Legislator::where('id', $row[0])->first();
        //     if ($legislator) {
        //         $legislator->district_id = $row[7];
        //         $legislator->save();
        //     }
        // }

        // Fill State's Districts Count
        // $states = State::get();
        // foreach ($states as $state) {
        //     $state->house_districts = District::where('state_id', $state->id)
        //         ->where('district', 'like', 'H%')
        //         ->count();
        //     $state->sen_districts = District::where('state_id', $state->id)
        //         ->where('district', 'like', 'S%')
        //         ->count();
        //     $state->save();
        // }

        // Update Party
        // $legislators = Legislator::get();
        // $partyMap = [
        //     'Nonpartisan' => 'N',
        //     'Republican Party' => 'R',
        //     'Democratic Party' => 'D',
        //     'Independent' => 'I',
        //     'Working Families Party' => 'D',
        //     'Independent Party' => 'I',
        //     'Common Sense Independent Party' => 'I',
        //     'Independent for Maine Party' => 'I',
        //     'Unenrolled' => 'N',
        //     'Democratic-Farmer-Labor Party' => 'D',
        //     'Democratic;Republican' => 'D',
        //     'Democratic' => 'D',
        //     'None' => 'N',
        //     'Republican Party, Conservative Party, Independence Party' => 'R',
        //     'Democratic Party, Working Families' => 'D',
        //     'Democratic Party, Independence Party' => 'D',
        //     'Democratic Party, Independence Party, Working Families Party' => 'D',
        //     'Republican Party, Conservative Party, Independence Party, Reform Party' => 'R',
        //     'Republican Party, Conservative Party' => 'R',
        //     'Democratic-NPL Party' => 'D',
        //     'Progressive Party' => 'D',
        //     'Democratic/Progressive' => 'D',
        //     'Progressive/Democratic' => 'D',
        // ];
        // foreach ($legislators as $legislator) {
        //     if (array_key_exists($legislator->party, $partyMap)) {
        //         $legislator->party = $partyMap[$legislator->party];
        //     }
        //     if (!$legislator->party) {
        //         $legislator->party = 'N';
        //     }
        //     $legislator->save();
        // }

        // Clean up legislators table
        $districts = District::get();
        foreach ($districts as $district) {
            $districtLegislators = Legislator::where('district_id', $district->id)->whereNull('abdicated_at')->get();
            if (count($districtLegislators) > 1) {
                foreach ($districtLegislators as $districtLegislator) {
                    if (!$districtLegislator->elected_at) {
                        $districtLegislator->delete();
                    }
                }
            }
        }
    }
}
