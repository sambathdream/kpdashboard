<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class ImportStates extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'states:import';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Import states from 3rd party API';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $sql = "
            INSERT INTO `states` 
                (`state`, `state_abbrev`, `district_total`, `district_label_s`, `district_label_p`, `house_districts`, `sen_districts`)
            VALUES
                ('Nebraska', 'ne', 0, 'District', 'Districts', 0, 0),
                ('Louisiana', 'la', 0, 'District', 'Districts', 0, 0),
                ('Virginia', 'va', 0, 'District', 'Districts', 0, 0),
                ('Oregon', 'or', 0, 'District', 'Districts', 0, 0),
                ('Alabama', 'al', 0, 'District', 'Districts', 0, 0),
                ('Alaska', 'ak', 0, 'District', 'Districts', 0, 0),
                ('Arizona', 'az', 0, 'District', 'Districts', 0, 0),
                ('Arkansas', 'ar', 0, 'District', 'Districts', 0, 0),
                ('California', 'ca', 0, 'District', 'Districts', 0, 0),
                ('Colorado', 'co', 0, 'District', 'Districts', 0, 0),
                ('Connecticut', 'ct', 0, 'District', 'Districts', 0, 0),
                ('Delaware', 'de', 0, 'District', 'Districts', 0, 0),
                ('Florida', 'fl', 0, 'District', 'Districts', 0, 0),
                ('Georgia', 'ga', 0, 'District', 'Districts', 0, 0),
                ('Hawaii', 'hi', 0, 'District', 'Districts', 0, 0),
                ('Idaho', 'id', 0, 'District', 'Districts', 0, 0),
                ('Illinois', 'il', 0, 'District', 'Districts', 0, 0),
                ('Indiana', 'in', 0, 'District', 'Districts', 0, 0),
                ('Iowa', 'ia', 0, 'District', 'Districts', 0, 0),
                ('Kansas', 'ks', 0, 'District', 'Districts', 0, 0),
                ('Kentucky', 'ky', 0, 'District', 'Districts', 0, 0),
                ('Maine', 'me', 0, 'District', 'Districts', 0, 0),
                ('Maryland', 'md', 0, 'District', 'Districts', 0, 0),
                ('Massachusetts', 'ma', 0, 'District', 'Districts', 0, 0),
                ('Michigan', 'mi', 0, 'District', 'Districts', 0, 0),
                ('Minnesota', 'mn', 0, 'District', 'Districts', 0, 0),
                ('Mississippi', 'ms', 0, 'District', 'Districts', 0, 0),
                ('Missouri', 'mo', 0, 'District', 'Districts', 0, 0),
                ('Montana', 'mt', 0, 'District', 'Districts', 0, 0),
                ('Nevada', 'nv', 0, 'District', 'Districts', 0, 0),
                ('New Hampshire', 'nh', 0, 'District', 'Districts', 0, 0),
                ('New Jersey', 'nj', 0, 'District', 'Districts', 0, 0),
                ('New Mexico', 'nm', 0, 'District', 'Districts', 0, 0),
                ('New York', 'ny', 0, 'District', 'Districts', 0, 0),
                ('North Carolina', 'nc', 0, 'District', 'Districts', 0, 0),
                ('North Dakota', 'nd', 0, 'District', 'Districts', 0, 0),
                ('Ohio', 'oh', 0, 'District', 'Districts', 0, 0),
                ('Oklahoma', 'ok', 0, 'District', 'Districts', 0, 0),
                ('Pennsylvania', 'pa', 0, 'District', 'Districts', 0, 0),
                ('Rhode Island', 'ri', 0, 'District', 'Districts', 0, 0),
                ('South Carolina', 'sc', 0, 'District', 'Districts', 0, 0),
                ('South Dakota', 'sd', 0, 'District', 'Districts', 0, 0),
                ('Tennessee', 'tn', 0, 'District', 'Districts', 0, 0),
                ('Texas', 'tx', 0, 'District', 'Districts', 0, 0),
                ('Utah', 'ut', 0, 'District', 'Districts', 0, 0),
                ('Vermont', 'vt', 0, 'District', 'Districts', 0, 0),
                ('Washington', 'wa', 0, 'District', 'Districts', 0, 0),
                ('West Virginia', 'wv', 0, 'District', 'Districts', 0, 0),
                ('Wisconsin', 'wi', 0, 'District', 'Districts', 0, 0),
                ('Wyoming', 'wy', 0, 'District', 'Districts', 0, 0);
        ";
        \DB::connection()->getPdo()->exec($sql);
    }
}
