<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\State;
use App\Models\District;
use App\Models\Legislator;
use App\Models\ImportedLegislator;

class ImportLegislators extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'legislators:import';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Import legislators from 3rd party API';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $isFederal = false;
        $googleApiKey = env('GOOGLE_API_KEY');
        $states = State::get();
        foreach ($states as $state) {
            $stateDivisionId = 'ocd-division%2Fcountry%3Aus%2Fstate%3A'.$state->state_abbrev;
            if ($isFederal) {
                // Federal Legislators
                $url = 'https://www.googleapis.com/civicinfo/v2/representatives/'.$stateDivisionId.'?levels=country&recursive=true&roles=legislatorLowerBody&roles=legislatorUpperBody&key='.$googleApiKey;
            } else {
                // State Legislators
                $url = 'https://www.googleapis.com/civicinfo/v2/representatives/'.$stateDivisionId.'?levels=administrativeArea1&recursive=true&roles=legislatorLowerBody&roles=legislatorUpperBody&key='.$googleApiKey;
            }
            
            $curl = curl_init();
            curl_setopt($curl, CURLOPT_URL, $url);
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
            $response = curl_exec($curl);
            $response = json_decode($response, true);
            if (!array_key_exists('error', $response)) {
                foreach ($response['divisions'] as $districtDivisionId => $districtData) {
                    $district = District::where('ocd_id', $districtDivisionId)->first();
                    $officeIndices = $districtData['officeIndices'];
                    $officialIndices = [];
                    foreach ($officeIndices as $officeIndex) {
                        $office = $response['offices'][$officeIndex];
                        if (array_key_exists('officialIndices', $office)) {
                            $officialIndices = array_merge($officialIndices, $office['officialIndices']);
                        }
                    }
                    foreach ($officialIndices as $officialIndex) {
                        $official = $response['officials'][$officialIndex];
                        // Check if official has official emails
                        if (!array_key_exists('name', $official) || !array_key_exists('emails', $official)) continue;
                        // Check if official was already added to legislators table
                        if (ImportedLegislator::where('state_id', $state->id)->where('official_email', $official['emails'][0])->first()) continue;

                        $originalName = $official['name'];
                        // Escape all symbols
                        $official['name'] = preg_replace('/\"(\w)*\"/', '', $official['name']);
                        $official['name'] = preg_replace('/,/', '', $official['name']);
                        $official['name'] = preg_replace('/\s(\w)+\./', ' ', $official['name']);
                        $official['name'] = preg_replace('/^(\w)+\.\s/', ' ', $official['name']);
                        $official['name'] = preg_replace('/(\s)+/', ' ', $official['name']);
                        $official['name'] = preg_replace('/^(\s)+/', '', $official['name']);
                        $official['name'] = preg_replace('/(\s)+$/', '', $official['name']);
                        $names = explode(' ', $official['name']);
                        $legislatorData = [
                            'state_id' => $state->id,
                            'district_id' => $district->id,
                            'is_federal' => $isFederal,
                            'health'=>'[0]',
                            'firstname' => $names[0],
                            'lastname' => $names[count($names) - 1],
                            'nickname' => $names[0],
                            'fullname' => $official['name'],
                            'common_name' => $originalName,
                            'official_email' => $official['emails'][0],
                        ];
                        $legislatorData['party'] = array_key_exists('party', $official) ? $official['party'] : 'None';
                        $legislatorData['official_phone'] = array_key_exists('phones', $official) ? $official['phones'][0] : '';
                        $legislatorData['official_image'] = array_key_exists('photoUrl', $official) ? $official['photoUrl'] : '';
                        $legislatorData['personal_email'] = count($official['emails']) > 1 ? $official['emails'][1] : '';
                        $legislatorData['local_phone'] = array_key_exists('phones', $official) && count($official['phones']) > 1 ? $official['phones'][1] : '';
                        $legislatorData['website_url'] = array_key_exists('urls', $official) ? $official['urls'][0] : '';
                        if (array_key_exists('address', $official)) {
                            $officialAddress = '';
                            $localAddress = '';
                            $address1 = $official['address'][0];
                            $address2 = count($official['address']) > 1 ? $official['address'][1] : null;
                            // Official Address
                            if (array_key_exists('line1', $address1) && $address1['line1']) $officialAddress .= $address1['line1'];
                            if (array_key_exists('line2', $address1) && $address1['line2']) $officialAddress .= ', '.$address1['line2'];
                            if (array_key_exists('line3', $address1) && $address1['line3']) $officialAddress .= ', '.$address1['line3'];
                            if (array_key_exists('city', $address1) && $address1['city']) $officialAddress .= ', '.$address1['city'];
                            if (array_key_exists('state', $address1) && $address1['state']) $officialAddress .= ', '.$address1['state'];
                            if (array_key_exists('zip', $address1) && $address1['zip']) $officialAddress .= ' '.$address1['zip'];
                            // Local Address
                            if ($address2) {
                                if (array_key_exists('line1', $address2) && $address2['line1']) $localAddress .= $address2['line1'];
                                if (array_key_exists('line2', $address2) && $address2['line2']) $localAddress .= ', '.$address2['line2'];
                                if (array_key_exists('line3', $address2) && $address2['line3']) $localAddress .= ', '.$address2['line3'];
                                if (array_key_exists('city', $address2) && $address2['city']) $localAddress .= ', '.$address2['city'];
                                if (array_key_exists('state', $address2) && $address2['state']) $localAddress .= ', '.$address2['state'];
                                if (array_key_exists('zip', $address2) && $address2['zip']) $localAddress .= ' '.$address2['zip'];
                            }
                            $officialAddress = str_replace(array("\n", "\r"), ' ', $officialAddress);
                            $officialAddress = preg_replace('/(\s)+/', ' ', $officialAddress);
                            $localAddress = str_replace(array("\n", "\r"), ' ', $localAddress);
                            $localAddress = preg_replace('/(\s)+/', ' ', $localAddress);
                            $legislatorData['official_address'] = $officialAddress;
                            $legislatorData['local_address'] = $localAddress;
                        }
                        if (array_key_exists('channels', $official)) {
                            foreach ($official['channels'] as $channel) {
                                if ($channel['type'] === 'Facebook') {
                                    $legislatorData['facebook_url'] = 'https://facebook.com/'.$channel['id'];
                                }
                                if ($channel['type'] === 'Twitter') {
                                    $legislatorData['twitter_url'] = 'https://twitter.com/'.$channel['id'];
                                }
                            }
                        }
                        $newLegislator = new ImportedLegislator($legislatorData);
                        $newLegislator->save();
                    }
                }
            }
        }
    }
}
