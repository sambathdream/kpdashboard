<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\State;
use App\Models\District;

class ImportDistricts extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'districts:import';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Import districts from 3rd party API';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $googleApiKey = env('GOOGLE_API_KEY');
        $states = State::get();
        foreach ($states as $state) {
            $stateDivisionId = 'ocd-division%2Fcountry%3Aus%2Fstate%3A'.$state->state_abbrev;
            // Get State Districts
            // $url = 'https://www.googleapis.com/civicinfo/v2/representatives/'.$stateDivisionId.'?levels=administrativeArea1&recursive=true&roles=legislatorLowerBody&roles=legislatorUpperBody&key='.$googleApiKey;
            // $curl = curl_init();
            // curl_setopt($curl, CURLOPT_URL, $url);
            // curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
            // $response = curl_exec($curl);
            // $response = json_decode($response, true);
            // if (!array_key_exists('error', $response)) {
            //     $houseDistrictCount = 0;
            //     $senateDistrictCount = 0;
            //     foreach ($response['divisions'] as $districtDivisionId => $districtData) {
            //         if (strpos($districtDivisionId, 'sldu') !== false) {
            //             $senateDistrictCount += 1;
            //         } else {
            //             $houseDistrictCount += 1;
            //         }
            //     }
            //     $houseDistrictLength = strlen(''.$houseDistrictCount);
            //     $senateDistrictLength = strlen(''.$senateDistrictCount);
            //     foreach ($response['divisions'] as $districtDivisionId => $districtData) {
            //         $isSenate = strpos($districtDivisionId, 'sldu') !== false;
            //         $seperator = $isSenate ? 'sldu:' : 'sldl:';
            //         $districtNumber = preg_split('/'.$seperator.'/', $districtDivisionId)[1];
            //         $newDistrict = new District([
            //             'state_id' => $state->id,
            //             'ocd_id' => $districtDivisionId,
            //             'district' => ($isSenate ? 'S' : 'H').strtoupper(str_pad($districtNumber, ($isSenate ? $senateDistrictLength : $houseDistrictLength), '0', STR_PAD_LEFT)),
            //             'name' => $districtData['name'],
            //         ]);
            //         $newDistrict->save();
            //     }
            // }
            // Get Federal Districts
            $url = 'https://www.googleapis.com/civicinfo/v2/representatives/'.$stateDivisionId.'?levels=country&recursive=true&roles=legislatorLowerBody&roles=legislatorUpperBody&key='.$googleApiKey;
            $curl = curl_init();
            curl_setopt($curl, CURLOPT_URL, $url);
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
            $response = curl_exec($curl);
            $response = json_decode($response, true);
            if (!array_key_exists('error', $response)) {
                $districtCount = count($response['divisions']) - 1;
                $districtLength = strlen(''.$districtCount);
                foreach ($response['divisions'] as $districtDivisionId => $districtData) {
                    $sections = preg_split('/cd:/', $districtDivisionId);
                    $newDistrictData = [
                        'state_id' => $state->id,
                        'is_federal' => true,
                        'ocd_id' => $districtDivisionId,
                        'name' => $districtData['name'],
                    ];
                    if (count($sections) === 2) {
                        $newDistrictData['district'] = 'F'.strtoupper(str_pad($sections[1], $districtLength, '0', STR_PAD_LEFT));
                    } else {
                        $newDistrictData['district'] = strtoupper($state->state_abbrev);
                    }
                    $newDistrict = new District($newDistrictData);
                    $newDistrict->save();
                }
            }
        }
    }
}
