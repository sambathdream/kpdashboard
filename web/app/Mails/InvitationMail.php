<?php

namespace App\Mails;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class InvitationMail extends Mailable
{
    use Queueable, SerializesModels;

    public $mFullName;
    public $mToken;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($mFullName, $mToken)
    {
        $this->mFullName = $mFullName;
        $this->mToken = $mToken;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from('support@kpdashboard.com', 'KP Dashboard Support Team')
            ->subject('A new account has been created for you!')
            ->view('email.invitation');
    }
}
