<?php


namespace App\Mails;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class DeactivateTwoFactorMail extends Mailable
{
    use Queueable, SerializesModels;

    public $mFullName;
    public $token;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($mFullName, $token)
    {
      $this->mFullName = $mFullName;
      $this->token = $token;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
      return $this->from('support@kpdashboard.com', 'KP Dashboard Support Team')
        ->subject('Your verification token')
        ->view('email.deactivatetwofactor');
    }
}
