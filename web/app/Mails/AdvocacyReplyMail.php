<?php

namespace App\Mails;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class AdvocacyReplyMail extends Mailable
{
    use Queueable, SerializesModels;

    public $mFromEmail;
    public $mFrom;
    public $mReplyToEmail;
    public $mSubject;
    public $mMessage;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($fromEmail, $from, $replyToEmail, $subject, $message)
    {
        $this->mFromEmail = $fromEmail;
        $this->mFrom = $from;
        $this->mReplyToEmail = $replyToEmail;
        $this->mSubject = $subject;
        $this->mMessage = $message;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from($this->mFromEmail, $this->mFrom)
            ->replyTo($this->mReplyToEmail, $this->mFrom)
            ->subject($this->mSubject)
            ->view('email.advocacy_reply');
    }
}
