<?php

namespace App\Mails;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class TaskMail extends Mailable
{
    use Queueable, SerializesModels;

    public $mFrom;
    public $mReplyToEmail;
    public $mSubject;
    public $mMessage;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($from, $replyToEmail, $subject, $message)
    {
        $this->mFrom = $from;
        $this->mReplyToEmail = $replyToEmail;
        $this->mSubject = $subject;
        $this->mMessage = $message;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from('outgoing@kpdashboard.com', $this->mFrom)
            ->replyTo($this->mReplyToEmail, $this->mFrom)
            ->subject($this->mSubject)
            ->view('email.task');
    }
}
