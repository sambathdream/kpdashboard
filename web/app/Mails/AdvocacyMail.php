<?php

namespace App\Mails;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class AdvocacyMail extends Mailable
{
    use Queueable, SerializesModels;

    public $mFromEmail;
    public $mFromName;
    public $mSubject;
    public $mMessage;
    public $mNickname;
    public $mToken;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($fromEmail, $fromName, $subject, $message, $nickname, $token)
    {
        $this->mFromEmail = $fromEmail;
        $this->mFromName = $fromName;
        $this->mSubject = $subject;
        $this->mMessage = $message;
        $this->mNickname = $nickname;
        $this->mToken = $token;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from($this->mFromEmail, $this->mFromName)
            ->subject($this->mSubject)
            ->view('email.advocacy');
    }
}
