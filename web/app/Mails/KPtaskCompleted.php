<?php


namespace App\Mails;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class KPtaskCompleted extends  Mailable
{
    use Queueable, SerializesModels;

    public $mMessage;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct( $message)
    {
        $this->mMessage = $message;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from('noreply@kpdashboard.com', 'KP Dashboard Support Team')
          ->subject($this->mMessage)
          ->view('email.kpTaskCompleted');
    }
}
