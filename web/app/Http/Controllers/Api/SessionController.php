<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\LegData;
use App\Models\Legislator;
use App\Models\Member;
use App\Models\MemberLegislator;
use App\Models\Session;
use App\Models\SessionTask;
use App\Models\TaskResult;
use App\Models\TaskType;

class SessionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->validate([
            'client_id' => 'required|numeric',
            'name' => 'required|string',
            'notes' => 'nullable|string',
            'start_at' => 'required|string',
            'end_at' => 'required|string',
            'house_committees' => 'present',
            'senate_committees' => 'present',
            'joint_committees' => 'present',
        ]);
        $session = new Session;
        $session->fill($data);
        $session->save();
        // Add Notes task to session by default
        SessionTask::create([
            'session_id' => $session->id,
            'task_type_id' => 3,
            'name' => 'Notes',
        ]);
        return $session;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Session $session)
    {
        $data = $request->validate([
            'client_id' => 'required|numeric',
            'name' => 'required|string',
            'notes' => 'nullable|string',
            'start_at' => 'required|string',
            'end_at' => 'required|string',
            'house_committees' => 'present',
            'senate_committees' => 'present',
            'joint_committees' => 'present',
        ]);
        $session->fill($data);
        $session->save();
        return $session;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Session $session)
    {
        $session->delete();
        return response()->json(true);
    }

    /**
     * Display a listing of the session's kp tasks.
     *
     * @return \Illuminate\Http\Response
     */
    public function getKPTasks(Session $session)
    {
        $client = $session->client;
        $user = \Auth::user();
        $userRole = $user->getRoleNames()[0];
        $result = [];
        $stateLegislators = Legislator::where('state_id', $client->state_id)
            ->with(['district', 'committees'])
            ->whereNull('abdicated_at')
            ->get()
            ->toArray();
        $myLegislators = [];
        foreach ($stateLegislators as $index => $legislator) {
            $legislator['primary_kp'] = MemberLegislator::where('leg_id', $legislator['id'])
                ->with(['member'])
                ->where('client_id', $client->id)
                ->where('kp_position', 1)
                ->whereNull('end_date')
                ->first();
            $legData = LegData::where('client_id', $client->id)
                ->where('leg_id', $legislator['id'])
                ->first();
            $legislator['leg_region'] = is_null($legData) ? null : $legData->leg_region;
            if ($userRole === 'Coordinator' && !$client->show_all && ($user->member->coordinator !== $legislator['leg_region'])) continue;
            $legislator['coordinator'] = Member::select('firstname', 'lastname', 'nickname', 'cell', 'email')
                ->where('client_id', $client->id)
                ->where('active', 1)
                ->whereNotNull('coordinator')
                ->where('coordinator', $legislator['leg_region'])
                ->first();
            $myLegislators[] = $legislator;
        }
        $tasks = $client->tasks($session->id);
        $taskIds = [];
        foreach ($tasks as $task) {
            $taskIds[] = $task->id;
        }
        $taskResults = TaskResult::whereIn('task_id', $taskIds)->get();
        $result['session'] = $session;
        $result['legislators'] = $myLegislators;
        $result['tasks'] = $tasks;
        $result['task_results'] = $taskResults;
        return $result;
    }

    /**
     * Display a listing of session tasks.
     *
     * @return \Illuminate\Http\Response
     */
    public function getTasks(Session $session)
    {
        return $session->tasks()
            ->where('task_type_id', '!=', 3)
            ->with('task_type')
            ->withCount('task_results')
            ->get();
    }

    /**
     * Add new tasks to a specific session
     *
     * @return \Illuminate\Http\Response
     */
    public function addTasks(Request $request, Session $session)
    {
        $data = $request->validate([
            'hide_from_kp' => 'nullable|boolean',
            'proto_ids' => 'present|array',
            'custom_task' => 'nullable',
            // 'custom_task.name' => 'required|string',
            // 'custom_task.task_type_id' => 'required|numeric',
            // 'custom_task.options' => 'present|array',
            // 'custom_task.tooltip' => 'nullable|string',
            // 'custom_task.notes' => 'nullable|string',
            // 'custom_task.deadline' => 'nullable|string',
        ]);
        $protoIds = $data['proto_ids'];
        if (count($protoIds) > 0) {
            $tasks = SessionTask::whereIn('id', $protoIds)->get();
            foreach ($tasks as $task) {
                SessionTask::create([
                    'session_id' => $session->id,
                    'task_type_id' => $task->task_type_id,
                    'name' => $task->name,
                    'tooltip' => $task->tooltip,
                    'notes' => $task->notes,
                    'hide_from_kp' => $data['hide_from_kp'],
                ]);
            }
        }
        if (!is_null($data['custom_task'])) {
            $taskData = $data['custom_task'];
            $taskTypeId = $taskData['task_type_id'];
            if ($taskTypeId === -1) {
                $taskType = TaskType::create([
                    'client_id' => $session->client_id,
                    'name' => $taskData['name'],
                    'data_type' => 'Select',
                    'options' => $taskData['options']
                ]);
                $taskTypeId = $taskType->id;
            }
            SessionTask::create([
                'session_id' => $session->id,
                'task_type_id' => $taskTypeId,
                'name' => $taskData['name'],
                'tooltip' => isset($taskData['tooltip']) ? $taskData['tooltip'] : null,
                'notes' => isset($taskData['notes']) ? $taskData['notes'] : null,
                'deadline' => isset($taskData['deadline']) ? $taskData['deadline'] : null,
                'hide_from_kp' => $data['hide_from_kp'],
            ]);
        }
        return response()->json(true);
    }

    /**
     * Display a client's dashboard data
     *
     * @return \Illuminate\Http\Response
     */
    public function getDashboardData(Session $session)
    {
        $client_id = $session->client->id;
        $state_id = $session->client->state_id;
        $result = [];



        /** Top */
        // Session Tasks
        $query = "
            SELECT COUNT(session_tasks.`id`) task_count
            FROM `task_types`, `session_tasks`
            WHERE task_types.`data_type` IN ('Boolean', 'Select') AND task_types.`id` = session_tasks.`task_type_id` AND session_tasks.`session_id` = ".$session->id."
        ";
        $result['task_count'] = \DB::select(\DB::raw($query))[0]->task_count ?: 0;

        /**  */
        // Upcoming Tasks
        $now = date('Y-m-d');
        $query = "
            SELECT COUNT(session_tasks.`id`) upcoming_tasks
            FROM `session_tasks`, `task_types`
            WHERE session_id = ".$session->id." 
                AND (deadline IS NULL OR deadline >= '".$now."')
                AND (session_tasks.task_type_id = task_types.id)
                AND (task_types.data_type in ('Boolean', 'Select'));
        ";
        $result['upcoming_tasks'] = \DB::select(\DB::raw($query))[0]->upcoming_tasks ?: 0;

        /**  */
        // Days Until Session End
        $now = new \DateTime('now');
        $session_end_date = new \DateTime($session->end_at);
        $result['days_left'] = (int)$now->diff($session_end_date)->format('%R%a');

        /**  */
        // Pull District Count From Client's State
        $query = "
            SELECT district_total as district_count
            FROM `states`
            WHERE id = ".$state_id."
        ";
        $district_count = \DB::select(\DB::raw($query))[0]->district_count ?: 0;

        // Get count of legislators who have a primary KP
        $query = "
            SELECT COUNT(id) as assigned_legislators
            FROM `member_legislator`
            WHERE client_id = ".$client_id." AND kp_position = 1 AND end_date IS NULL
        ";
        $assigned_legislators = \DB::select(\DB::raw($query))[0]->assigned_legislators ?: 0;

        // Calculate % of legislators assigned a primary KP
        $result['assigned_legislators'] = $district_count ? round(($assigned_legislators / $district_count) * 100) : 0;

        /**  */
        // Total KPs
        $query = "
            SELECT COUNT(DISTINCT member_id) as total_kps
            FROM `member_legislator`
            WHERE client_id = ".$client_id." AND end_date IS NULL
        ";
        $total_kps = \DB::select(\DB::raw($query))[0]->total_kps ?: 0;

        // Active KPs
        $query = "
            SELECT COUNT(DISTINCT task_results.kp_id) as active_kps
            FROM `task_results`, `session_tasks`
            WHERE session_tasks.session_id = ".$session->id." AND session_tasks.id = task_results.task_id
        ";
        $active_kps = \DB::select(\DB::raw($query))[0]->active_kps ?: 0;
        $result['active_kps'] = $total_kps > 0 ? round(($active_kps / $total_kps) * 100) : 0;

        /**  */
        // Total Donations
        $query = "
            SELECT SUM(REPLACE(task_results.`result`, ',', '')) donations
            FROM `task_types`, `session_tasks`, `task_results`
            WHERE task_types.`data_type` = 'Currency'
                AND session_tasks.`session_id` = ".$session->id."
                AND session_tasks.`task_type_id` = task_types.`id`
                AND session_tasks.`id` = task_results.`task_id`
        ";
        $result['donations'] = \DB::select(\DB::raw($query))[0]->donations ?: 0;
        


        /** Left */
        // Active Members
        $query = "
            SELECT COUNT(`id`) active_members
            FROM `members`
            WHERE client_id = ".$client_id." AND active = 1;
        ";
        $result['active_members'] = \DB::select(\DB::raw($query))[0]->active_members ?: 0;

        // Total Unique KPs
        $query = "
            SELECT COUNT(DISTINCT `member_id`) total_kps
            FROM `member_legislator`
            WHERE client_id = ".$client_id." AND end_date IS NULL;
        ";
        $result['total_kps'] = \DB::select(\DB::raw($query))[0]->total_kps ?: 0;

        // Opted-Out
        $query = "
            SELECT COUNT(`id`) opted_out
            FROM `members`
            WHERE client_id = ".$client_id." AND active = 1 AND eligible = 0;
        ";
        $result['opted_out'] = \DB::select(\DB::raw($query))[0]->opted_out ?: 0;

        // Eligible to Volunteer
        $result['eligible'] = $result['active_members'] - $result['opted_out'];


        /** Center */
        // Total Legislators
        $query = "
            SELECT district_total as leg
            FROM `states`
            WHERE id = ".$state_id."
        ";
        $result['leg'] = \DB::select(\DB::raw($query))[0]->leg ?: 0;

        // House
        $query = "
            SELECT house_districts as house
            FROM `states`
            WHERE id = ".$state_id."
        ";
        $result['house'] = \DB::select(\DB::raw($query))[0]->house ?: 0;

        // Senate
        $query = "
            SELECT sen_districts as senate
            FROM `states`
            WHERE id = ".$state_id."
        ";
        $result['senate'] = \DB::select(\DB::raw($query))[0]->senate ?: 0;

        // Active GOP Count
        $query = "
            SELECT COUNT(party) AS republicans
            FROM `legislators`
            WHERE state_id = ".$state_id." AND party = 'R' AND abdicated_at IS NULL
        ";
        $result['republicans'] = \DB::select(\DB::raw($query))[0]->republicans ?: 0;
        // Active Democrats Count
        $query = "
            SELECT COUNT(party) AS democrats
            FROM `legislators`
            WHERE state_id = ".$state_id." AND party = 'D' AND abdicated_at IS NULL
        ";
        $result['democrats'] = \DB::select(\DB::raw($query))[0]->democrats ?: 0;
        // Active Independents Count
        $query = "
            SELECT COUNT(party) AS independents
            FROM `legislators`
            WHERE state_id = ".$state_id." AND party = 'I' AND abdicated_at IS NULL
        ";
        $result['independents'] = \DB::select(\DB::raw($query))[0]->independents ?: 0;
        // Active Libertarians Count
        $query = "
            SELECT COUNT(party) AS libertarians
            FROM `legislators`
            WHERE state_id = ".$state_id." AND party = 'L' AND abdicated_at IS NULL
        ";
        $result['libertarians'] = \DB::select(\DB::raw($query))[0]->libertarians ?: 0;


        /** Center */
        // Session Tasks
        $query = "
            SELECT session_tasks.*
            FROM `session_tasks`, `task_types`
            WHERE session_tasks.session_id = ".$session->id." 
                AND session_tasks.task_type_id = task_types.id
                AND task_types.data_type in ('Boolean', 'Select')
            ORDER BY -deadline DESC;
        ";
        $result['session_tasks'] = \DB::select(\DB::raw($query));

        // Latest Completed Tasks
        $query = "
            SELECT CONCAT(members.nickname, ' ', members.lastname) kp_name, session_tasks.name task_name, task_results.updated_at
            FROM `task_results`, `session_tasks`, `members`, `task_types`
            WHERE session_tasks.session_id = ".$session->id." 
                AND session_tasks.task_type_id = task_types.id
                AND task_types.data_type in ('Boolean', 'Select')
                AND task_results.task_id = session_tasks.id
                AND task_results.kp_id = members.id
            ORDER BY task_results.updated_at DESC
            LIMIT 10;
        ";
        $result['latest_completed_tasks'] = \DB::select(\DB::raw($query));

        return $result;
    }
}
