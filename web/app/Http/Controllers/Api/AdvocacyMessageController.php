<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Mails\AdvocacyMail;
use App\Mails\AdvocacyReplyMail;
use App\Mails\TaskMail;
use App\Models\AdvocacyMessage;
use App\Models\AdvocacyMemberLegislator;
use App\Models\AdvocacyMemberMessage;
use App\Models\Legislator;
use App\Models\Member;
use App\Models\MemberLegislator;
use Twilio\Rest\Client as TwilioClient;

class AdvocacyMessageController extends Controller
{
    /**
     * Create a new instance
     *
     * @return void
     */
    public function __construct()
    {
        $this->twilioClient = new TwilioClient(env('TWILIO_SID'), env('TWILIO_TOKEN'));
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->validate([
            'client_id' => 'required|numeric',
            'subject' => 'required|string',
            'message' => 'required|string',
            'legislators' => 'required|array',
            'members' => 'required|array',
            'is_kp_only' => 'required|boolean',
            'active' => 'nullable|boolean',
            'deadline' => 'nullable|string',
            'invitation_subject' => 'required|string',
            'invitation_message' => 'required|string',
            'via_sms' => 'nullable|boolean',
        ]);
        $advocacyMessage = new AdvocacyMessage;
        $advocacyMessage->fill($data);
        $advocacyMessage->save();
        $this->fillAdvocacyMemberLegislators($advocacyMessage);
        return $advocacyMessage;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, AdvocacyMessage $advocacyMessage)
    {
        $data = $request->validate([
            'client_id' => 'required|numeric',
            'subject' => 'required|string',
            'message' => 'required|string',
            'legislators' => 'required|array',
            'members' => 'required|array',
            'is_kp_only' => 'required|boolean',
            'active' => 'nullable|boolean',
            'deadline' => 'nullable|string',
            'invitation_subject' => 'required|string',
            'invitation_message' => 'required|string',
            'via_sms' => 'nullable|boolean',
        ]);
        $advocacyMessage->fill($data);
        $advocacyMessage->save();
        $this->fillAdvocacyMemberLegislators($advocacyMessage);
        return $advocacyMessage;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(AdvocacyMessage $advocacyMessage)
    {
        $advocacyMessage->delete();
        return response()->json(true);
    }

    public function send(AdvocacyMessage $advocacyMessage)
    {
        foreach ($advocacyMessage->members as $memberId) {
            $member = Member::find($memberId);
            $sentMessageCount = AdvocacyMemberMessage::where('advocacy_message_id', $advocacyMessage->id)
                ->where('member_id', $member->id)
                ->count();
            $assignedLegislatorCount = count(
                AdvocacyMemberLegislator::where('advocacy_message_id', $advocacyMessage->id)
                    ->where('member_id', $member->id)
                    ->first()
                    ->legislators
            );
            if ($sentMessageCount !== $assignedLegislatorCount) {
                $messageContent = \AppHelper::getFinalMessage($advocacyMessage->invitation_message, null, $member, '?s=adv');
                if ($advocacyMessage->via_sms && $member->cell && $member->client->twilio_number) {
                    $smsBody = [
                        'from' => \AppHelper::getInternationalPhoneNumber($member->client->twilio_number),
                        'body' => $messageContent,
                    ];
                    $this->twilioClient->messages->create(\AppHelper::getInternationalPhoneNumber($member->cell), $smsBody);
                } else {
                    $fromEmail = $member->client->mailgun_email ?: 'outgoing@kpdashboard.com';
                    $fromName = $member->client->assoc_abbrev;
                    \Mail::to($member->email)
                        ->send(new AdvocacyMail(
                            $fromEmail,
                            $fromName,
                            $advocacyMessage->invitation_subject,
                            $messageContent,
                            $member->nickname,
                            $member->token
                        ));
                }
            }
        }
        $advocacyMessage->sent_at = date('Y-m-d h:i:s');
        $advocacyMessage->save();
        return $advocacyMessage;
    }

    public function remind(Request $request, AdvocacyMessage $advocacyMessage)
    {
        $data = $request->validate([
            'member_id' => 'required|numeric',
        ]);
        $member = Member::find($data['member_id']);

        $messageContent = \AppHelper::getFinalMessage($advocacyMessage->invitation_message, null, $member, '?s=adv');
        if ($advocacyMessage->via_sms && $member->cell && $member->client->twilio_number) {
            $smsBody = [
                'from' => \AppHelper::getInternationalPhoneNumber($member->client->twilio_number),
                'body' => $messageContent,
            ];
            $this->twilioClient->messages->create(\AppHelper::getInternationalPhoneNumber($member->cell), $smsBody);
        } else {
            $fromEmail = $member->client->mailgun_email ?: 'outgoing@kpdashboard.com';
            $fromName = $member->client->assoc_abbrev;
            \Mail::to($member->email)
                ->send(new AdvocacyMail(
                    $fromEmail,
                    $fromName,
                    $advocacyMessage->invitation_subject,
                    $messageContent,
                    $member->nickname,
                    $member->token
                ));
        }
        return response()->json(true);
    }

    public function searchMembers(Request $request)
    {
        $data = $request->validate([
            'client_id' => 'required|numeric',
            'is_kp_only' => 'required|boolean',
            'legislators' => 'required|array',
        ]);
        if ($data['is_kp_only']) {
            $memberLegislators = MemberLegislator::where('client_id', $data['client_id'])
                ->whereIn('leg_id', $data['legislators'])
                ->whereNull('end_date')
                ->where('kp_position', 1)
                ->with('member')
                ->get()
                ->toArray();
            $result = array_map(function ($memberLegislator) {
                return $memberLegislator['member'];
            }, $memberLegislators);
            return $result;
        }
        $legislators = Legislator::whereIn('id', $data['legislators'])
            ->with('district')
            ->get()
            ->toArray();
        $districts = array_map(function ($legislator) {
            return $legislator['district']['district'];
        }, $legislators);
        $result = Member::where('client_id', $data['client_id'])
            ->where(function ($query) use ($districts) {
                $query->whereIn('home_house_district', $districts)
                    ->orWhereIn('home_sen_district', $districts)
                    ->orWhereIn('work_house_district', $districts)
                    ->orWhereIn('work_sen_district', $districts)
                    ->orWhereIn('work2_house_district', $districts)
                    ->orWhereIn('work2_sen_district', $districts)
                    ->orWhereIn('work3_house_district', $districts)
                    ->orWhereIn('work3_sen_district', $districts)
                    ->orWhereIn('work4_house_district', $districts)
                    ->orWhereIn('work4_sen_district', $districts);
            })
            ->get();
        return $result;
    }

    public function getStats(Request $request, AdvocacyMessage $advocacyMessage)
    {        
        $messageList = [];
        foreach ($advocacyMessage->advocacyMemberLegislators as $row) {
            $member = $row->member;
            foreach ($row->legislators as $legislatorId) {
                $legislator = Legislator::find($legislatorId);
                $messageList[] = [
                    'legislator_id' => $legislator->id,
                    'legislator_name' => $legislator->fullname,
                    'member_id' => $member->id,
                    'member_name' => $member->firstname.' '.$member->lastname,
                ];
            }
        }

        $sentMessageList = AdvocacyMemberMessage::where('advocacy_message_id', $advocacyMessage->id)
            ->get();
        foreach ($sentMessageList as $sentMessage) {
            foreach($messageList as $index => $message) {
                if ($message['legislator_id'] === $sentMessage->legislator_id && $message['member_id'] === $sentMessage->member_id) {
                    $messageList[$index]['sent_at'] = $sentMessage->created_at.'';
                }
            }
        }
        return $messageList;
    }

    public function getReplyStats(Request $request, AdvocacyMessage $advocacyMessage)
    {
        $data = $request->validate([
            'member_id' => 'required|numeric',
        ]);
        // Get all available legislators
        $availableLegislators = AdvocacyMemberLegislator::where('advocacy_message_id', $advocacyMessage->id)
            ->where('member_id', $data['member_id'])
            ->first()
            ->legislators;
        // Get sent messages
        $sentMessages = AdvocacyMemberMessage::select('legislator_id', 'created_at')
            ->where('advocacy_message_id', $advocacyMessage->id)
            ->where('member_id', $data['member_id'])
            ->get();
        $availableLegislators = Legislator::select('id', 'fullname')
            ->whereIn('id', $availableLegislators)
            ->get();
        foreach ($availableLegislators as $index => $legislator) {
            foreach ($sentMessages as $message) {
                if ($message->legislator_id === $legislator->id) {
                    $legislator->sent_at = $message->created_at.'';
                }
            }
            $availableLegislators[$index] = $legislator;
        }
        return $availableLegislators;
    }

    public function getRemainingLegislators(Request $request, AdvocacyMessage $advocacyMessage)
    {
        $data = $request->validate([
            'member_id' => 'required|numeric',
        ]);
        // Get available legislators
        $availableLegislators = AdvocacyMemberLegislator::where('advocacy_message_id', $advocacyMessage->id)
            ->where('member_id', $data['member_id'])
            ->first()
            ->legislators;
        // Get sent legislators
        $sentMessages = AdvocacyMemberMessage::select('legislator_id')
            ->where('advocacy_message_id', $advocacyMessage->id)
            ->where('member_id', $data['member_id'])
            ->get()
            ->toArray();
        $sentLegislators = array_map(function ($message) {
            return $message['legislator_id'];
        }, $sentMessages);
        // Get remaining legislators
        $remainingLegislators = array_diff($availableLegislators, $sentLegislators);
        $remainingLegislators = Legislator::whereIn('id', $remainingLegislators)->get();
        return $remainingLegislators;
    }

    public function getFormattedSubject(Request $request, AdvocacyMessage $advocacyMessage)
    {
        $data = $request->validate([
            'legislator_id' => 'required',
            'member_id' => 'required',
        ]);
        $legislator = Legislator::find($data['legislator_id']);
        $member = Member::find($data['member_id']);
        $result = \AppHelper::getFinalMessage($advocacyMessage->subject, $legislator, $member);
        $result = preg_replace('/District [^0-9]+/', 'District ', $result);
        return $result;
    }

    public function getFormattedMessage(Request $request, AdvocacyMessage $advocacyMessage)
    {
        $data = $request->validate([
            'legislator_id' => 'required',
            'member_id' => 'required',
        ]);
        $legislator = Legislator::find($data['legislator_id']);
        $member = Member::find($data['member_id']);
        $result = \AppHelper::getFinalMessage($advocacyMessage->message, $legislator, $member);
        return $result;
    }

    public function sendReply(Request $request, AdvocacyMessage $advocacyMessage)
    {
        $data = $request->validate([
            'legislator_id' => 'required|numeric',
            'member_id' => 'required|numeric',
            'subject' => 'required|string',
            'message' => 'required|string',
        ]);
        $data['advocacy_message_id'] = $advocacyMessage->id;
        $message = new AdvocacyMemberMessage;
        $message->fill($data);
        $message->save();

        $legislator = Legislator::find($data['legislator_id']);
        $member = Member::find($data['member_id']);
        $fromEmail = strtolower($member->firstname.'_'.$member->lastname.'_'.$member->id).'@mg.kpdashboard.com';
        $fromName = 'Dr. '.$member->firstname.' '.$member->lastname;
        \Mail::to($legislator->official_email)
            ->send(new AdvocacyReplyMail($fromEmail, $fromName, $member->email, $data['subject'], $data['message']));
        return $message;
    }

    // Private Methods
    private function fillAdvocacyMemberLegislators($advocacyMessage)
    {
        AdvocacyMemberLegislator::where('advocacy_message_id', $advocacyMessage->id)
            ->delete();
        $allLegislators = Legislator::select('id', 'district_id')
            ->whereIn('id', $advocacyMessage->legislators)
            ->with('district')
            ->get();
        foreach ($advocacyMessage->members as $memberId) {
            $legislators = [];
            if ($advocacyMessage->is_kp_only) {
                $legislators = MemberLegislator::select('leg_id')
                    ->whereIn('leg_id', $advocacyMessage->legislators)
                    ->where('member_id', $memberId)
                    ->whereNull('end_date')
                    ->where('kp_position', 1)
                    ->get()
                    ->toArray();
                $legislators = array_map(function ($legislator) {
                    return $legislator['leg_id'];
                }, $legislators);
            } else {
                $member = Member::find($memberId);
                foreach ($allLegislators as $legislator) {
                    $flag = false;
                    $district = $legislator->district->district;
                    $flag |= ($member->home_house_district === $district);
                    $flag |= ($member->home_sen_district === $district);
                    $flag |= ($member->work_house_district === $district);
                    $flag |= ($member->work_sen_district === $district);
                    $flag |= ($member->work2_house_district === $district);
                    $flag |= ($member->work2_sen_district === $district);
                    $flag |= ($member->work3_house_district === $district);
                    $flag |= ($member->work3_sen_district === $district);
                    $flag |= ($member->work4_house_district === $district);
                    $flag |= ($member->work4_sen_district === $district);
                    if ($flag) {
                        $legislators[] = $legislator->id;
                    }
                }
            }
            $advocacyMemberLegislator = new AdvocacyMemberLegislator;
            $advocacyMemberLegislator->fill([
                'advocacy_message_id' => $advocacyMessage->id,
                'member_id' => $memberId,
                'legislators' => $legislators,
            ]);
            $advocacyMemberLegislator->save();
        }
    }
}
