<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use App\Http\Controllers\Controller;
use App\Models\Legislator;
use App\Models\Member;

class AppController extends Controller
{
  public function getFormattedMessage(Request $request)
  {
    $data = $request->validate([
      'legislator_id' => 'required|numeric',
      'member_id' => 'required|numeric',
      'message' => 'required',
    ]);
    $legislator = Legislator::find($data['legislator_id']);
    $member = Member::find($data['member_id']);
    $result = \AppHelper::getFinalMessage($data['message'], $legislator, $member);
    return $result;
  }
}
