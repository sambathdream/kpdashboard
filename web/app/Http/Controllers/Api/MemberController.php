<?php

namespace App\Http\Controllers\Api;

use App\Mails\KPtaskCompleted;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use App\Http\Controllers\Controller;
use App\Models\AdvocacyMessage;
use App\Models\AdvocacyMemberLegislator;
use App\Models\AdvocacyMemberMessage;
use App\Models\Client;
use App\Models\District;
use App\Models\LegData;
use App\Models\Legislator;
use App\Models\Member;
use App\Models\MemberLegislator;
use App\Models\TaskResult;

class MemberController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Member::get();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->merge(['cell' => \AppHelper::getDomesticPhoneNumber($request->input('cell'))]);
        $data = $request->validate([
            'client_id' => 'required|numeric',
            'active' => 'nullable|boolean',
            'firstname' => 'required|string',
            'lastname' => 'required|string',
            'nickname' => 'required|string',
            'cell' => 'nullable|string|unique:members',
            'office_phone' => 'nullable|string',
            'email' => 'required|string|unique:members',
            'avatar_url' => 'nullable|string',
            'eligible' => 'nullable|boolean',
            'coordinator' => 'nullable|numeric',
            'dob' => 'nullable|date',
            'grad_year' => 'nullable|numeric',
            'notes' => 'nullable|string',
            'home_address' => 'nullable|string',
            'home_lat' => 'nullable|numeric',
            'home_lng' => 'nullable|numeric',
            'home_house_district' => 'nullable|string',
            'home_sen_district' => 'nullable|string',
            'work_address' => 'nullable|string',
            'work_lat' => 'nullable|numeric',
            'work_lng' => 'nullable|numeric',
            'work_house_district' => 'nullable|string',
            'work_sen_district' => 'nullable|string',
            'work2_address' => 'nullable|string',
            'work2_lat' => 'nullable|numeric',
            'work2_lng' => 'nullable|numeric',
            'work2_house_district' => 'nullable|string',
            'work2_sen_district' => 'nullable|string',
            'work3_address' => 'nullable|string',
            'work3_lat' => 'nullable|numeric',
            'work3_lng' => 'nullable|numeric',
            'work3_house_district' => 'nullable|string',
            'work3_sen_district' => 'nullable|string',
            'work4_address' => 'nullable|string',
            'work4_lat' => 'nullable|numeric',
            'work4_lng' => 'nullable|numeric',
            'work4_house_district' => 'nullable|string',
            'work4_sen_district' => 'nullable|string',
        ]);
        $member = new Member;
        $member->fill($data);
        $member->save();
        return $member;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Member $member)
    {
        $request->merge(['cell' => \AppHelper::getDomesticPhoneNumber($request->input('cell'))]);
        $data = $request->validate([
            'client_id' => 'required|numeric',
            'active' => 'nullable|boolean',
            'firstname' => 'required|string',
            'lastname' => 'required|string',
            'nickname' => 'required|string',
            'cell' => ['nullable', 'string', Rule::unique('members')->ignore($member->id)],
            'office_phone' => 'nullable|string',
            'email' => ['required', 'string', Rule::unique('members')->ignore($member->id)],
            'avatar_url' => 'nullable|string',
            'eligible' => 'nullable|boolean',
            'coordinator' => 'nullable|numeric',
            'dob' => 'nullable|date',
            'grad_year' => 'nullable|numeric',
            'notes' => 'nullable|string',
            'home_address' => 'nullable|string',
            'home_lat' => 'nullable|numeric',
            'home_lng' => 'nullable|numeric',
            'home_house_district' => 'nullable|string',
            'home_sen_district' => 'nullable|string',
            'work_address' => 'nullable|string',
            'work_lat' => 'nullable|numeric',
            'work_lng' => 'nullable|numeric',
            'work_house_district' => 'nullable|string',
            'work_sen_district' => 'nullable|string',
            'work2_address' => 'nullable|string',
            'work2_lat' => 'nullable|numeric',
            'work2_lng' => 'nullable|numeric',
            'work2_house_district' => 'nullable|string',
            'work2_sen_district' => 'nullable|string',
            'work3_address' => 'nullable|string',
            'work3_lat' => 'nullable|numeric',
            'work3_lng' => 'nullable|numeric',
            'work3_house_district' => 'nullable|string',
            'work3_sen_district' => 'nullable|string',
            'work4_address' => 'nullable|string',
            'work4_lat' => 'nullable|numeric',
            'work4_lng' => 'nullable|numeric',
            'work4_house_district' => 'nullable|string',
            'work4_sen_district' => 'nullable|string',
        ]);
        $member->fill($data);
        $member->save();
        return $member;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Member $member)
    {
        $member->delete();
        return response()->json(true);
    }

    /**
     * Display a listing of the specific member's KP history.
     *
     * @return \Illuminate\Http\Response
     */
    public function getKPHistory(Request $request, Member $member)
    {
        $data = $request->validate([
            'client_id' => 'required|numeric',
        ]);
        return MemberLegislator::where('member_id', $member->id)
            ->where('client_id', $data['client_id'])
            ->with('legislator')
            ->get();
    }

    /**
     * Display a listing of the specific KP's assigned tasks.
     *
     * @return \Illuminate\Http\Response
     */
    public function getKPTasks(Request $request, Member $member)
    {
        $result = [];
        $myLegislators = MemberLegislator::where('member_id', $member->id)
            ->where('kp_position', 1)
            ->whereNull('end_date')
            ->with('legislator', 'legislator.district')
            ->get()
            ->toArray();
        foreach ($myLegislators as $index => $legislator) {
            $myLegislators[$index]['leg_data'] = LegData::where('leg_id', $legislator['leg_id'])
                ->where('client_id', $legislator['client_id'])
                ->first();
            $myLegislators[$index]['kps'] = MemberLegislator::where('leg_id', $legislator['leg_id'])
                ->where('client_id', $legislator['client_id'])
                ->whereNull('end_date')
                ->orderBy('kp_position', 'asc')
                ->with('member')
                ->get()
                ->toArray();
        }
        $myLegislators = array_map(function ($legislator) {
            $temp = $legislator['legislator'];
            $temp['leg_data'] = $legislator['leg_data'];
            $temp['kps'] = array_map(function ($kp) {
                $kp['member']['kp_position'] = $kp['kp_position'];
                return $kp['member'];
            }, $legislator['kps']);
            return $temp;
        }, $myLegislators);
        $client = $member->client;
        $tasks = $client->tasks($client->active_session()->id)->toArray();
        $tasks = array_filter($tasks, function ($task) {
            return !$task['hide_from_kp'] && $task['name'] !== 'F-Vote' && $task['name'] !== 'PAC Donations';
        });
        $taskList = [];
        foreach ($tasks as $task) {
            $taskList[] = $task;
        }
        $taskResults = TaskResult::where('kp_id', $member->id)->get();
        $result['legislators'] = $myLegislators;
        $result['tasks'] = $taskList;
        $result['task_results'] = $taskResults;
        return $result;
    }

    /**
     * Add/Update specific KP's tasks
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function addKPTaskResults(Request $request, Member $member)
    {
        $data = $request->validate([
            'legislator_id' => 'required|numeric',
            'results' => 'nullable|array',
        ]);
        foreach ($data['results'] as $result) {
            $taskResult = TaskResult::where('legislator_id', $data['legislator_id'])
                ->where('kp_id', $member->id)
                ->where('task_id', $result['task_id'])
                ->first();
            if (!$taskResult) {
                $taskResult = new TaskResult;
            }
            $taskResult->fill([
                'legislator_id' => $data['legislator_id'],
                'kp_id' => $member->id,
                'task_id' => $result['task_id'],
                'result' => $result['result']
            ]);
            $taskResult->save();
        }
        $member = Member::with('client')->findOrFail($member->id);

        $legislator = MemberLegislator::with('legislator')
            ->where('client_id', $member->client->id)
            ->where('kp_position', 1)
            ->where('leg_id', $data['legislator_id'])
            ->whereNull('end_date')
            ->first();
        $toEmail = null;
        if (isset($member->client->id) && $member->client->uses_regions) {
            if (isset($legislator->id)) {
                $coordinator = Member::where('client_id', $member->client->id)
                  ->where('active', 1)
                  ->whereNotNull('coordinator')
                  ->where('coordinator', $legislator->leg_region)
                  ->first();
                $toEmail = isset($coordinator->id)?$coordinator->email: $toEmail;
            }
        }
        $message = "$member->firstname $member->lastname completed the task for ".$legislator->legislator->fullname.". Log in to  ".config('app.app_alias_url')." to view.";
        if (!is_null($toEmail)) {
            \Mail::to($toEmail)->send(new KPtaskCompleted($message));
        }
        else {
            $clients = User::where('client_id', $member->client->id)->get();
            collect($clients)->map(function ($client) use ($message) {
                if (isset($client->id) && $client->getRoleNames()[0] === 'VIP')
                {
                  \Mail::to($client->email)->send(new KPtaskCompleted($message));
                }
            });
        }
        return response()->json(true);
    }

    /**
     * Search members by district
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function searchByDistrict(Request $request)
    {
        $data = $request->validate([
            'client_id' => 'required|numeric',
            'district' => 'required|string',
        ]);
        $result = [
            'district_legislator' => null,
            'member_list' => [],
        ];
        $client = Client::find($data['client_id']);
        $district = District::where('state_id', $client->state_id)
            ->where('district', $data['district'])
            ->first();
        if (is_null($district)) return response()->json($result);
        $result['district_legislator'] = Legislator::where('district_id', $district->id)->orderBy('id', 'DESC')->first();
        $result['member_list'] = Member::where('client_id', $data['client_id'])
            ->where(function ($query) use ($data) {
                $query->where('home_house_district', $data['district'])
                    ->orWhere('home_sen_district', $data['district'])
                    ->orWhere('work_house_district', $data['district'])
                    ->orWhere('work_sen_district', $data['district'])
                    ->orWhere('work2_house_district', $data['district'])
                    ->orWhere('work2_sen_district', $data['district'])
                    ->orWhere('work3_house_district', $data['district'])
                    ->orWhere('work3_sen_district', $data['district'])
                    ->orWhere('work4_house_district', $data['district'])
                    ->orWhere('work4_sen_district', $data['district']);
            })
            ->get();
        return $result;
    }

    /**
     * Display a listing of the specific member's advocacy messages.
     *
     * @return \Illuminate\Http\Response
     */
    public function getAdvocacyMessages(Member $member)
    {
        $today = date('Y-m-d');
        $startDate = date('Y-m-d H:i:s', strtotime('-7 days'));
        $advocacyMessages = AdvocacyMessage::whereJSONContains('members', $member->id)
            ->where('active', 1)
            ->whereNotNull('sent_at')
            ->where(function ($query) use ($today) {
                $query->whereNull('deadline')
                    ->orWhere('deadline', '>=', $today);
            })
            ->orderBy('sent_at', 'desc')
            ->get();
        $result = [];
        foreach($advocacyMessages as $index => $advocacyMessage) {
            // Get sent message count
            $advocacyMessage->sent_message_count = AdvocacyMemberMessage::select('legislator_id')
                ->where('advocacy_message_id', $advocacyMessage->id)
                ->where('member_id', $member->id)
                ->count();
            // Get all available legislator count
            $advocacyMessage->assigned_legislator_count = count(
                AdvocacyMemberLegislator::where('advocacy_message_id', $advocacyMessage->id)
                    ->where('member_id', $member->id)
                    ->first()
                    ->legislators
            );
            if ($advocacyMessage->sent_message_count === $advocacyMessage->assigned_legislator_count) continue;
            $result[] = $advocacyMessage;
        }
        return $result;
    }

    /**
     * Display a listing of legislators who the specific member's assigned to.
     *
     * @return \Illuminate\Http\Response
     */
    public function getAssignedLegislators(Member $member)
    {
        $legislatorList = MemberLegislator::select('leg_id')
            ->where('member_id', $member->id)
            ->whereNull('end_date')
            ->with('legislator')
            ->distinct()
            ->get()
            ->toArray();
        $legislatorList = array_map(function ($legislator) {
            return $legislator['legislator'];
        }, $legislatorList);
        return $legislatorList;
    }

    /**
     * Display a listing of the specific member's advocacy message history.
     *
     * @return \Illuminate\Http\Response
     */
    public function getAdvocacyMessageHistory(Member $member)
    {
        $result = [];
        $advocacyMemberLegislators = AdvocacyMemberLegislator::where('member_id', $member->id)
            ->with('advocacyMessage')
            ->get();
        foreach ($advocacyMemberLegislators as $advocacyMemberLegislator) {
            foreach ($advocacyMemberLegislator->legislators as $legislatorId) {
                $legislator = Legislator::find($legislatorId);
                $result[] = [
                    'advocacy_message_id' => $advocacyMemberLegislator->advocacyMessage->id,
                    'sent_at' => $advocacyMemberLegislator->advocacyMessage->sent_at.'',
                    'legislator_id' => $legislatorId,
                    'legislator_name' => $legislator->fullname,
                ];
            }
        }
        foreach ($result as $index => $row) {
            $sentMessage = AdvocacyMemberMessage::select('created_at')
                ->where('advocacy_message_id', $row['advocacy_message_id'])
                ->where('member_id', $member->id)
                ->where('legislator_id', $row['legislator_id'])
                ->first();
            if ($sentMessage) {
                $result[$index]['reply_sent_at'] = $sentMessage->created_at.'';
            }
        }
        return $result;
    }
}
