<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\District;
use App\Models\IgnoredEmail;
use App\Models\ImportedLegislator;
use App\Models\Legislator;
use App\Models\MemberLegislator;
use App\Models\State;

class StateController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return State::orderBy('state')->get();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, State $state)
    {
        $data = $request->validate([
            'has_joint_committees' => 'boolean',
        ]);
        $state->fill($data);
        $state->save();
        return $state;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * Display a listing of the state's committees.
     *
     * @return \Illuminate\Http\Response
     */
    public function getCommittees(State $state)
    {
        return $state->committees;
    }

    /**
     * Display a listing of the state's legislators.
     *
     * @return \Illuminate\Http\Response
     */
    public function getLegislators(State $state)
    {
        return $state->legislators()->with(['district', 'committees'])->get();
    }

    /**
     * Display a listing of the state's unelected districts.
     *
     * @return \Illuminate\Http\Response
     */
    public function getUnelectedDistricts(State $state)
    {
        $electedDistrictIds = Legislator::whereNull('abdicated_at')
            ->orWhere('abdicated_at', '')
            ->pluck('district_id');
        $electedDistrictIds = json_decode($electedDistrictIds);
        $electedDistrictIds = explode(',', implode(',', $electedDistrictIds));
        $unelectedDistricts = $state->districts()->whereNotIn('id', $electedDistrictIds)->get();
        return $unelectedDistricts;
    }

    public function getNewLegislators(State $state)
    {
        $result = [];
        foreach ($state->districts as $district) {
            $oldLegislator = Legislator::where('district_id', $district->id)
                ->whereNull('abdicated_at')
                ->with('district')
                ->first();
            $newLegislator = ImportedLegislator::where('district_id', $district->id)
                ->whereNull('abdicated_at')
                ->with('district')
                ->first();
            if (!$newLegislator) {
                continue;
            }
            $ignored = IgnoredEmail::where('email', $newLegislator->official_email)->first();
            if ($ignored) {
                continue;
            }
            if ($oldLegislator) {
                $oldLegislator->kp_count = MemberLegislator::where('leg_id', $oldLegislator->id)
                    ->whereNull('end_date')
                    ->count('id');
            }
            if (!$oldLegislator || $oldLegislator->official_email !== $newLegislator->official_email) {
                $result[] = [
                    'old' => $oldLegislator,
                    'new' => $newLegislator,
                ];
            }
        }
        return $result;
    }
}
