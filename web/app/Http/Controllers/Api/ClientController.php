<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use App\Http\Controllers\Controller;
use App\Imports\MembersImport;
use App\Mails\TaskMail;
use App\Models\AdvocacyMemberMessage;
use App\Models\Client;
use App\Models\District;
use App\Models\LegData;
use App\Models\Legislator;
use App\Models\Member;
use App\Models\MemberLegislator;
use App\Models\Message;
use App\Models\MessageTime;
use App\Models\Session;
use App\Models\SessionTask;
use App\Models\TaskResult;
use App\Models\UserSession;
use Maatwebsite\Excel\Facades\Excel;
use Twilio\Rest\Client as TwilioClient;

class ClientController extends Controller
{
    /**
     * Create a new instance
     *
     * @return void
     */
    public function __construct()
    {
        $this->twilioClient = new TwilioClient(env('TWILIO_SID'), env('TWILIO_TOKEN'));
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Client::with('state')->get();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->validate([
            'active' => 'nullable|boolean',
            'association' => 'required|string|unique:clients',
            'assoc_abbrev' => 'nullable|string',
            'avatar_url' => 'nullable|string',
            'profession' => 'nullable|string',
            'is_forward_email_required' => 'nullable|boolean',
            'reply_to_email' => 'nullable|string',
            'mailgun_email' => 'nullable|string',
            'forward_email' => 'nullable|string',
            'is_forward_text_required' => 'nullable|boolean',
            'twilio_number' => 'nullable|string',
            'forward_number' => 'nullable|string',
            'state_id' => 'required|numeric',
            'uses_regions' => 'nullable|boolean',
            'region_count' => 'numeric|min:0|max:100',
            'uses_coordinators' => 'nullable|boolean',
            'show_all' => 'nullable|boolean',
            'uses_kps' => 'nullable|numeric',
            'uses_dob' => 'nullable|boolean',
            'uses_grad_year' => 'nullable|boolean',
            'uses_2nd_work' => 'nullable|boolean',
            'uses_3rd_work' => 'nullable|boolean',
            'uses_4th_work' => 'nullable|boolean',
            'formal_house' => 'nullable|string',
            'formal_senate' => 'nullable|string',
            'house_committees' => 'present',
            'senate_committees' => 'present',
            'joint_committees' => 'present',
        ]);
        $client = new Client;
        $client->fill($data);
        $client->save();
        return $client;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Client $client)
    {
        $data = $request->validate([
            'active' => 'nullable|boolean',
            'association' => ['required', 'string', Rule::unique('clients')->ignore($client->id)],
            'assoc_abbrev' => 'required|string',
            'avatar_url' => 'nullable|string',
            'profession' => 'nullable|string',
            'is_forward_email_required' => 'nullable|boolean',
            'reply_to_email' => 'nullable|string',
            'mailgun_email' => 'nullable|string',
            'forward_email' => 'nullable|string',
            'is_forward_text_required' => 'nullable|boolean',
            'twilio_number' => 'nullable|string',
            'forward_number' => 'nullable|string',
            'state_id' => 'required|numeric',
            'uses_regions' => 'nullable|boolean',
            'region_count' => 'numeric|min:0|max:100',
            'uses_coordinators' => 'nullable|boolean',
            'show_all' => 'nullable|boolean',
            'uses_kps' => 'nullable|numeric',
            'uses_dob' => 'nullable|boolean',
            'uses_grad_year' => 'nullable|boolean',
            'uses_2nd_work' => 'nullable|boolean',
            'uses_3rd_work' => 'nullable|boolean',
            'uses_4th_work' => 'nullable|boolean',
            'formal_house' => 'nullable|string',
            'formal_senate' => 'nullable|string',
            'house_committees' => 'present',
            'senate_committees' => 'present',
            'joint_committees' => 'present',
        ]);
        $client->fill($data);
        $client->save();
        return $client;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Client $client)
    {
        $client->delete();
        return response()->json(true);
    }

    /**
     * Disable the specified client.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function disable(Client $client)
    {
        $client->active = false;
        $client->save();
        return $client;
    }

    /**
     * Enable the specified client.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function enable(Client $client)
    {
        $client->active = true;
        $client->save();
        return $client;
    }

    /**
     * Display a listing of the client's users.
     *
     * @return \Illuminate\Http\Response
     */
    public function getUsers(Client $client)
    {
        $users = $client->users()->with('roles')->get();
        foreach ($users as $index => $user) {
            $users[$index]->last_login = UserSession::where('client_id', $client->id)
                ->where('user_id', $user->id)
                ->max('created_at');
        }
        return $users;
    }

    /**
     * Display a listing of the client's districts.
     *
     * @return \Illuminate\Http\Response
     */
    public function getDistricts(Client $client)
    {
        $districts = $client->districts;
        collect($districts)->map(function ($district) {
          if (isset($district->legislator)) {
            $district['alias_district'] = $district->district.' -'.$district->legislator->firstname.' '.
              $district->legislator->lastname;
            return $district;
          }
        });

        return $districts;
    }

    /**
     * Display a listing of the client's sessions.
     *
     * @return \Illuminate\Http\Response
     */
    public function getSessions(Client $client)
    {
        return $client->sessions;
    }

    /**
     * Display a listing of the client's active session.
     *
     * @return \Illuminate\Http\Response
     */
    public function getActiveSession(Client $client)
    {
        return $client->active_session();
    }

  /**
   * Display a listing of the client's members.
   *
   * @return \Illuminate\Http\Response
   */
  public function getMembers(Client $client)
  {
    $members = $client->members;
    collect($members)->map(function ($member) {
      $memberLegislator = MemberLegislator::where('member_id', $member->id)
        ->where('client_id', $member->client_id)->first();
      $member['kp_member'] = false;
      if (isset($memberLegislator->id) && is_null($memberLegislator->end_date)) {
        $member['kp_member'] = true;
      }
      return $member;
    });
    return $members;
  }

    /**
     * Display a listing of the client's legislators.
     *
     * @return \Illuminate\Http\Response
     */
    public function getLegislators(Request $request, Client $client)
    {
      $legislators = $client->legislators($request->get('committees', []))->toArray();

      foreach ($legislators as $index => $legislator) {
        $legislators[$index]['kps'] = [];
        for ($i = 0; $i < $client->uses_kps; $i += 1) {
          $legislators[$index]['kps'][] = MemberLegislator::where('leg_id', $legislator['id'])
            ->where('client_id', $client->id)
            ->where('kp_position', $i + 1)
            ->whereNull('end_date')
            ->first();
        }
      }
      return $legislators;
    }

    /**
     * Display a listing of the client's assignments.
     *
     * @return \Illuminate\Http\Response
     */
    public function getAssignments(Client $client)
    {
        $legislators = Legislator::where('state_id', $client->state_id)
            ->with(['district', 'committees'])
            ->get()
            ->toArray();
        foreach ($legislators as $index => $legislator) {
            $legData = LegData::where('client_id', $client->id)
                ->where('leg_id', $legislator['id'])
                ->first();
            $legislators[$index]['leg_region'] = is_null($legData) ? null : $legData->leg_region;
            $legislators[$index]['leg_notes'] = is_null($legData) ? null : $legData->leg_notes;
            $legislators[$index]['vip_notes'] = is_null($legData) ? null : $legData->vip_notes;

            $legislators[$index]['kps'] = [];
            for ($i = 0; $i < $client->uses_kps; $i += 1) {
                $legislators[$index]['kps'][] = MemberLegislator::where('leg_id', $legislator['id'])
                    ->where('client_id', $client->id)
                    ->where('kp_position', $i + 1)
                    ->whereNull('end_date')
                    ->first();
            }
        }
        return $legislators;
    }

    /**
     * Display a listing of the client's kp tasks.
     *
     * @return \Illuminate\Http\Response
     */
    public function getKPTasks(Client $client)
    {
        $user = \Auth::user();
        $userRole = $user->getRoleNames()[0];
        $result = [];
        $stateLegislators = Legislator::where('state_id', $client->state_id)
            ->with(['district', 'committees'])
            ->whereNull('abdicated_at')
            ->get()
            ->toArray();
        $myLegislators = [];
        foreach ($stateLegislators as $index => $legislator) {
            $legislator['primary_kp'] = MemberLegislator::where('leg_id', $legislator['id'])
                ->with(['member'])
                ->where('client_id', $client->id)
                ->where('kp_position', 1)
                ->whereNull('end_date')
                ->first();
            $legData = LegData::where('client_id', $client->id)
                ->where('leg_id', $legislator['id'])
                ->first();
            $legislator['leg_region'] = is_null($legData) ? null : $legData->leg_region;
            $legislator['leg_notes'] = is_null($legData) ? null : $legData->leg_notes;
            $legislator['vip_notes'] = is_null($legData) ? null : $legData->vip_notes;
            if ($userRole === 'Coordinator' && !$client->show_all && ($user->member->coordinator !== $legislator['leg_region'])) continue;
            $legislator['coordinator'] = Member::select('firstname', 'lastname', 'nickname', 'cell', 'email')
                ->where('client_id', $client->id)
                ->where('active', 1)
                ->whereNotNull('coordinator')
                ->where('coordinator', $legislator['leg_region'])
                ->first();
            $myLegislators[] = $legislator;
        }
        if (is_null($client->active_session())) {
            $result['legislators'] = $myLegislators;
            $result['tasks'] = [];
            return $result;
        }
        $tasks = $client->tasks($client->active_session()->id);
        $taskIds = [];
        foreach ($tasks as $task) {
            $taskIds[] = $task->id;
        }
        $taskResults = TaskResult::whereIn('task_id', $taskIds)->get();
        $result['session'] = $client->active_session();
        $result['legislators'] = $myLegislators;
        $result['tasks'] = $tasks;
        $result['task_results'] = $taskResults;
        return $result;
    }

    /**
     * Display a listing of the messages between client and a member.
     *
     * @return \Illuminate\Http\Response
     */
    function getMessages(Client $client, Request $request)
    {
        $data = $request->validate([
            'member_id' => 'required|numeric',
        ]);
        $messages = Message::where('client_id', $client->id)
            ->where('member_id', $data['member_id'])
            ->orderBy('created_at', 'asc')
            ->get();
        return $messages;
    }

    /**
     * Display a listing of the message members.
     *
     * @return \Illuminate\Http\Response
     */
    function getMessageMembers(Client $client, Request $request)
    {
        $user = \Auth::user();
        $userRole = $user->getRoleNames()[0];
        $searchText = $request->input('search_text');
        $userId = \Auth::user()->id;
        $memberIds = [];
        $uniqueMembers = [];
        if (true) {
            $messages = $client->messages()
                ->orderBy('created_at', 'desc')
                ->with(['member'])
                ->get();
            foreach ($messages as $message) {
                if (in_array($message->member_id, $memberIds)) continue;
                $member = $message->member;
                $query = "
                    SELECT count(`messages`.id) as message_count
                    FROM `members`, `messages`
                    WHERE `messages`.client_id = ".$client->id."
                        AND `messages`.member_id = ".$member->id."
                        AND `messages`.member_id = `members`.id
                        AND (
                            UPPER(`messages`.message) LIKE '%".$searchText."%'
                            OR UPPER(`members`.firstname) LIKE '%".$searchText."%'
                            OR UPPER(`members`.lastname) LIKE '%".$searchText."%'
                        );
                ";
                if (!\DB::select(\DB::raw($query))[0]->message_count) continue;
                $member->last_message = [
                    'message' => $message->message,
                    'created_at' => $message->created_at->format('Y-m-d H:i:s'),
                ];
                $email_last_seen_at = MessageTime::where('user_id', $userId)
                    ->where('member_id', $member->id)
                    ->where('is_sms', 0)
                    ->first();
                if ($email_last_seen_at) {
                    $email_last_seen_at = $email_last_seen_at->last_seen_at;
                } else {
                    $email_last_seen_at = '1970-01-01 00:00:00';
                }
                $member->unseen_email_count = Message::where('client_id', $client->id)
                    ->where('member_id', $member->id)
                    ->where('is_sms', 0)
                    ->where('is_reply', 1)
                    ->where('created_at', '>', $email_last_seen_at)
                    ->count();
                $text_last_seen_at = MessageTime::where('user_id', $userId)
                    ->where('member_id', $member->id)
                    ->where('is_sms', 1)
                    ->first();
                if ($text_last_seen_at) {
                    $text_last_seen_at = $text_last_seen_at->last_seen_at;
                } else {
                    $text_last_seen_at = '1970-01-01 00:00:00';
                }
                $member->unseen_text_count = Message::where('client_id', $client->id)
                    ->where('member_id', $member->id)
                    ->where('is_sms', 1)
                    ->where('is_reply', 1)
                    ->where('created_at', '>', $text_last_seen_at)
                    ->count();
                $memberIds[] = $member->id;

                if ($member->coordinator) {
                    $query = "
                        SELECT members.*
                        FROM members, users
                        WHERE members.client_id = {$member->client_id}
                            AND members.coordinator = {$member->coordinator}
                            AND members.email = users.email
                            AND users.active = 1
                    ";
                    $member->coordinator_user = \DB::select(\DB::raw($query));
                    if (count($member->coordinator_user)) {
                        $member->coordinator_user = $member->coordinator_user[0];
                    }
                } else {
                    $member->coordinator_user = null;
                }

                $uniqueMembers[] = $member;
            }
        }
        // VIP
        if ($userRole === 'VIP') {
            $member = Member::where('email', $user->email)->first();
            if ($member) {
                $coordinator = $member->coordinator;
                $query = "
                    SELECT count(members.id), members.id
                    FROM members, member_legislator, leg_data
                    WHERE member_legislator.client_id = {$client->id}
                        AND member_legislator.kp_position = 1
                        AND member_legislator.end_date IS NULL
                        AND member_legislator.member_id = members.id
                        AND member_legislator.leg_id = leg_data.leg_id
                        AND leg_data.client_id = {$client->id}
                        AND leg_data.leg_region = {$coordinator}
                    GROUP BY members.id;
                ";
                $myKPList = \DB::select(\DB::raw($query));
                foreach ($uniqueMembers as $i => $uniqueMember) {
                    $members = MemberLegislator::where('member_id', $uniqueMember->id)
                        ->where('kp_position', 1)
                        ->whereNull('end_date')
                        ->with('legislator')
                        ->get()
                        ->toArray();
                    $legislatorIds = array_map(function ($member) {
                        return $member['legislator']['id'];
                    }, $members);
                    $legNotes = LegData::where('client_id', $client->id)
                        ->whereIn('leg_id', $legislatorIds)
                        ->get()
                        ->toArray();
                    $legRegions = array_map(function ($legNote) {
                        return $legNote['leg_region'];
                    }, $legNotes);
                    $uniqueMembers[$i]->leg_regions = $legRegions;

                    foreach ($myKPList as $myKP) {
                        if ($myKP->id === $uniqueMember->id) {
                            $uniqueMembers[$i]->is_my_kp = true;
                        }
                    }
                }
            }
        }
        // Coordinator
        if ($userRole === 'Coordinator') {
            $query = "
                SELECT DISTINCT member_legislator.member_id AS member_id
                FROM leg_data, member_legislator
                WHERE leg_data.client_id = {$client->id}
                    AND leg_data.leg_region = {$user->member->coordinator}
                    AND member_legislator.leg_id = leg_data.leg_id
                    AND member_legislator.client_id = {$client->id}
                    AND member_legislator.kp_position = 1
                    AND member_legislator.end_date IS NULL;
            ";
            $regionMemberIds = \DB::select(\DB::raw($query));
            $regionMemberIds = array_map(function ($regionMember) {
                return $regionMember->member_id;
            }, $regionMemberIds);
            $regionMembers = [];
            foreach ($uniqueMembers as $member) {
                if (in_array($member->id, $regionMemberIds)) {
                    $regionMembers[] = $member;
                }
            }
            $uniqueMembers = $regionMembers;
        }
        return $uniqueMembers;
    }

    /**
     * Get unseen message count
     *
     * @return \Illuminate\Http\Response
     */
    function getUnseenMessageCount(Client $client, Request $request)
    {
        $user = \Auth::user();
        $userId = $user->id;
        $userRole = $user->getRoleNames()[0];
        // VIP
        if ($userRole === 'VIP') {
            $query = "
                SELECT count(message_id) AS message_count
                    FROM (
                            SELECT id message_id, client_id, member_id, is_reply, is_sms, created_at
                            FROM messages
                            WHERE client_id = ".$client->id." AND is_reply = 1
                        ) AS messages1
                        LEFT OUTER JOIN
                        (
                            SELECT user_id, member_id, is_sms, last_seen_at
                            FROM message_times
                            WHERE user_id = ".$userId."
                        ) AS message_times1
                        ON messages1.member_id = message_times1.member_id AND messages1.is_sms = message_times1.is_sms
                    WHERE last_seen_at IS NULL OR created_at > last_seen_at
            ";
            return \DB::select(\DB::raw($query))[0]->message_count;
        }
        // Coordinator
        if ($userRole === 'Coordinator') {
            $query = "
                SELECT DISTINCT member_legislator.member_id AS member_id
                FROM leg_data, member_legislator
                WHERE leg_data.client_id = {$client->id}
                    AND leg_data.leg_region = {$user->member->coordinator}
                    AND member_legislator.leg_id = leg_data.leg_id
                    AND member_legislator.client_id = {$client->id}
                    AND member_legislator.kp_position = 1
                    AND member_legislator.end_date IS NULL;
            ";
            $regionMemberIds = \DB::select(\DB::raw($query));
            $regionMemberIds = array_map(function ($regionMember) {
                return $regionMember->member_id;
            }, $regionMemberIds);
            if (!count($regionMemberIds)) {
                return 0;
            }
            $regionMemberIds = '('.implode($regionMemberIds, ',').')';

            $query = "
                SELECT count(message_id) AS message_count
                    FROM (
                            SELECT id message_id, client_id, member_id, is_reply, is_sms, created_at
                            FROM messages
                            WHERE client_id = ".$client->id." AND is_reply = 1
                        ) AS messages1
                        LEFT OUTER JOIN
                        (
                            SELECT user_id, member_id, is_sms, last_seen_at
                            FROM message_times
                            WHERE user_id = ".$userId." AND member_id IN {$regionMemberIds}
                        ) AS message_times1
                        ON messages1.member_id = message_times1.member_id AND messages1.is_sms = message_times1.is_sms
                    WHERE last_seen_at IS NULL OR created_at > last_seen_at
            ";
            return \DB::select(\DB::raw($query))[0]->message_count;
        }
    }

    /**
     * Send email to members
     *
     * @return \Illuminate\Http\Response
     */
    function sendEmail(Client $client, Request $request)
    {
        $data = $request->validate([
            'legislators' => 'nullable|array',
            'tos' => 'required|array',
            'from' => 'required|string',
            'reply_to_email' => 'nullable|string',
            'subject' => 'required|string',
            'message' => 'required|string',
        ]);

        if ($client->mailgun_email && !$request->has('personalReplies')) {
          $replyToEmail = $client->mailgun_email;
        } else {
          $replyToEmail = $data['reply_to_email'];
        }

        if (array_key_exists('legislators', $data)) {
            foreach ($data['tos'] as $index => $to) {
                $legislator = Legislator::find($data['legislators'][$index]);
                $member = Member::where('email', $to)->first();
                $message = \AppHelper::getFinalMessage($data['message'], $legislator, $member);
                \Mail::to($member->email)
                    ->send(new TaskMail($data['from'], $replyToEmail, $data['subject'], $message));
                if ($client->mailgun_email) {
                    Message::create([
                        'client_id' => $client->id,
                        'member_id' => $member->id,
                        'is_reply' => false,
                        'is_sms' => false,
                        'sender' => $client->mailgun_email,
                        'receiver' => $member->email,
                        'message' => $message,
                    ]);
                }
            }
        } else {
            \Mail::bcc($data['tos'])
                ->send(new TaskMail($data['from'], $replyToEmail, $data['subject'], $data['message']));
            if ($client->mailgun_email) {
                $members = Member::whereIn('email', $data['tos'])->get();
                foreach ($members as $member) {
                    Message::create([
                        'client_id' => $client->id,
                        'member_id' => $member->id,
                        'is_reply' => false,
                        'is_sms' => false,
                        'sender' => $client->mailgun_email,
                        'receiver' => $member->email,
                        'message' => $data['message'],
                    ]);
                }
            }
        }
        return response()->json(true);
    }

    /**
     * Send text to members
     *
     * @return \Illuminate\Http\Response
     */
    function sendText(Client $client, Request $request)
    {
        $data = $request->validate([
            'legislators' => 'nullable|array',
            'tos' => 'required|array',
            'message' => 'required|string',
        ]);
        $smsBody = [
            'from' => \AppHelper::getInternationalPhoneNumber($client->twilio_number),
            'body' => $data['message'],
        ];
        foreach ($data['tos'] as $index => $to) {
            if (preg_match('/^BAD_/', $to)) {
              continue;
            }
            $member = Member::where('cell', $to)->first();
            $smsBody['body'] = $data['message'];
            if (array_key_exists('legislators', $data)) {
                $legislator = Legislator::find($data['legislators'][$index]);
                $smsBody['body'] = \AppHelper::getFinalMessage($data['message'], $legislator, $member);
            }
            try {
                $this->twilioClient->messages->create(\AppHelper::getInternationalPhoneNumber($to), $smsBody);
                // Log Text
                Message::create([
                    'client_id' => $client->id,
                    'member_id' => $member->id,
                    'is_reply' => false,
                    'is_sms' => true,
                    'sender' => $client->twilio_number,
                    'receiver' => $member->cell,
                    'message' => $smsBody['body'],
                ]);
            } catch (\Exception $e) {
                Member::where('cell', $to)
                    ->update([
                        'cell' => 'BAD_'.$to
                    ]);
            }
        }
        return response()->json(true);
    }

    /**
     * Import members from file
     *
     * @return \Illuminate\Http\Response
     */
    function importMembers(Client $client, Request $request)
    {
        \Config::set('excel.imports.csv.delimiter', '|');
        $path = $request->file('file')->storeAs('imports/members', $request->user()->id);
        $membersImport = new MembersImport($client->id);
        try {
            $membersImport->import($path, null, \Maatwebsite\Excel\Excel::CSV);
        } catch (\Maatwebsite\Excel\Validators\ValidationException $e) {
            return response()->json(false);
        }
        $result = [
            'success' => $membersImport->getImportedCount(),
            'failure' => count($membersImport->failures()),
        ];
        return $result;
    }

    /**
     * Get specific client's status
     *
     * @return \Illuminate\Http\Response
     */
    function getStatus(Client $client)
    {
        $activeSession = $client->active_session();
        $result = [];
        $memberCount = Member::where('client_id', $client->id)
            ->where('active', 1)
            ->count();
        $result['members_imported'] = !!$memberCount;
        $legDataCount = LegData::where('client_id', $client->id)
            ->count();
        $result['legislators_edited'] = !!$legDataCount;
        $memberLegislatorCount = MemberLegislator::where('client_id', $client->id)
            ->count();
        $result['kps_assigned'] = !!$memberLegislatorCount;
        $result['sessions_created'] = !is_null($activeSession);
        if ($result['sessions_created']) {
            $taskCount = SessionTask::where('session_id', $activeSession->id)
                ->count();
            $result['tasks_created'] = !!$taskCount;
        } else {
            $result['tasks_created'] = false;
        }
        if ($result['tasks_created']) {
            $query = "
                SELECT COUNT(`task_results`.id) AS result_count
                FROM `session_tasks`, `task_results`
                WHERE `session_tasks`.session_id = ".$activeSession->id." AND `session_tasks`.id = `task_results`.task_id
            ";
            $result['results_recorded'] = !!\DB::select(\DB::raw($query))[0]->result_count;
        } else {
            $result['results_recorded'] = false;
        }
        return $result;
    }

    /**
     * Get specific client's advocacy messages
     *
     * @return \Illuminate\Http\Response
     */
    public function getAdvocacyMessages(Client $client)
    {
        $advocacyMessages = $client->advocacyMessages()
            ->orderBy('created_at', 'desc')
            ->get();
        foreach ($advocacyMessages as $index => $advocacyMessage) {
            $advocacyMessage->total_message_count = 0;
            $advocacyMessage->sent_message_count = AdvocacyMemberMessage::where('advocacy_message_id', $advocacyMessage->id)->count();
            foreach ($advocacyMessage->advocacyMemberLegislators as $advocacyMemberLegislator) {
                $advocacyMessage->total_message_count += count($advocacyMemberLegislator->legislators);
            }
            $advocacyMessages[$index] = $advocacyMessage;
        }
        return $advocacyMessages;
    }

    /**
     * Get specific client's user sessions
     *
     * @return \Illuminate\Http\Response
     */
    public function getUserSessions(Client $client)
    {
        $result = $client->user_sessions()->with(['client', 'user', 'user.roles'])->get();
        foreach($result as $key => $row) {
            $result[$key]->user->role = $row->user->roles[0]->name;
        }
        return $result;
    }

    /**
     * Update specific client's all districts using google api.
     *
     * @return \Illuminate\Http\Response
     */
    public function updateDistricts(Client $client)
    {
        $stateAbbr = $client->state->state_abbrev;
        $result = [
            'updated' => 0,
            'skipped' => 0,
        ];
        foreach ($client->members as $member) {
            $isUpdated = false;
            $prefixList = ['home_', 'work_', 'work2_', 'work3_', 'work4_'];
            foreach ($prefixList as $prefix) {
                $addressKey = $prefix.'address';
                $addressLat = $prefix.'lat';
                $addressLng = $prefix.'lng';
                $senDistrictKey = $prefix.'sen_district';
                $houseDistrictKey = $prefix.'house_district';
                $conDistrictKey = $prefix.'con_district';
                $federalSenate = $prefix.'federal_senate';
                if ($member->{$addressKey}) {
                    $response = \AppHelper::searchDistrictByAddress($member->{$addressKey});
                    if (!array_key_exists('results', $response)) {
                        $member->{$addressKey} = null;
                        $result['skipped'] += 1;
                        continue;
                    }
                    $response = $response['results'][0];
                    if (strtolower($response['address_components']['state']) !== $stateAbbr) {
                        $member->{$addressKey} = null;
                        $result['skipped'] += 1;
                        continue;
                    }
                    $stateLegislativeDistricts = $response['fields']['state_legislative_districts'];
                    $member->{$senDistrictKey} = null;
                    $member->{$houseDistrictKey} = null;
                    if (array_key_exists('senate', $stateLegislativeDistricts)) {
                        $length = strlen((string)$client->state->sen_districts);
                        $districtNumber = $stateLegislativeDistricts['senate']['district_number'];
                        $districtNumber = str_pad($districtNumber, $length, '0', STR_PAD_LEFT);
                        $member->{$senDistrictKey} = 'S'.$districtNumber;
                    }
                    if (array_key_exists('house', $stateLegislativeDistricts)) {
                        $length = strlen((string)$client->state->house_districts);
                        $districtNumber = $stateLegislativeDistricts['house']['district_number'];
                        $districtNumber = str_pad($districtNumber, $length, '0', STR_PAD_LEFT);
                        $member->{$houseDistrictKey} = 'H'.$districtNumber;
                    }
                    $member->{$addressKey} = $response['formatted_address'];
                    $member->{$addressLat} = $response['location']['lat'];
                    $member->{$addressLng} = $response['location']['lng'];

                    $congressionalDistricts = $response['fields']['congressional_districts'];
                    if (count($congressionalDistricts) > 0) {
                        $ocdId = 'ocd-division/country:us/state:'.$stateAbbr.'/cd:'.$congressionalDistricts[0]['district_number'];
                        $district = District::where('ocd_id', $ocdId)->first();
                        $member->{$conDistrictKey} = $district->district;
                    }
                    $member->{$federalSenate} = $response['address_components']['state'];
                    $result['updated'] += 1;
                }
            }
            $member->save();
        }
        return $result;
    }

    /**
     * Display a client's KP leaderboard data
     *
     * @return \Illuminate\Http\Response
     */
    public function getKPLeaderboardData(Client $client)
    {
        // KP1 Information
        $query = "
            SELECT DISTINCT members.id, members.nickname, members.lastname, members.cell, members.email
            FROM member_legislator, members
            WHERE member_legislator.client_id = {$client->id}
                AND member_legislator.kp_position = 1
                AND member_legislator.member_id = members.id
            ORDER BY members.id;
        ";
        $kpList = \DB::select(\DB::raw($query));
        // KP1's current legislators
        $query = "
            SELECT COUNT(member_legislator.leg_id) leg_count, member_legislator.member_id
            FROM member_legislator
            WHERE member_legislator.client_id = {$client->id}
                AND member_legislator.kp_position = 1
                AND member_legislator.end_date IS NULL
            GROUP BY member_legislator.member_id;
        ";
        $kpCurrentLegislatorList = \DB::select(\DB::raw($query));
        // KP1's lifetime legislators
        $query = "
            SELECT COUNT(member_legislator.leg_id) leg_count, member_legislator.member_id
            FROM member_legislator
            WHERE member_legislator.client_id = {$client->id}
                AND member_legislator.kp_position = 1
            GROUP BY member_legislator.member_id;
        ";
        $kpLifetimeLegislatorList = \DB::select(\DB::raw($query));
        // KP1's last updated date
        $query = "
            SELECT MAX(task_results.updated_at) last_updated_at, task_results.kp_id member_id
            FROM task_results, session_tasks, sessions, task_types
            WHERE task_results.task_id = session_tasks.id
                AND session_tasks.session_id = sessions.id
                AND sessions.client_id = {$client->id}
                AND session_tasks.task_type_id = task_types.id
                AND task_types.data_type IN ('Boolean', 'String', 'Select')
            GROUP BY task_results.kp_id;
        ";
        $kpLastUpdatedDateList = \DB::select(\DB::raw($query));

        // KP1's session stats
        $activeSessionTaskCount = 0;
        $kpActiveSessionAssignedLegislatorList = [];
        $kpActiveSessionAssignedTaskList = [];
        $kpActiveSessionFinishedTaskList = [];
        if ($client->active_session()) {
            $query = "
                SELECT COUNT(session_tasks.id) session_task_count
                FROM session_tasks, task_types
                WHERE session_tasks.session_id = {$client->active_session()->id}
                    AND session_tasks.name <> 'F-Vote'
                    AND session_tasks.task_type_id = task_types.id
                    AND task_types.data_type IN ('Boolean', 'String', 'Select');
            ";
            $activeSessionTaskCount = \DB::select(\DB::raw($query))[0]->session_task_count;

            $query = "
                SELECT COUNT(member_legislator.leg_id) leg_count, session_tasks.session_id, member_legislator.member_id
                FROM session_tasks, sessions, task_types, member_legislator
                WHERE session_tasks.name <> 'F-Vote'
                    AND session_tasks.session_id = {$client->active_session()->id}
                    AND session_tasks.session_id = sessions.id
                    AND session_tasks.task_type_id = task_types.id
                    AND task_types.data_type IN ('Boolean', 'String', 'Select')
                    AND session_tasks.deadline >= member_legislator.start_date
                    AND member_legislator.start_date <= sessions.end_at
                GROUP BY member_legislator.member_id, session_tasks.session_id;
            ";
            $kpActiveSessionAssignedLegislatorList = \DB::select(\DB::raw($query));
            foreach ($kpActiveSessionAssignedLegislatorList as $kpActiveSessionAssignedLegislator) {
                if (!isset($kpActiveSessionAssignedTaskList[$kpActiveSessionAssignedLegislator->member_id])) {
                    $kpActiveSessionAssignedTaskList[$kpActiveSessionAssignedLegislator->member_id] = 0;
                }
                $kpActiveSessionAssignedTaskList[$kpActiveSessionAssignedLegislator->member_id] += $kpActiveSessionAssignedLegislator->leg_count * $activeSessionTaskCount;
            }

            $query = "
                SELECT COUNT(task_results.id) task_result_count, task_results.kp_id member_id
                FROM task_results, session_tasks, sessions, task_types, member_legislator
                WHERE task_results.result <> 'NO'
                    AND task_results.task_id = session_tasks.id
                    AND session_tasks.session_id = {$client->active_session()->id}
                    AND session_tasks.name <> 'F-Vote'
                    AND session_tasks.session_id = sessions.id
                    AND session_tasks.task_type_id = task_types.id
                    AND task_types.data_type IN ('Boolean', 'String', 'Select')
                    AND task_results.kp_id = member_legislator.member_id
                    AND session_tasks.deadline >= member_legislator.start_date
                    AND member_legislator.start_date <= sessions.end_at
                GROUP BY task_results.kp_id;
            ";
            $kpActiveSessionFinishedTaskList = \DB::select(\DB::raw($query));
        }
        // KP1's lifetime stats
        $sessionTaskList = [];
        $kpLifetimeAssignedLegislatorList = [];
        $kpLifetimeAssignedTaskList = [];
        $kpLifetimeFinishedTaskList = [];
        $query = "
            SELECT COUNT(session_tasks.id) session_task_count, session_tasks.session_id
            FROM session_tasks, sessions, task_types
            WHERE session_tasks.name <> 'F-Vote'
                AND session_tasks.task_type_id = task_types.id
                AND task_types.data_type IN ('Boolean', 'String', 'Select')
                AND session_tasks.session_id = sessions.id
                AND sessions.client_id = {$client->id}
            GROUP BY session_tasks.session_id;
        ";
    $sessionTaskList = \DB::select(\DB::raw($query));

        $query = "
            SELECT COUNT(member_legislator.leg_id) leg_count, session_tasks.session_id, member_legislator.member_id
            FROM session_tasks, sessions, task_types, member_legislator
            WHERE session_tasks.name <> 'F-Vote'
                AND session_tasks.session_id = sessions.id
                AND sessions.client_id = {$client->id}
                AND session_tasks.task_type_id = task_types.id
                AND task_types.data_type IN ('Boolean', 'String', 'Select')
                AND session_tasks.deadline >= member_legislator.start_date
                AND member_legislator.start_date <= sessions.end_at
            GROUP BY member_legislator.member_id, session_tasks.session_id;
        ";
        $kpLifetimeAssignedLegislatorList = \DB::select(\DB::raw($query));

        foreach ($kpLifetimeAssignedLegislatorList as $kpLifetimeAssignedLegislator) {
            foreach ($sessionTaskList as $sessionTask) {
                if ($kpLifetimeAssignedLegislator->session_id === $sessionTask->session_id) {
                    $kpLifetimeAssignedLegislator->session_task_count = $kpLifetimeAssignedLegislator->leg_count * $sessionTask->session_task_count;
                }
            }
        }
        foreach ($kpLifetimeAssignedLegislatorList as $kpLifetimeAssignedLegislator) {
            if (!isset($kpLifetimeAssignedTaskList[$kpLifetimeAssignedLegislator->member_id])) {
                $kpLifetimeAssignedTaskList[$kpLifetimeAssignedLegislator->member_id] = 0;
            }
            $kpLifetimeAssignedTaskList[$kpLifetimeAssignedLegislator->member_id] += $kpLifetimeAssignedLegislator->session_task_count;
        }

        $query = "
            SELECT COUNT(task_results.id) task_result_count, task_results.kp_id member_id
            FROM task_results, session_tasks, sessions, task_types, member_legislator
            WHERE task_results.result <> 'NO'
                AND task_results.task_id = session_tasks.id
                AND session_tasks.name <> 'F-Vote'
                AND session_tasks.session_id = sessions.id
                AND sessions.client_id = {$client->id}
                AND session_tasks.task_type_id = task_types.id
                AND task_types.data_type IN ('Boolean', 'String', 'Select')
                AND task_results.kp_id = member_legislator.member_id
                AND session_tasks.deadline >= member_legislator.start_date
                AND member_legislator.start_date <= sessions.end_at
            GROUP BY task_results.kp_id;
        ";
        $kpLifetimeFinishedTaskList = \DB::select(\DB::raw($query));
        // KP1's total donations
        $query = "
            SELECT SUM(task_results.result) total_donation, task_results.kp_id member_id
            FROM task_results, session_tasks, sessions, task_types
            WHERE task_results.task_id = session_tasks.id
                AND session_tasks.session_id = sessions.id
                AND sessions.client_id = {$client->id}
                AND session_tasks.task_type_id = task_types.id
                AND task_types.data_type = 'Currency'
            GROUP BY task_results.kp_id;
        ";
        $kpTotalDonationList = \DB::select(\DB::raw($query));

        foreach ($kpList as $kp) {
            $kp->current_legislator_count = 0;
            $kp->lifetime_legislator_count = 0;
            $kp->inactive_days = -1;
            $kp->active_session_finished_task_count = 0;
            $kp->lifetime_finished_task_count = 0;
            $kp->total_donation = 0;
            $kp->active_session_task_count = isset($kpActiveSessionAssignedTaskList[$kp->id]) ? $kpActiveSessionAssignedTaskList[$kp->id] : 0;
            $kp->lifetime_task_count = isset($kpLifetimeAssignedTaskList[$kp->id]) ? $kpLifetimeAssignedTaskList[$kp->id] : 0;

            foreach ($kpCurrentLegislatorList as $kpCurrentLegislator) {
                if ($kp->id === $kpCurrentLegislator->member_id) {
                    $kp->current_legislator_count = $kpCurrentLegislator->leg_count;
                }
            }
            foreach ($kpLifetimeLegislatorList as $kpLifetimeLegislator) {
                if ($kp->id === $kpLifetimeLegislator->member_id) {
                    $kp->lifetime_legislator_count = $kpLifetimeLegislator->leg_count;
                }
            }
            foreach ($kpLastUpdatedDateList as $kpLastUpdatedDate) {
                if ($kp->id === $kpLastUpdatedDate->member_id) {
                    $now = new \DateTime();
                    $lastUpdatedAt = new \DateTime($kpLastUpdatedDate->last_updated_at);
                    $kp->inactive_days = $now->diff($lastUpdatedAt)->format('%a');
                }
            }
            foreach ($kpActiveSessionFinishedTaskList as $kpActiveSessionFinishedTask) {
                if ($kp->id === $kpActiveSessionFinishedTask->member_id) {
                    $kp->active_session_finished_task_count = $kpActiveSessionFinishedTask->task_result_count;
                }
            }
            foreach ($kpLifetimeFinishedTaskList as $kpLifetimeFinishedTask) {
                if ($kp->id === $kpLifetimeFinishedTask->member_id) {
                    $kp->lifetime_finished_task_count = $kpLifetimeFinishedTask->task_result_count;
                }
            }
            foreach ($kpTotalDonationList as $kpTotalDonation) {
                if ($kp->id === $kpTotalDonation->member_id) {
                    $kp->total_donation = $kpTotalDonation->total_donation;
                }
            }
        }

        return $kpList;
    }

    /**
     * Display a client's KPs
     *
     * @return \Illuminate\Http\Response
     */
    public function getMyKPs(Client $client)
    {
        $user = \Auth::user();
        $userRole = $user->getRoleNames()[0];
        $activeSession = $client->active_session();
        $result = [];
        if ($activeSession) {
            $result['tasks'] = $client->tasks($client->active_session()->id);
        }

        if ($userRole === 'Coordinator') {
            $coordinator = $user->member->coordinator;
        } else if ($userRole === 'VIP') {
            $member = Member::where('email', $user->email)->first();
            if ($member) {
                $coordinator = $member->coordinator;
            } else {
                return response()->json('Forbidden Request', 422);
            }
        }
        $query = "
            SELECT count(members.id), members.id, members.nickname, members.lastname, members.avatar_url, members.email, members.cell, members.office_phone, members.notes, members.dob
            FROM members, member_legislator, leg_data
            WHERE member_legislator.client_id = {$client->id}
                AND member_legislator.kp_position = 1
                AND member_legislator.end_date IS NULL
                AND member_legislator.member_id = members.id
                AND member_legislator.leg_id = leg_data.leg_id
                AND leg_data.client_id = {$client->id}
                AND leg_data.leg_region = {$coordinator}
            GROUP BY members.id;
        ";
        $myKPList = \DB::select(\DB::raw($query));
        foreach ($myKPList as $index => $myKP) {
            $members = MemberLegislator::where('member_id', $myKP->id)
                ->where('kp_position', 1)
                ->whereNull('end_date')
                ->with('legislator')
                ->get()
                ->toArray();
            $myKPList[$index]->legislators = array_map(function ($member) {
                $member['legislator']['notes'] = $member['notes'];
                return $member['legislator'];
            }, $members);
        }
        $result['myKPList'] = $myKPList;

        return $result;
    }
}
