<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Client;
use App\Models\Member;
use App\Models\Message;
use Twilio\Rest\Client as TwilioClient;

class TwilioController extends Controller
{
  /**
   * Create a new instance
   *
   * @return void
   */
  public function __construct()
  {
    $this->twilioClient = new TwilioClient(env('TWILIO_SID'), env('TWILIO_TOKEN'));
  }

  public function notify(Request $request)
  {
    $data = $request->all();
    $sender = $data['From'];
    $receiver = $data['To'];
    $client = Client::where('twilio_number', \AppHelper::getDomesticPhoneNumber($receiver))->first();
    $member = Member::where('cell', \AppHelper::getDomesticPhoneNumber($sender))->first();
    if (!$client || !$member) return response()->json(true);
    Message::create([
      'client_id' => $client->id,
      'member_id' => $member->id,
      'is_reply' => true,
      'is_sms' => true,
      'sender' => $sender,
      'receiver' => $receiver,
      'message' => $data['Body'],
    ]);
    if ($client->forward_number) {
      $smsBody = [
        'from' => $receiver,
        'body' => $data['Body'],
      ];
      $this->twilioClient->messages->create(\AppHelper::getInternationalPhoneNumber($client->forward_number), $smsBody);
    }
  }
}
