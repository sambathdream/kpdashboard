<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\District;
use App\Models\State;

class DistrictController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return District::get();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * Search district by google address
     *
     */
    public function searchDistrictByAddress(Request $request)
    {
        $data = $request->validate([
            'state_id' => 'required|numeric',
            'address' => 'required|string',
        ]);
        $response = \AppHelper::searchDistrictByAddress($data['address']);
        $result = [
            'sen_district' => null,
            'house_district' => null,
            'con_district' => null,
            'federal_senate' => null,
            'location' => null,
        ];
        if (array_key_exists('results', $response)) {
            $response = $response['results'][0];
            $state = State::where('state_abbrev', strtolower($response['address_components']['state']))->first();
            if ($state && $state->id !== $data['state_id']) {
                return response()->json(['message' => 'Seems you entered invalid address.'], 400);
            }
            $stateLegislativeDistricts = $response['fields']['state_legislative_districts'];
            if (array_key_exists('senate', $stateLegislativeDistricts)) {
                $length = strlen((string)$state->sen_districts);
                $districtNumber = $stateLegislativeDistricts['senate']['district_number'];
                $districtNumber = str_pad($districtNumber, $length, '0', STR_PAD_LEFT);
                $district = District::where('state_id', $state->id)
                    ->where('district', 'S'.$districtNumber)
                    ->first();
                $result['sen_district'] = $district;
            }
            if (array_key_exists('house', $stateLegislativeDistricts)) {
                $length = strlen((string)$state->house_districts);
                $districtNumber = $stateLegislativeDistricts['house']['district_number'];
                $districtNumber = str_pad($districtNumber, $length, '0', STR_PAD_LEFT);
                $district = District::where('state_id', $state->id)
                    ->where('district', 'H'.$districtNumber)
                    ->first();
                $result['house_district'] = $district;
            }
            $stateCongressionalDistricts = $response['fields']['congressional_districts'];
            if (count($stateCongressionalDistricts) > 0) {
                $count = District::where('state_id', $state->id)
                    ->where('name', 'like', '% congressional district')
                    ->count();
                $length = strlen((string)$count);
                $districtNumber = $stateCongressionalDistricts[0]['district_number'];
                $districtNumber = str_pad($districtNumber, $length, '0', STR_PAD_LEFT);
                $district = District::where('state_id', $state->id)
                    ->where('district', 'F'.$districtNumber)
                    ->first();
                $result['con_district'] = $district;
            }
            $result['location'] = $response['location'];
        }
        return response()->json($result);
    }
}
