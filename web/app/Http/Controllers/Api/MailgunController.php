<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Mails\TaskMail;
use App\Models\Client;
use App\Models\Member;
use App\Models\Message;
use Illuminate\Support\Facades\Storage;

class MailgunController extends Controller
{
  public function notify(Request $request)
  {
    $data = $request->all();
    $sender = $data['sender'];
    $receiver = $data['recipient'];
    $client = Client::where('mailgun_email', $receiver)->first();
    $member = Member::where('email', $sender)->first();
    if (!$client || !$member) return response()->json(true);
    Message::create([
      'client_id' => $client->id,
      'member_id' => $member->id,
      'is_reply' => true,
      'is_sms' => false,
      'sender' => $sender,
      'receiver' => $receiver,
      'message' => $data['stripped-text'],
    ]);
    if ($client->forward_email) {
      \Mail::bcc($client->forward_email)
          ->send(new TaskMail($member->firstname.' '.$member->lastname, $member->email, 'Reply', $data['stripped-text']));
    }
  }

  public function permanentFailure(Request $request)
  {
    $data = $request->all();
    $receiver = $data['event-data']['recipient'];
    if (preg_match('/^BAD_/', $receiver)) {
      return;
    }
    Member::where('email', $receiver)
      ->update([
        'email' => 'BAD_'.$receiver
      ]);
  }

  public function upload(Request $request)
    {
      $request->validate(['file' => 'required|file|image']);

      $path = $request->file('file')->store('public');

      return ['success' => true, 'link' => url(Storage::url( $path))];
    }
}
