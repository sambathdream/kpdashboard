<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\SessionTask;
use App\Models\TaskResult;

class TaskController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, SessionTask $task)
    {
        $data = $request->validate([
            'name' => 'required|string|max:15',
            'tooltip' => 'nullable|string',
            'notes' => 'nullable|string',
            'deadline' => 'nullable|string',
            'hide_from_kp' => 'nullable|boolean',
        ]);
        $task->fill($data);
        $task->save();
        return $task;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(SessionTask $task)
    {
        $task->delete();
        return response()->json(true);
    }

    /**
     * Display a listing of the common tasks.
     *
     * @return \Illuminate\Http\Response
     */
    function getCommonTasks(Request $request)
    {
        $commonTasks = SessionTask::where('session_id', 0)
            ->orderBy('task_type_id')
            ->get();
        $names = ['PAC Donations', 'KP1 Donations', 'Other Donations'];
        $indices = [];
        $donationTasks = [];
        foreach ($commonTasks as $index => $commonTask) {
            foreach ($names as $name) {
                if ($commonTask->name === $name) {
                    $indices[] = $index;
                    $donationTasks[$name] = clone $commonTask;
                }
            }
        }
        sort($indices);
        for ($index = 0; $index < 3; $index += 1) {
            $commonTasks[$indices[$index]] = $donationTasks[$names[$index]];
        }
        return $commonTasks;
    }

    /**
     * Add/Update Task Result to the specified legislator(KP)'s task.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function addTaskResult(Request $request, SessionTask $sessionTask)
    {
        $data = $request->validate([
            'legislator_id' => 'required|numeric',
            'kp_id' => 'nullable|numeric',
            'task_id' => 'required|numeric',
            'result' => 'nullable|string',
        ]);
        $taskResult = TaskResult::where('legislator_id', $data['legislator_id'])
            ->where('kp_id', $data['kp_id'])
            ->where('task_id', $data['task_id'])
            ->first();
        if (is_null($taskResult)) {
            $taskResult = new TaskResult;
        }
        $taskResult->fill($data);
        $taskResult->save();
        return $taskResult;
    }
}
