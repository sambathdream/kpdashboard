<?php

  namespace App\Http\Controllers\Api;

  use App\Helpers\MailgunHelper;
  use App\Mails\DeactivateTwoFactorMail;
  use Carbon\Carbon;
  use Illuminate\Http\Request;
  use Illuminate\Support\Facades\Hash;
  use Illuminate\Support\Facades\Mail;
  use Illuminate\Support\Facades\URL;
  use Illuminate\Validation\Rule;
  use App\Http\Controllers\Controller;
  use App\Mails\InvitationMail;
  use App\Models\Invitation;
  use App\Models\MessageTime;
  use App\Models\User;
  use Authy\AuthyApi;

  class UserController extends Controller
  {
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
      //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $data = $request->validate([
        'active' => 'nullable|boolean',
        'role' => 'required|string',
        'client_id' => 'nullable|numeric',
        'member_id' => 'nullable|numeric',
        'firstname' => 'required|string',
        'lastname' => 'required|string',
        'nickname' => 'required|string',
        'email' => 'required|string|unique:users',
        'mailing_address' => 'nullable|string',
        'cell_phone' => 'nullable|string',
      ]);
      $user = new User;
      $user->fill($data);
      $user->password = '';
      $user->save();
      $user->assignRole($data['role']);
      // Send invitation email
      $invitation = Invitation::where('user_id', $user->id)->first();
      if (is_null($invitation)) {
        $invitation = Invitation::create([
          'user_id' => $user->id,
          'token' => str_random(60),
        ]);
      } else {
        $invitation->created_at = date('Y-m-d h:i:s');
      }

      MailgunHelper::send('welcome_email', $user->email, 'A new account has been created for you!', [
        "user_nick_name" => $user->nickname,
        "full_client_name" => $user->client->association,
        'confirm_link' => URL::to('/register/' . $invitation->token)
      ]);

      return $user;
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
      //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $user)
    {
      $data = $request->validate([
        'active' => 'nullable|boolean',
        'role' => 'required|string',
        'client_id' => 'nullable|numeric',
        'member_id' => 'nullable|numeric',
        'firstname' => 'required|string',
        'lastname' => 'required|string',
        'nickname' => 'required|string',
        'email' => ['required', 'string', Rule::unique('users')->ignore($user->id)],
        'mailing_address' => 'nullable|string',
        'cell_phone' => 'nullable|string',
        'need_to_change_password' => 'nullable|boolean',
        'current_password' => 'nullable|string',
        'new_password' => 'nullable|string',
      ]);
      if (array_key_exists('need_to_change_password', $data) && $data['need_to_change_password']) {
        $hasher = app('hash');
        if ($hasher->check($data['current_password'], $user->password)) {
          $user->password = bcrypt($data['new_password']);
        } else {
          return response()->json(['message' => 'Seems you entered wrong credential.'], 401);
        }
      }
      //$requireInvitation = $user->email !== $data['email'];
      $user->fill($data);
      $user->save();
      $user->syncRoles([$data['role']]);
      // Send invitation email
      // This code sent welcome email if user changed email.
      // It was removed because Adam Parker could not find a reason to keep it as it confused users and they
      // would still log in if they didn't click new user account link in email

      //if ($requireInvitation) {
      //    $invitation = Invitation::where('user_id', $user->id)->first();
      //    if (is_null($invitation)) {
      //        $invitation = Invitation::create([
      //            'user_id' => $user->id,
      //            'token' => str_random(60),
      //        ]);
      //    } else {
      //        $invitation->created_at = date('Y-m-d h:i:s');
      //        $invitation->save();
      //    }
      //    \Mail::to($user->email)
      //        ->send(new InvitationMail($user->firstname . ' ' . $user->lastname, $invitation->token));
      //}
      return $user;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
      $user->removeRole($user->roles[0]->name);
      $user->delete();
      return response()->json(true);
    }

    /**
     * Update last seen time of message
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function sawMessage(Request $request, User $user)
    {
      $data = $request->validate([
        'member_id' => 'required|numeric',
        'is_sms' => 'required|numeric',
      ]);
      $messageTime = MessageTime::where('user_id', $user->id)
        ->where('member_id', $data['member_id'])
        ->where('is_sms', $data['is_sms'])
        ->first();
      if (!$messageTime) {
        $messageTime = MessageTime::create([
          'user_id' => $user->id,
          'member_id' => $data['member_id'],
          'is_sms' => $data['is_sms'],
          'last_seen_at' => date('Y-m-d h:i:s'),
        ]);
      }
      $messageTime->last_seen_at = date('Y-m-d h:i:s');
      $messageTime->save();
      return response()->json(true);
    }

    public function sendSMSVerificationCode(Request $request)
    {
      $data = $request->validate([
        'phone' => 'required'
      ]);
      $phone = $data['phone'];
      $userEmail = auth()->user()->email;
      $countryCode = User::COUNTRY_CODE;
      try {
        $authyApi = new AuthyApi(config('services.twilio.authy_key'));
        $user = User::find(auth()->user()->id);
        $authUser = $authyApi->registerUser($userEmail, $phone, $countryCode);
        $user->twilio_authy_id = $authUser->id();
        $user->save();
        $authyApi->requestSms($user->twilio_authy_id);
        return response()->json(['message' => 'Verification code has been sent, Please check your phone'], 200);
      } catch (\Throwable $th) {
        return response()->json(['message' => $th->getMessage()], 500);
      }
    }

    public function verifyTwoFactorCode(Request $request)
    {
      try {
        $data = $request->validate([
          'verification_code' => ['required', 'numeric'],
        ]);
        $authyAPI = new AuthyApi(config('services.twilio.authy_key'));
        $res = $authyAPI->verifyToken(auth()->user()->twilio_authy_id, $data['verification_code']);

        if ($res->bodyvar("success")) {
          $user = User::find(auth()->user()->id);
          $user->two_factor_verified = true;
          $user->save();
          return response()->json(['verified' => true]);
        }
        return response()->json(['verified' => false]);
      } catch (\Throwable $th) {
        return response()->json(['error' => $th->getMessage()]);
      }
    }

    public function verifyPassword(Request $request)
    {
      $data = $request->validate([
        'password' => 'required|string',
      ]);
      $password = $data['password'];
      if (Hash::check($password, auth()->user()->getAuthPassword())) {
        return response()->json(
          array(
            'verified' => true
          )
          , 200);
      } else {
        return response()->json(
          array(
            'verified' => false
          )
          , 200);
      }
    }

    public function verifyLoginTwoFactorCode(Request $request)
    {
      try {
        $data = $request->validate([
          'verification_code' => ['required', 'numeric'],
        ]);
        $authyAPI = new AuthyApi(config('services.twilio.authy_key'));
        $res = $authyAPI->verifyToken(auth()->user()->twilio_authy_id, $data['verification_code']);
        if ($res->bodyvar("success")) {
          $user = User::find(auth()->user()->id);
          $user->two_fa_expire = Carbon::now()->addHour(2);
          $user->save();
          return response()->json(['verified' => true]);
        }
        return response()->json(['verified' => false]);
      } catch (\Throwable $th) {
        return response()->json(['error' => $th->getMessage()]);
      }
    }

    public function disableTwoFactor(Request $request)
    {
      try {
        $data = $request->validate([
          'verification_code' => ['required', 'numeric'],
        ]);
        $user = User::find(auth()->user()->id);
        $authyAPI = new AuthyApi(config('services.twilio.authy_key'));
        $res = $authyAPI->verifyToken(auth()->user()->twilio_authy_id, $data['verification_code']);
        if ($res->bodyvar("success")) {
          $authyAPI->deleteUser($user->twilio_authy_id);
          $user->two_factor_verified = false;
          $user->two_fa_expire = null;
          $user->twilio_authy_id = null;
          $user->save();
          return response()->json(['success' => true]);
        } else {
          return response()->json(['success' => false]);
        }
      } catch (\Throwable $throwable) {
        return response()->json(['error' => $throwable->getMessage()], 500);
      }
    }

    public function sendDeactivateCode()
    {
      try {
        $user = User::find(auth()->user()->id);
        $token = uniqid();
        $fullName = $user->firstname . ' ' . $user->lastname;
        $user->deactivate_token = Hash::make($token);
        $user->save();
        Mail::to($user->email)->send(new DeactivateTwoFactorMail($fullName, $token));
        return response()->json(['success' => true]);
      } catch (\Throwable $th) {
        return response()->json(['error' => $th->getMessage()]);
      }
    }

    public function deactivateTwoFactorCode(Request $request)
    {
      try {
        $user = User::find(auth()->user()->id);
        $data = $request->validate([
          'deactivate_token' => ['required', 'string'],
        ]);
        if (Hash::check($data['deactivate_token'], $user->deactivate_token)) {
          $user->twilio_authy_id = null;
          $user->two_factor_verified = false;
          $user->two_fa_expire = null;
          $user->save();
          return response()->json(['success' => 1]);
        } else {
          return response()->json(['success' => 0]);
        }
      } catch (\Throwable $th) {
        return response()->json(['error' => $th->getMessage()]);
      }
    }
  }
