<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use App\Http\Controllers\Controller;
use App\Models\IgnoredEmail;
use App\Models\ImportedLegislator;
use App\Models\LegData;
use App\Models\Legislator;
use App\Models\MemberLegislator;
use App\Models\LegislatorCommittee;

class LegislatorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Legislator::with(['district'])->get();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->validate([
            'state_id' => 'required|numeric',
            'is_federal' => 'nullable|boolean',
            'firstname' => 'required|string',
            'lastname' => 'required|string',
            'nickname' => 'required|string',
            'fullname' => 'required|string',
            'party' => 'nullable|string',
            'district_id' => 'nullable|numeric',
            'committees' => 'present',

            'official_email' => 'nullable|string',
            'official_phone' => 'nullable|string',
            'official_fax' => 'nullable|string',
            'official_address' => 'nullable|string',
            'state_room' => 'nullable|string',
            'aide_name' => 'nullable|string',
            'official_image' => 'nullable|string',

            'occupation' => 'nullable|string',
            'personal_email' => 'nullable|string',
            'local_phone' => 'nullable|string',
            'local_address' => 'nullable|string',

            'elected_at' => 'required|string',
            'abdicated_at' => 'nullable|string',

            'website_url' => 'nullable|string',
            'biography_url' => 'nullable|string',
            'ballotpedia_url' => 'nullable|string',
            'campaign_url' => 'nullable|string',
            'twitter_url' => 'nullable|string',
            'facebook_url' => 'nullable|string',
            'custom_link' => 'nullable|string',

            'instagram_url' => 'nullable|string',
            'linkedin_url' => 'nullable|string',
            'youtube_url' => 'nullable|string',
        ]);
        $legislator = new Legislator;
        $legislator->fill($data);
        $legislator->save();
        foreach ($data['committees'] as $committeeId => $memberships) {
            LegislatorCommittee::create([
                'legislator_id' => $legislator->id,
                'committee_id' => $committeeId,
                'memberships' => $memberships,
            ]);
        }
        return $legislator;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Legislator $legislator)
    {
        $data = $request->validate([
            'state_id' => 'required|numeric',
            'is_federal' => 'nullable|boolean',
            'firstname' => 'required|string',
            'lastname' => 'required|string',
            'nickname' => 'required|string',
            'fullname' => 'required|string',
            'party' => 'nullable|string',
            'district_id' => 'nullable|numeric',
            'committees' => 'present',

            'official_email' => 'nullable|string',
            'official_phone' => 'nullable|string',
            'official_fax' => 'nullable|string',
            'official_address' => 'nullable|string',
            'state_room' => 'nullable|string',
            'aide_name' => 'nullable|string',
            'official_image' => 'nullable|string',

            'occupation' => 'nullable|string',
            'personal_email' => 'nullable|string',
            'local_phone' => 'nullable|string',
            'local_address' => 'nullable|string',

            'elected_at' => 'required|string',
            'abdicated_at' => 'nullable|string',

            'website_url' => 'nullable|string',
            'biography_url' => 'nullable|string',
            'ballotpedia_url' => 'nullable|string',
            'campaign_url' => 'nullable|string',
            'twitter_url' => 'nullable|string',
            'facebook_url' => 'nullable|string',
            'custom_link' => 'nullable|string',

            'instagram_url' => 'nullable|string',
            'linkedin_url' => 'nullable|string',
            'youtube_url' => 'nullable|string',
        ]);
        $legislator->fill($data);
        $legislator->save();
        foreach ($data['committees'] as $committeeId => $memberships) {
            $legislatorCommittee = LegislatorCommittee::firstOrCreate([
                'legislator_id' => $legislator->id,
                'committee_id' => $committeeId,
            ], [
                'memberships' => $memberships,
            ]);
            $legislatorCommittee->memberships = $memberships;
            $legislatorCommittee->save();
        }
        return $legislator;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Legislator $legislator)
    {
        $legislator->delete();
        return response()->json(true);
    }

    /**
     * Display a listing of the specific legislator's vote history.
     *
     * @return \Illuminate\Http\Response
     */
    public function getVoteHistory(Request $request, Legislator $legislator)
    {
        $data = $request->validate([
            'client_id' => 'required|numeric',
        ]);
        $query = "
            select sessions.id, sessions.name as session_name, sessions.notes, session_tasks.name as task_name, task_results.result
            FROM sessions, task_results, session_tasks
            WHERE sessions.client_id = ".$data['client_id']."
            AND sessions.id = session_tasks.session_id
            AND session_tasks.id = task_results.task_id
            AND session_tasks.task_type_id in (5, 6)
            AND task_results.legislator_id = ".$legislator->id.";
        ";
        return \DB::select(\DB::raw($query));
    }

    /**
     * Display a listing of the specific legislator's KP history.
     *
     * @return \Illuminate\Http\Response
     */
    public function getKPHistory(Request $request, Legislator $legislator)
    {
        $data = $request->validate([
            'client_id' => 'required|numeric',
        ]);
        return MemberLegislator::where('leg_id', $legislator->id)
            ->where('client_id', $data['client_id'])
            ->with('member')
            ->orderBy('start_date', 'DESC')
            ->get();
    }

    /**
     * Display a listing of the specific legislator's Donation history.
     *
     * @return \Illuminate\Http\Response
     */
    public function getDonationHistory(Request $request, Legislator $legislator)
    {
        $data = $request->validate([
            'client_id' => 'required|numeric',
        ]);
        $query = "
            SELECT SUM(task_results.result) as donation, session_tasks.name session_task_name, sessions.id session_id, sessions.`name` session_name
            FROM task_results, session_tasks, task_types, sessions
            WHERE task_results.legislator_id = {$legislator->id}
                AND task_results.task_id = session_tasks.id
                AND session_tasks.task_type_id = task_types.id
                AND task_types.name = 'Currency'
                AND sessions.id = session_tasks.session_id
                AND sessions.client_id = {$data['client_id']}
            GROUP BY session_id, session_task_name
            ORDER BY sessions.start_at;
        ";
        return \DB::select(\DB::raw($query));
    }

    /**
     * Add data to the specified legislator.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function addLegData(Request $request, Legislator $legislator)
    {
        $data = $request->validate([
            'leg_id' => 'required|numeric',
            'client_id' => 'required|numeric',
            'vip_notes' => 'nullable|string',
            'leg_notes' => 'nullable|string',
            'leg_support' => 'nullable|numeric',
            'leg_region' => 'nullable|numeric',
        ]);
        $legData = new LegData;
        $legData->fill($data);
        $legData->save();
        return $legData;
    }

    /**
     * Update the specified legislator's data.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function updateLegData(Request $request, Legislator $legislator)
    {
        $data = $request->validate([
            'leg_id' => 'required|numeric',
            'client_id' => 'required|numeric',
            'vip_notes' => 'nullable|string',
            'leg_notes' => 'nullable|string',
            'leg_support' => 'nullable|numeric',
            'leg_region' => 'nullable|numeric',
        ]);
        $legData = LegData::where('leg_id', $data['leg_id'])
            ->where('client_id', $data['client_id'])
            ->first();
        $legData->fill($data);
        $legData->save();
        return $legData;
    }

    /**
     * Add/Update KP to the specified legislator.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function addKP(Request $request, Legislator $legislator)
    {
        $data = $request->validate([
            'leg_id' => 'required|numeric',
            'client_id' => 'required|numeric',
            'member_id' => 'required|numeric',
            'kp_position' => 'required|numeric',
            'kp_relationship' => 'present',
            'notes' => 'nullable|string',
            'start_date' => 'required|string',
            'end_date' => 'nullable|string'
        ]);
        $kp = MemberLegislator::where('leg_id', $data['leg_id'])
            ->where('client_id', $data['client_id'])
            ->where('kp_position', $data['kp_position'])
            ->whereNull('end_date')
            ->first();
        if (is_null($kp)) {
            $kp = new MemberLegislator;
        }
        $kp->fill($data);
        $kp->save();
        return $kp;
    }

    public function importFromGoogle(Request $request)
    {
        ImportedLegislator::truncate();
        \Artisan::call('legislators:import', []);
        return response()->json(true);
    }

    public function replaceWithNew(Request $request)
    {
        $data = $request->validate([
            'replaces' => 'required|array',
        ]);
        foreach ($data['replaces'] as $replace) {
            if ($replace['old'] !== null) {
                MemberLegislator::where('leg_id', $replace['old'])
                    ->whereNull('end_date')
                    ->update([
                        'end_date' => date('Y-m-d', strtotime('-1 days')),
                    ]);
                Legislator::where('id', $replace['old'])
                    ->update([
                        'abdicated_at' => date('Y-m-d', strtotime('-1 days')),
                    ]);
            }
            $newLegislator = ImportedLegislator::find($replace['new'])->toArray();
            $newLegislatorData = [];
            $except = ['id', 'created_at', 'updated_at'];
            foreach ($newLegislator as $key => $value) {
                if (!in_array($key, $except)) {
                    $newLegislatorData[$key] = $value;
                }
            }
            $newLegislatorData['elected_at'] = date('Y-m-d');
            Legislator::create($newLegislatorData);
        }
        return response()->json(true);
    }

    public function ignore(Request $request)
    {
        $data = $request->validate([
            'state_id' => 'required|numeric',
            'ignores' => 'required|array',
        ]);
        foreach ($data['ignores'] as $email) {
            $exist = IgnoredEmail::where('state_id', $data['state_id'])
                ->where('email', $email)
                ->first();
            if (!$exist) {
                IgnoredEmail::create([
                    'state_id' => $data['state_id'],
                    'email' => $email,
                ]);
            }
        }
        return response()->json(true);
    }
}
