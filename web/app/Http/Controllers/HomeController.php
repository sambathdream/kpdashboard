<?php

  namespace App\Http\Controllers;

  use Carbon\Carbon;
  use Illuminate\Http\Request;
  use Illuminate\Support\Facades\Auth;

  class HomeController extends Controller
  {
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
      //
    }

    /**
     * Show the application dashboard for authenticated users and login page for unauthenciated users.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */

    public function index()
    {
      /**
       *
       * And, of course, redirect the user to the login page if they aren't authenticated
       * at all yet.
       */
      if (Auth::check()) {
        $user = \auth()->user();
        if (isset($user->id) && (!$user->two_factor_verified ||
          ($user->two_factor_verified && !is_null($user->two_fa_expire) && $user->two_fa_expire > Carbon::now()))) {
          return view('dashboard');
        } else {
          auth()->logout();
          return view('auth.login');
        }
      }
      else if (Auth::guard('member')->check()){
        return view('dashboard');
      }
      else {
        return view('auth.login');
      }
    }
  }
