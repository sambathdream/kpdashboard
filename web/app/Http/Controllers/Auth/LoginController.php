<?php

namespace App\Http\Controllers\Auth;

use App\Helpers\AppHelper;
use App\Http\Controllers\Controller;
use App\Models\User;
use App\Services\TwilioService;
use Authy\AuthyApi;
use Carbon\Carbon;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use App\Models\Member;
use App\Models\MemberLegislator;
use App\Models\UserSession;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function login(Request $request) {
        $credentials = $request->only('email', 'password');

        if (\Auth::attempt($credentials)) {
            $user = \Auth::user();
            if (!$user->active) {
                \Auth::logout();
                return response()->json(['message' => 'This Account has been disabled by your team. Please contact your Admin for assistance.'], 401);
            }
            // Validate User
            $isInvalid = (!is_null($user->client) && !$user->client->active);
            // If member is active
            $userRole = $user->getRoleNames()[0];
            $isInvalid = $isInvalid || (($userRole === 'Coordinator' || $userRole === 'KP') && (is_null($user->member) || !$user->member->active));
            // If member is in charge of coordinator
            $isInvalid = $isInvalid || (($userRole === 'Coordinator' && !$user->member->coordinator));
            if ($isInvalid) {
                \Auth::logout();
                return response()->json(['message' => 'Login suspended - please contact your association\'s administrator.'], 401);
            }
            // Log
            UserSession::create([
                'client_id' => $user->client_id,
                'user_id' => $user->id
            ]);
            // Generate Token
            $userData = [
                'id' => $user->id,
                'client_id' => $user->client_id,
                'firstname' => $user->firstname,
                'lastname' => $user->lastname,
                'nickname' => $user->nickname,
                'email' => $user->email,
                'cell_phone' => $user->cell_phone,
                'twilio_authy_id' => $user->twilio_authy_id,
                'two_factor_verified' => $user->two_factor_verified,
            ];
            if ($userRole === 'VIP') {
                $member = Member::where('email', $userData['email'])->first();
                if ($member) {
                    $userData['coordinator'] = $member->coordinator;
                }
            }
            $token = $user->createToken(json_encode($userData));
            if ($user->client) {
                $user->client->state = $user->client->state;
            }
            $userData['client'] = $user->client;
            $userPermissions = $user->getAllPermissions();
            $userData['role'] = $userRole;
            $userData['permissions'] = [];
            foreach($userPermissions as $permission) {
                $userData['permissions'][] = $permission->name;
            }
            $responseData = [
                'token' => $token,
                'user' => $userData,
            ];
            return response()->json($responseData);
        }
        return response()->json(['message' => 'Seems you entered wrong credential.'], 401);
    }

    public function showLoginAsMemberPage($token)
    {
        return view('auth.login_as_member')->with([
            'token' => $token,
        ]);
    }

    public function loginAsMember(Request $request)
    {
        $data = $request->validate([
            'token' => 'required|string',
        ]);
        $credentials = [
            'token' => $data['token'],
            'password' => $data['token'],
            'active' => 1,
        ];
        if (\Auth::guard('member')->attempt($credentials)) {
            $member = \Auth::guard('member')->user();
            // Log
            if (!$member->token_used_at) {
                $member->token_used_at = date('Y-m-d h:i:s');
                $member->save();
            }
            // Generate Token
            $memberData = [
                'id' => $member->id,
                'client_id' => $member->client_id,
                'firstname' => $member->firstname,
                'lastname' => $member->lastname,
                'nickname' => $member->nickname,
                'email' => $member->email,
                'cell_phone' => $member->cell,
            ];
            $token = $member->createToken(json_encode($memberData));

            $member->client->state = $member->client->state;
            $memberData['client'] = $member->client;
            // Member Role
            $isKP = MemberLegislator::where('member_id', $member->id)
                ->where('kp_position', 1)
                ->whereNull('end_date')
                ->count() > 0;
            $memberData['role'] = $isKP ? 'KP' : 'Member';
            $memberData['permissions'] = [];
            $responseData = [
                'token' => $token,
                'user' => $memberData,
            ];
            return response()->json($responseData);
        }
        return response()->json(['message' => 'Seems you entered wrong link.'], 401);
    }
    public function sendKPLoginLink(Request $request)
    {
        $data = $request->validate([
          'phone' => 'required',
        ]);

      $phone = $data['phone'];
        try {
            $member = Member::with(['memberLegislator' =>  function ($q) {
                $q->whereNull('end_date');
              }])
              ->whereHas('memberLegislator')
              ->where('cell', AppHelper::getDomesticPhoneNumber($phone))
              ->where('active', 1)
              ->first();
          if (!$member) {
              return response()->json(['message' => 'Error. This number is not associated with an active Key Person'], 200);
            }
            $token = $member->token;
            $ezLink = config('app.app_alias_url').'/ez/'.$token;
            $msg = 'Here is your login link at KP Dashboard. '.$ezLink;
            $twilioService = new TwilioService();
            if ($twilioService->sendSMS($phone, $msg)) {
              return response()->json(['message' => "Login link sent to  $phone"], 200);
            }
            else{
              return response()->json(['message' => "Something went wrong, please try again later."], 200);
            }
        }catch (\Throwable $th)
        {
          return response()->json(['message' => $th->getMessage()], 500);
        }
    }
}
