<?php

namespace App\Http\Controllers\Auth;

use App\Models\Invitation;
use App\Models\Member;
use App\Models\User;
use App\Models\UserSession;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    public function index($token)
    {
        $invitation = Invitation::where('token', $token)->first();
        if (is_null($invitation)) {
            return view('auth.register')->with([
                'token' => $token,
                'email' => '',
            ]);
        }
        return view('auth.register')->with([
            'token' => $token,
            'email' => $invitation->user->email,
        ]);
    }

    public function register(Request $request)
    {
        // Check if token is valid
        $data = $request->validate([
            'token' => 'required|string',
        ]);
        $invitation = Invitation::where('token', $data['token'])->first();
        if (is_null($invitation)) {
            return response()->json(['message' => 'Seems you entered wrong link.'], 401);
        }
        $now = new \DateTime('now');
        $created_at = new \DateTime($invitation->created_at);
        $timeSpent = (int)$now->diff($created_at)->format('%i');
        if ($timeSpent >= 60) {
            return response()->json(['message' => 'Seems you entered expired link.'], 401);
        }

        $user = $invitation->user;
        $data = $request->validate([
            'password' => 'required|string|min:6',
        ]);
        $user->password = bcrypt($data['password']);
        $user->save();
        $invitation->delete();

        // Login
        $credentials = [
            'email' => $user->email,
            'password' => $data['password'],
        ];
        if (\Auth::attempt($credentials)) {
            // Validate User
            $isInvalid = (!is_null($user->client) && !$user->client->active);
            // If member is active
            $userRole = $user->getRoleNames()[0];
            $isInvalid = $isInvalid || (($userRole === 'Coordinator' || $userRole === 'KP') && (is_null($user->member) || !$user->member->active));
            // If member is in charge of coordinator
            $isInvalid = $isInvalid || (($userRole === 'Coordinator' && !$user->member->coordinator));
            if ($isInvalid) {
                \Auth::logout();
                return response()->json(['message' => 'Login suspended - please contact your association\'s administrator.'], 401);
            }
            // Log
            UserSession::create([
                'client_id' => $user->client_id,
                'user_id' => $user->id
            ]);
            // Generate access token
            $userData = [
                'id' => $user->id,
                'client_id' => $user->client_id,
                'firstname' => $user->firstname,
                'lastname' => $user->lastname,
                'nickname' => $user->nickname,
                'email' => $user->email,
                'cell_phone' => $user->cell_phone,
                'mailing_address' => $user->mailing_address,
            ];
            if ($userRole === 'VIP') {
                $member = Member::where('email', $userData['email'])->first();
                if ($member) {
                    $userData['coordinator'] = $member->coordinator;
                }
            }
            $token = $user->createToken(json_encode($userData));
            
            $userData['client'] = $user->client;
            $userRole = $user->getRoleNames()[0];
            $userPermissions = $user->getAllPermissions();
            $userData['role'] = $userRole;
            $userData['permissions'] = [];
            foreach($userPermissions as $permission) {
                $userData['permissions'][] = $permission->name;
            }
            $responseData = [
                'token' => $token,
                'user' => $userData,
            ];
            return response()->json($responseData);
        }
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:6', 'confirmed'],
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\Models\User
     */
    protected function create(array $data)
    {
        return User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
        ]);
    }
}
