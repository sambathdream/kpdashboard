<?php

namespace App\Providers;

use Illuminate\Support\Facades\Event;
use Illuminate\Auth\Events\Registered;
use Illuminate\Auth\Listeners\SendEmailVerificationNotification;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;
use App\Models\Client;
use App\Models\Legislator;
use App\Models\Member;
use App\Models\Message;
use App\Models\User;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        Registered::class => [
            SendEmailVerificationNotification::class,
        ],
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        //
        Client::saving(function($client) {
            $client->twilio_number = \AppHelper::getDomesticPhoneNumber($client->twilio_number);
            $client->forward_number = \AppHelper::getDomesticPhoneNumber($client->forward_number);
        });
        Legislator::saving(function($legislator) {
            $legislator->official_phone = \AppHelper::getDomesticPhoneNumber($legislator->official_phone);
            $legislator->local_phone = \AppHelper::getDomesticPhoneNumber($legislator->local_phone);
        });
        Member::saving(function($member) {
            $member->cell = \AppHelper::getDomesticPhoneNumber($member->cell);
            $member->office_phone = \AppHelper::getDomesticPhoneNumber($member->office_phone);
        });
        Message::saving(function($message) {
            if ($message->is_sms) {
                $message->sender = \AppHelper::getDomesticPhoneNumber($message->sender);
                $message->receiver = \AppHelper::getDomesticPhoneNumber($message->receiver);
            }
        });
        User::saving(function($user) {
            $user->cell_phone = \AppHelper::getDomesticPhoneNumber($user->cell_phone);
        });
    }
}
