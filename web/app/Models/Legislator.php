<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Legislator extends Model
{
  protected $fillable = [
    'state_id', 'district_id', 'is_federal', 'firstname', 'lastname', 'nickname', 'fullname', 'party',
    'official_email', 'official_phone', 'official_fax', 'official_address', 'state_room', 'aide_name',
    'official_image', 'occupation', 'personal_email', 'local_phone', 'local_address',
    'elected_at', 'abdicated_at', 'website_url', 'biography_url', 'ballotpedia_url',
    'campaign_url', 'twitter_url', 'facebook_url', 'custom_link',
    'instagram_url', 'linkedin_url', 'youtube_url',
  ];

  public function state()
  {
    return $this->belongsTo('App\Models\State');
  }

  public function district()
  {
    return $this->belongsTo('App\Models\District');
  }

  public function committees()
  {
    return $this->belongsToMany('App\Models\Committee', 'legislator_committee', 'legislator_id', 'committee_id')->withPivot('memberships');
  }
}
