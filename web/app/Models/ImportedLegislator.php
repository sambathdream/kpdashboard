<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ImportedLegislator extends Model
{
  protected $fillable = [
    'state_id', 'district_id', 'is_federal', 'firstname', 'lastname', 'nickname', 'fullname', 'party',
    'official_email', 'official_phone', 'official_fax', 'official_address', 'state_room', 'aide_name',
    'official_image', 'occupation', 'personal_email', 'local_phone', 'local_address',
    'elected_at', 'abdicated_at', 'website_url', 'biography_url', 'ballotpedia_url',
    'campaign_url', 'twitter_url', 'facebook_url','health',
  ];

  public function state()
  {
    return $this->belongsTo('App\Models\State', 'state_id', 'id');
  }

  public function district()
  {
    return $this->belongsTo('App\Models\District', 'district_id', 'id');
  }
}
