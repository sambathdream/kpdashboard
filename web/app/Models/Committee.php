<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Committee extends Model
{

  protected $fillable = [
    'state_id', 'type', 'shortname', 'fullname',
  ];

  public function state()
  {
    return $this->belongsTo('App\Models\State');
  }
}
