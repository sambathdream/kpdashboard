<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class State extends Model
{

    protected $fillable = ['has_joint_committees'];

    public function committees()
    {
        return $this->hasMany('App\Models\Committee');
    }

    public function districts($legislaterRequired = false)
    {
      if(!$legislaterRequired) {
        return $this->hasMany('App\Models\District');
      }
      else {
        return $this->hasMany('App\Models\District')->with('legislator')->whereHas('legislator');
      }
    }

    public function legislators()
    {
        return $this->hasMany('App\Models\Legislator');
    }
}
