<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SessionTask extends Model
{
  // use SoftDeletes;
  
  protected $fillable = [
    'session_id', 'task_type_id', 'order', 'name', 'tooltip', 'notes', 'deadline', 'hide_from_kp'
  ];

  public function session()
  {
    return $this->belongsTo('App\Models\Session');
  }

  public function task_type()
  {
    return $this->belongsTo('App\Models\TaskType');
  }

  public function task_results()
  {
    return $this->hasMany('App\Models\TaskResult', 'task_id');
  }
}
