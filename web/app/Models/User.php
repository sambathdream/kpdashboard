<?php

namespace App\Models;

use Laravel\Passport\HasApiTokens;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Spatie\Permission\Traits\HasRoles;

class User extends Authenticatable
{
    use HasApiTokens, HasRoles, Notifiable;
    const COUNTRY_CODE = '+1';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'active', 'client_id', 'member_id', 'firstname', 'lastname', 'nickname', 'email', 'password', 'mailing_address', 'cell_phone',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function client()
    {
        return $this->belongsTo('App\Models\Client');
    }

    public function member()
    {
        return $this->belongsTo('App\Models\Member');
    }

    public function invitation()
    {
        return $this->hasOne('App\Models\Invitation');
    }
}
