<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AdvocacyMemberMessage extends Model
{
  protected $fillable = [
    'advocacy_message_id', 'legislator_id', 'member_id', 'subject', 'message',
  ];

  public function advocacyMessage()
  {
    return $this->belongsTo('App\Models\AdvocacyMessage');
  }

  public function legislator()
  {
    return $this->belongsTo('App\Models\Legislator');
  }

  public function member()
  {
    return $this->belongsTo('App\Models\Member');
  }
}
