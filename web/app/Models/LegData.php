<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class LegData extends Model
{
  protected $table = 'leg_data';

  protected $fillable = [
    'leg_id', 'client_id', 'vip_notes', 'leg_notes', 'leg_support', 'leg_region'
  ];
}
