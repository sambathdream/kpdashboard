<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MemberLegislator extends Model
{
  protected $table = 'member_legislator';

  protected $fillable = [
    'leg_id', 'member_id', 'client_id', 'kp_position',
    'kp_relationship', 'notes', 'start_date', 'end_date',
  ];

  protected $casts = [
    'kp_relationship' => 'array',
  ];

  public function legislator()
  {
    return $this->belongsTo('App\Models\Legislator', 'leg_id', 'id');
  }

  public function client()
  {
    return $this->belongsTo('App\Models\Client');
  }

  public function member()
  {
    return $this->belongsTo('App\Models\Member');
  }
}
