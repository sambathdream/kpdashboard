<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class LegislatorCommittee extends Model
{
  protected $table = 'legislator_committee';

  protected $fillable = [
    'legislator_id', 'committee_id', 'memberships'
  ];

  protected $casts = [
    'memberships' => 'array',
  ];
}
