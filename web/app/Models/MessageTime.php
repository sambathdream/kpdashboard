<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MessageTime extends Model
{
  protected $fillable = [
    'user_id', 'member_id', 'is_sms', 'last_seen_at',
  ];
}
