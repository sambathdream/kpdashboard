<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class District extends Model
{

  protected $fillable = [
    'state_id', 'is_federal', 'ocd_id', 'district', 'name',
  ];

  public function state()
  {
    return $this->belongsTo('App\Models\State');
  }
  public function legislator() {
    return $this->hasOne('App\Models\Legislator');
  }
}
