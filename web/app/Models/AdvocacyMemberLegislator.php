<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AdvocacyMemberLegislator extends Model
{
  protected $fillable = [
    'advocacy_message_id', 'member_id', 'legislators',
  ];

  protected $casts = [
    'legislators' => 'array',
  ];

  public function advocacyMessage()
  {
    return $this->belongsTo('App\Models\AdvocacyMessage');
  }

  public function member()
  {
    return $this->belongsTo('App\Models\Member');
  }
}
