<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TaskResult extends Model
{
  protected $fillable = [
    'task_id', 'legislator_id', 'kp_id', 'result'
  ];

  public function client()
  {
    return $this->belongsTo('App\Models\Client');
  }
}
