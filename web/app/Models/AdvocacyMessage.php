<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AdvocacyMessage extends Model
{
  protected $fillable = [
    'client_id', 'subject', 'message', 'legislators', 'members', 'is_kp_only', 'sent_at',
    'active', 'deadline', 'invitation_subject', 'invitation_message', 'via_sms',
  ];

  protected $casts = [
    'legislators' => 'array',
    'members' => 'array',
  ];

  public function client()
  {
    return $this->belongsTo('App\Models\Client');
  }

  public function advocacyMemberLegislators()
  {
    return $this->hasMany('App\Models\AdvocacyMemberLegislator');
  }

  public function advocacyMemberMessages()
  {
    return $this->hasMany('App\Models\AdvocacyMemberMessage');
  }
}
