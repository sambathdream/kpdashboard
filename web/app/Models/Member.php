<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;
use Spatie\Permission\Traits\HasRoles;

class Member extends Authenticatable
{
  use HasApiTokens, HasRoles, Notifiable;

  protected $fillable = [
    'client_id', 'active', 'firstname', 'lastname', 'nickname',
    'cell', 'office_phone', 'email', 'avatar_url', 'eligible', 'coordinator', 'dob', 'grad_year', 'notes',
    'home_address', 'home_lat', 'home_lng', 'home_house_district', 'home_sen_district', 'home_con_district', 'home_federal_senate',
    'work_address', 'work_lat', 'work_lng', 'work_house_district', 'work_sen_district', 'work_con_district', 'work_federal_senate',
    'work2_address', 'work2_lat', 'work2_lng', 'work2_house_district', 'work2_sen_district', 'work2_con_district', 'work2_federal_senate',
    'work3_address', 'work3_lat', 'work3_lng', 'work3_house_district', 'work3_sen_district', 'work3_con_district', 'work3_federal_senate',
    'work4_address', 'work4_lat', 'work4_lng', 'work4_house_district', 'work4_sen_district', 'work4_con_district', 'work4_federal_senate',
    'token', 'token_used_at',
  ];

  /**
   * The attributes that should be hidden for arrays.
   *
   * @var array
   */
  protected $hidden = [
      'token', 'token_used_at',
  ];

  public function client()
  {
    return $this->belongsTo('App\Models\Client');
  }
  public function memberLegislator()
  {
    return $this->hasMany('App\Models\MemberLegislator');
  }
}
