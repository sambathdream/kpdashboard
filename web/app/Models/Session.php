<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Session extends Model
{
  protected $fillable = [
    'client_id', 'name', 'notes', 'start_at', 'end_at', 'house_committees', 'senate_committees', 'joint_committees',
  ];

  protected $casts = [
    'house_committees' => 'array',
    'senate_committees' => 'array',
    'joint_committees' => 'array',
  ];

  public function client()
  {
    return $this->belongsTo('App\Models\Client');
  }

  public function tasks()
  {
    return $this->hasMany('App\Models\SessionTask');
  }
}
