<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Legislator;
use App\Models\SessionTask;

class Client extends Model
{
    protected $fillable = [
        'active', 'association', 'assoc_abbrev', 'avatar_url', 'profession',
        'is_forward_email_required', 'reply_to_email', 'mailgun_email', 'forward_email',
        'is_forward_text_required', 'twilio_number', 'forward_number',
        'state_id', 'uses_regions', 'region_count', 'uses_coordinators',
        'show_all', 'uses_kps', 'uses_dob', 'uses_grad_year', 'uses_2nd_work', 'uses_3rd_work', 'uses_4th_work',
        'formal_house', 'formal_senate', 'house_committees', 'senate_committees', 'joint_committees',
    ];

    protected $casts = [
        'house_committees' => 'array',
        'senate_committees' => 'array',
        'joint_committees' => 'array',
    ];

    public function state()
    {
        return $this->belongsTo('App\Models\State');
    }

    public function districts()
    {
        return $this->state->districts(true)->orderBy('district');
    }

    public function members()
    {
        return $this->hasMany('App\Models\Member');
    }

    public function messages()
    {
        return $this->hasMany('App\Models\Message');
    }

    public function advocacyMessages()
    {
        return $this->hasMany('App\Models\AdvocacyMessage');
    }

    public function sessions()
    {
        return $this->hasMany('App\Models\Session');
    }

    public function users()
    {
        return $this->hasMany('App\Models\User');
    }

    public function user_sessions()
    {
        return $this->hasMany('App\Models\UserSession');
    }

    public function tasks($session_id)
    {
        return SessionTask::where('session_id', $session_id)
            ->with(['task_type'])
            ->orderByRaw('-deadline DESC')
            ->get();
    }

    public function legislators($committees = [])
    {
      $clientId = $this->id;
      $query = Legislator::where('state_id', $this->state_id)
        ->leftJoin('leg_data', function ($join) use ($clientId) {
          $join->on('legislators.id', '=', 'leg_data.leg_id')
            ->where('leg_data.client_id', '=', $clientId);
        })
        ->with(['district', 'committees'])
        ->select('*', 'legislators.id as id');

      if (!empty($committees)) {
        $query = $query->whereHas('committees', function ($query) use ($committees) {
          $query->whereIn('committees.id', $committees);
        });
      }

      return $query->get();
    }

    public function active_session()
    {
        return $this->sessions()->where('end_at', '>=', date('Y-m-d'))->first();
    }
}
