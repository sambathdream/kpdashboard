<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TaskType extends Model
{
  protected $fillable = [
    'client_id', 'name', 'data_type', 'options'
  ];

  protected $casts = [
    'options' => 'array',
  ];
}
