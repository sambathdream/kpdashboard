<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Message extends Model
{
  protected $fillable = [
    'client_id', 'member_id', 'is_reply', 'is_sms', 'sender', 'receiver', 'message',
  ];

  public function client()
  {
    return $this->belongsTo('App\Models\Client');
  }

  public function member()
  {
    return $this->belongsTo('App\Models\Member');
  }
}
