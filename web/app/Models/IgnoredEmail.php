<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class IgnoredEmail extends Model
{

  protected $fillable = [
    'state_id', 'email',
  ];
}
