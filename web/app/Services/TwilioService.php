<?php


namespace App\Services;

use App\Helpers\AppHelper;
use Illuminate\Support\Facades\Log;
use PhpParser\Error;
use Twilio\Exceptions\ConfigurationException;
use Twilio\Rest\Client as TwilioClient;


class TwilioService
{
    // Twilio Service instance
    public $twilioService = null;
    public $error = null;
    public function __construct()
    {
        try {
            $this->twilioService = new TwilioClient(config('services.twilio.twilio_sid'),
              config('services.twilio.token'));
        } catch (ConfigurationException $e) {
            $this->error = $e->getMessage();
        }
    }
    public function sendSMS($toPhone, $smsBody)
    {
        try {
            $this->twilioService->messages->create(AppHelper::getInternationalPhoneNumber($toPhone),
              array('body' => $smsBody, 'from' => config('services.twilio.number')));
            Log::info('Twilio Message sent to '.$toPhone);
            return true;
            // Log Text
        } catch (\Exception $e) {
            Log::error($e->getMessage());
            return false;
        }
    }
}
