<p>Dear {{ $mFullName }},</p>
<p></p>
<p>This is a verification token  to deactivate 2fa for your account : {{$token}}</p>
<p></p>
<p></p>
<em>KP Dashboard</em>
