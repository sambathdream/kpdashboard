<p>Dear {{ $mFullName }},</p>
<p></p>
<p>A new account has been created for you at kpdashboard.com. Please click the link to set up your account.</p>
<a href="{{ URL::to('/register/'.$mToken) }}">{{ URL::to('/register/'.$mToken) }}</a>
<p></p>
<p></p>
<em>KP Dashboard - Strong grassroots starts here.</em>
