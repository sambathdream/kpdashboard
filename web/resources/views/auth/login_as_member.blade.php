@extends('layouts.app')

@section('content')
  <v-app id="app">
    <login-as-member-page token="{{ $token }}" />
  </v-app>

  <!-- Scripts -->
  <script src="{{ asset('js/auth.js') }}" defer></script>
@endsection
