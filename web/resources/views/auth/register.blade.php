@extends('layouts.app')

@section('content')
  <v-app id="app">
    <register-page token="{{ $token }}" email="{{ $email }}" />
  </v-app>

  <!-- Scripts -->
  <script src="{{ asset('js/auth.js') }}" defer></script>
@endsection
