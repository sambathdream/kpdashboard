@extends('layouts.app')

@section('content')
  <v-app id="app">
    <login-page />
  </v-app>

  <!-- Scripts -->
  <script src="{{ asset('js/auth.js') }}" defer></script>
@endsection
