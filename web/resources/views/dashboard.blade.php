@extends('layouts.app')

@section('content')
	<div id="app">
		<router-view></router-view>
	</div>

    <!-- Scripts -->
	<script src="https://cdn.quilljs.com/1.3.6/quill.min.js"></script>
    <script src="{{ asset('js/dashboard.js') }}" defer></script>
@endsection
