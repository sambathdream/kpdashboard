<!DOCTYPE html>
<html>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <!-- <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui"> -->
  <style>
    body {
      background-color: #EEE;
    }
    p {
      font-size: 15px;
      line-height: 20px;
      color: #666;
    }
    a {
      font-size: 15px;
    }
    .panel {
      margin: 50px auto;
      max-width: 600px;
      overflow: hidden;
      border-radius: 8px;
      background-color: #FFF;
    }
    .panel-header {
      padding: 10px 30px;
      color: #1E3C72;
    }
    .panel-subheader {
      padding: 30px;
      text-align: center;
      background-color: #1E3C72;
      color: #FFF;
    }
    .panel-body {
      padding: 30px;
    }
    .panel-footer {
      padding: 30px;
      text-align: center;
    }
  </style>
  @yield('css_modules')
</head>
<body>
  <div class="panel">
    <div class="panel-header">
      <h2>@yield('header')</h2>
    </div>
    <div class="panel-subheader">
      <h1>@yield('subheader')<h1>
    </div>
    <div class="panel-body">
      @yield('body')
    </div>
    <div class="panel-footer">
      <hr />
      <a href="https://staging.kpdashboard.com">staging.kpdashboard.com</a>
      <br />
      <p>Copyright KP Dashboard © 2019</p>
    </div>
  </div>
</body>
</html>
