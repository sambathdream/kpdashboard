export default {
  csrf () {
    return document.head.querySelector('meta[name="csrf-token"]').content
  },
  getAccessToken () {
    const token = localStorage.getItem('token')
    if (!token) return null
    return JSON.parse(token).accessToken
  },
  getUserId () {
    const user = localStorage.getItem('user')
    if (!user) return null
    return JSON.parse(user).id
  },
  getUserEmail () {
    const user = localStorage.getItem('user')
    if (!user) return null
    return JSON.parse(user).email
  },
  logout () {
    axios.post('/logout')
    localStorage.clear()
    location.reload()
  },
  validateEmail (email) {
    const emailRegExp = /^\w+([.-]?\w+)*@\w+([.-]?\w+)*(\.\w{2,3})+$/
    return emailRegExp.test((email || '').trim().toLowerCase())
  }
}
