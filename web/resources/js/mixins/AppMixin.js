import { mapState, mapGetters } from 'vuex'

export default {
  computed: {
    ...mapState('appStore', {
      appState: state => state
    }),
    ...mapState('authStore', {
      userData: state => state.userData,
      refreshedAt: state => state.refreshedAt,
    }),
    ...mapGetters('authStore', [
      'userId',
      'userClientId',
      'userClient',
      'userName',
      'userEmail',
      'userRole',
      'userMailingAddress',
      'userCellPhone',
      'userPermissions',
    ]),
    isSuperAdmin () {
      return this.userRole === 'Super Admin'
    },
    isVIP () {
      return this.userRole === 'VIP'
    },
    isCoordinator () {
      return this.userRole === 'Coordinator'
    },
    isKP () {
      return this.userRole === 'KP'
    },
    isMember () {
      return this.userRole === 'Member'
    },
    phoneNumberRules () {
      return [
        v => (!v || !!v && v.length === 10) || 'Invalid phone number',
      ]
    }
  },
  filters: {
    dateFormatter (value, format) {
      if (!value) return ''
      if (!format) format = 'MMM D, YYYY'
      return moment(value).format(format)
    },
    dateTimeFormatter (value) {
      if (!value) return ''
      const utc = moment.utc(value).toDate()
      return moment(utc).local().format('MMM D, YYYY hh:mm A')
    },
    timeFormatter (value) {
      if (!value) return ''
      const utc = moment.utc(value).toDate()
      return moment(utc).local().format('hh:mm A')
    },
    dateDiffFormatter (value, isUTC) {
      let date = null
      if (isUTC) {
        const utc = moment.utc(value).toDate()
        date = moment(utc).local()
      } else {
        date = moment(value)
      }
      const duration = moment.duration(moment().diff(date))
      if (duration.asYears() >= 1) return `${parseInt(duration.asYears())}yr`
      if (duration.asMonths() >= 1) return `${parseInt(duration.asMonths())}mo`
      if (duration.asDays() >= 1) return `${parseInt(duration.asDays())}day`
      if (duration.asHours() >= 1) return `${parseInt(duration.asHours())}hr`
      if (duration.asMinutes() >= 1) return `${parseInt(duration.asMinutes())}min`
      return '1min'
    },
    integerFormatter (value, prefix, targetLength, paddingString) {
      if (!value) return prefix + ''.padStart(targetLength, '-')
      return prefix + value.toString().padStart(targetLength, paddingString)
    },
    textFormatter (value, length) {
      if (value.length <= length) return value
      return value.substring(0, length) + '...'
    }
  },
  methods: {
    handleError (err, useToastr) {
      this.isBusy = false
      switch (err.response.status) {
        case 400:
        case 401:
          this.error = err.response.data.message
          break
        case 422:
          _.forEach(err.response.data.errors, (error) => {
            this.error = error[0]
          })
          break
        default:
          this.error = 'Something went wrong!'
      }
      if (!useToastr) return
      toastr.clear()
      toastr.error(this.error, 'Error')
    },
    isBad (text) {
      return text && /^BAD_/.test(text)
    },
    getThumbImage (imgUrl) {
      const baseUrl = 'https://res.cloudinary.com/kpdash/image/upload/'
      if (imgUrl.indexOf(baseUrl) === -1) {
        return imgUrl
      }
      const sections = imgUrl.split(baseUrl)
      return `${baseUrl}c_thumb,w_60,h_60,g_face,e_improve/${sections[1]}`
    },
    getColor (name) {
      const colorList = [
        'primary',
        'success',
        'warning',
        'error',
      ]
      const index = name[0].charCodeAt(0)
      return colorList[index % colorList.length]
    },
    getProgressColor (progress) {
      if (progress < 0.3) return 'error'
      if (progress < 0.7) return 'warning'
      return 'success'
    },
    getColorFromText (text) {
      const colorList = [
        'red',
        'pink',
        'purple',
        'deep-purple',
        'indigo',
        'blue',
        'light-blue',
        'cyan',
        'teal',
        'green',
        'light-green',
        'amber',
        'orange',
        'deep-orange',
        'brown',
        'blue-grey',
      ]
      const sum = text.split('').reduce((a, b) => (a + b.charCodeAt(0) * b.charCodeAt(0)), 0)
      return colorList[sum % colorList.length]
    },
    getLocaleFromUTC (date, format) {
      const utc = moment.utc(date).toDate()
      return moment(utc).local().format(format)
    },
    getFormattedDate (date, format) {
      return moment(date).format(format)
    }
  }
}
