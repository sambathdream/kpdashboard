import AppService from '../modules/dashboard/services/AppService'

export default {
  mixins: [AppService],
  methods: {
    setImageUploadHendler() {
      if(this.$refs.quillEditor && this.$refs.quillEditor.quill.getModule('toolbar')){
        let toolbar = this.$refs.quillEditor.quill.getModule('toolbar')
        toolbar.addHandler('image', this.onImageUpload);
      }
    },
    sendImage(file) {
      let data = new FormData()
      data.append('file', file)

      return this.uploadImage(data).then((response) => {
        return response.data.link
      })
    },
    onImageUpload() {
      let self = this
      let input = document.createElement('input')
      input.setAttribute('type', 'file')
      input.setAttribute('accept', 'image/*')
      input.click()
      input.onchange = async function() {
        let file = input.files[0]
        let filePath = await self.sendImage(file)
        let range = self.$refs.quillEditor.quill.getSelection();

        self.$refs.quillEditor.quill.insertEmbed(range.index, "image", filePath);
      }
    }
  },
  mounted() {
    this.setImageUploadHendler()
  }
}
