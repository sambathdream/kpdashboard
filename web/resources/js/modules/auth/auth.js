
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('../../bootstrap')

import Vue from 'vue'
import Vuetify from 'vuetify'

import store from '../../store'
import AppMixin from '../../mixins/AppMixin'

import 'vuetify/dist/vuetify.min.css'

Vue.use(Vuetify)

Vue.mixin(AppMixin)
/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i)
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

Vue.component('login-page', require('./pages/LoginPage.vue').default)
Vue.component('login-as-member-page', require('./pages/LoginAsMemberPage.vue').default)
Vue.component('register-page', require('./pages/RegisterPage.vue').default)
Vue.component('forgot-password-page', require('./pages/ForgotPasswordPage.vue').default)
Vue.component('reset-password-page', require('./pages/ResetPasswordPage.vue').default)
import toastr from 'toastr'
window.toastr = toastr
const app = new Vue({
    el: '#app',
    store,
    // vuetify: new Vuetify()
})
