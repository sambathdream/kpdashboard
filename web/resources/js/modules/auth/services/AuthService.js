export default {
  methods: {
    login (payload) {
      return axios.post('/login', payload)
    },
    loginAsMember (payload) {
      return axios.post('/login-as-member', payload)
    },
    register (payload) {
      return axios.post('/register', payload)
    },
    remember (payload) {
      return axios.post('/password/email', payload)
    },
    reset (payload) {
      return axios.post('/password/reset', payload)
    },
    sendKPLoginLink(payload) {
      return axios.post('/sendKPLoginLink', payload)
    }
  }
}
