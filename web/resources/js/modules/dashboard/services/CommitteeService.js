export default {
  methods: {
    getCommittees () {
      return axios.get('/api/committees')
    },
    createCommittee (payload) {
      return axios.post('/api/committees', payload)
    },
    updateCommittee (payload) {
      return axios.put(`/api/committees/${payload.id}`, payload)
    },
    deleteCommittee (committeeId) {
      return axios.delete(`/api/committees/${committeeId}`)
    },
  }
}
