import qs from 'qs'
export default {
  methods: {
    getClients () {
      return axios.get('/api/clients')
    },
    createClient (payload) {
      return axios.post('/api/clients', payload)
    },
    updateClient (payload) {
      return axios.put(`/api/clients/${payload.id}`, payload)
    },
    deleteClient (clientId) {
      return axios.delete(`/api/clients/${clientId}`)
    },
    disableClient (clientId) {
      return axios.post(`/api/clients/${clientId}/disable`)
    },
    enableClient (clientId) {
      return axios.post(`/api/clients/${clientId}/enable`)
    },
    getClientUsers (clientId) {
      return axios.get(`/api/clients/${clientId}/users`)
    },
    getClientDistricts (clientId) {
      return axios.get(`/api/clients/${clientId}/districts`)
    },
    getClientSessions (clientId) {
      return axios.get(`/api/clients/${clientId}/sessions`)
    },
    getClientActiveSession (clientId) {
      return axios.get(`/api/clients/${clientId}/active-session`)
    },
    getClientMembers (clientId) {
      return axios.get(`/api/clients/${clientId}/members`)
    },
    getClientLegislators (clientId) {
      return axios.get(`/api/clients/${clientId}/legislators`)
    },
    getClientLegislatorsWithCommittees (clientId, params) {
      return axios.get(`/api/clients/${clientId}/legislators`, { params })
    },
    getClientAssignments (clientId) {
      return axios.get(`/api/clients/${clientId}/assignments`)
    },
    getClientKPTasks (clientId) {
      return axios.get(`/api/clients/${clientId}/kp-tasks`)
    },
    getClientMessageMembers (clientId, params) {
      return axios.get(`/api/clients/${clientId}/message-members`, { params })
    },
    getClientMessages (clientId, params) {
      return axios.get(`/api/clients/${clientId}/messages`, { params })
    },
    getClientUnseenMessageCount (clientId) {
      return axios.get(`/api/clients/${clientId}/unseen-message-count`)
    },
    getClientAdvocacyMessages (clientId) {
      return axios.get(`/api/clients/${clientId}/advocacy-messages`)
    },
    getClientUserSessions (clientId) {
      return axios.get(`/api/clients/${clientId}/user-sessions`)
    },
    sendEmail (payload) {
      return axios.post(`/api/clients/${payload.clientId}/send-email`, payload)
    },
    sendText (payload) {
      return axios.post(`/api/clients/${payload.clientId}/send-text`, payload)
    },
    importClientMembers (clientId, payload) {
      return axios.post(`/api/clients/${clientId}/import-members`, payload)
    },
    getClientStatus (clientId) {
      return axios.get(`/api/clients/${clientId}/status`)
    },
    updateClientDistricts (clientId) {
      return axios.post(`/api/clients/${clientId}/update-districts`)
    },
    getClientKPLeaderboardData (clientId) {
      return axios.get(`/api/clients/${clientId}/kp-leaderboard-data`)
    },
    getMyKPs (clientId) {
      return axios.get(`/api/clients/${clientId}/my-kps`)
    }
  }
}
