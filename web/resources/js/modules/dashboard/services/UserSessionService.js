export default {
  methods: {
    getUserSessions () {
      return axios.get('/api/user-sessions')
    },
    createUserSession (payload) {
      return axios.post('/api/user-sessions', payload)
    },
    updateUserSession (payload) {
      return axios.put(`/api/user-sessions/${payload.id}`, payload)
    },
    deleteUserSession (userId) {
      return axios.delete(`/api/user-sessions/${userId}`)
    }
  }
}
