export default {
  methods: {
    getLegislators () {
      return axios.get('/api/legislators')
    },
    createLegislator (payload) {
      return axios.post('/api/legislators', payload)
    },
    updateLegislator (payload) {
      return axios.put(`/api/legislators/${payload.id}`, payload)
    },
    deleteLegislator (legislatorId) {
      return axios.delete(`/api/legislators/${legislatorId}`)
    },
    getLegislatorVoteHistory (params) {
      return axios.get(`/api/legislators/${params.id}/vote-history`, { params })
    },
    getLegislatorKPHistory (params) {
      return axios.get(`/api/legislators/${params.id}/kp-history`, { params })
    },
    getLegislatorDonationHistory (params) {
      return axios.get(`/api/legislators/${params.id}/donation-history`, { params })
    },
    addLegData (payload) {
      return axios.post(`/api/legislators/${payload.leg_id}/data`, payload)
    },
    updateLegData (payload) {
      return axios.put(`/api/legislators/${payload.leg_id}/data`, payload)
    },
    addLegislatorKP (payload) {
      return axios.post(`/api/legislators/${payload.leg_id}/kp`, payload)
    },
    importLegislatorsFromGoogle () {
      return axios.post('/api/legislators/import-from-google')
    },
    replaceWithNewLegislators (payload) {
      return axios.post('/api/legislators/replace-with-new', payload)
    },
    ignoreLegislators (payload) {
      return axios.post('/api/legislators/ignore', payload)
    },
    getLegislatorNews(payload, data) {
      let proxyUrl = 'https://cors-anywhere.herokuapp.com/'
      return axios.get(`${proxyUrl}https://news.google.com/rss/search?q=${payload.fullname}+${data.state}+&tbm=nws&hl=en-US&gl=US&ceid=US:en`)
    }
  }
}
