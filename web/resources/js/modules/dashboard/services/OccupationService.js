export default {
  methods: {
    getOccupations () {
      return axios.get('/api/occupations')
    },
    createOccupation (payload) {
      return axios.post('/api/occupations', payload)
    },
    updateOccupation (payload) {
      return axios.put(`/api/occupations/${payload.id}`, payload)
    },
    deleteOccupation (occupationId) {
      return axios.delete(`/api/occupations/${occupationId}`)
    }
  }
}
