export default {
  methods: {
    getMembers () {
      return axios.get('/api/members')
    },
    createMember (payload) {
      return axios.post('/api/members', payload)
    },
    updateMember (payload) {
      return axios.put(`/api/members/${payload.id}`, payload)
    },
    deleteMember (memberId) {
      return axios.delete(`/api/members/${memberId}`)
    },
    getMemberKPHistory (params) {
      return axios.get(`/api/members/${params.id}/kp-history`, { params })
    },
    searchMembersByDistrict (payload) {
      return axios.post('/api/members/search-by-district', payload)
    },
    getKPTasks (memberId) {
      return axios.get(`/api/members/${memberId}/tasks`)
    },
    getMemberAssignedLegislators (memberId) {
      return axios.get(`/api/members/${memberId}/assigned-legislators`)
    },
    addKPTaskResults (payload) {
      return axios.post(`/api/members/${payload.id}/task-results`, payload)
    },
    getMemberAdvocacyMessages (memberId) {
      return axios.get(`/api/members/${memberId}/advocacy-messages`)
    },
    getMemberAdvocacyMessageHistory (memberId) {
      return axios.get(`/api/members/${memberId}/advocacy-message-history`)
    },
  }
}
