export default {
  methods: {
    getSessions () {
      return axios.get('/api/sessions')
    },
    createSession (payload) {
      return axios.post('/api/sessions', payload)
    },
    updateSession (payload) {
      return axios.put(`/api/sessions/${payload.id}`, payload)
    },
    deleteSession (sessionId) {
      return axios.delete(`/api/sessions/${sessionId}`)
    },
    getSessionKPTasks (sessionId) {
      return axios.get(`/api/sessions/${sessionId}/kp-tasks`)
    },
    getSessionTasks (sessionId) {
      return axios.get(`/api/sessions/${sessionId}/tasks`)
    },
    getSessionDashboardData (sessionId) {
      return axios.get(`/api/sessions/${sessionId}/dashboard-data`)
    },
    addSessionTasks (payload) {
      return axios.post(`/api/sessions/${payload.sessionId}/tasks`, payload)
    }
  }
}
