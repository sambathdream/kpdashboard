export default {
  methods: {
    getAppFormattedMessage (params) {
      return axios.post('/api/app/formatted-message', params)
    },
    uploadImage(payload) {
      return axios.post(`/api/mailgun/upload`, payload, {headers: {'Content-Type': 'multipart/form-data'}})
    }
  }
}
