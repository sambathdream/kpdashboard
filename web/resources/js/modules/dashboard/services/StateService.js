export default {
  methods: {
    getStates () {
      return axios.get('/api/states')
    },
    createState (payload) {
      return axios.post('/api/states', payload)
    },
    updateState (stateId, payload) {
      return axios.put(`/api/states/${stateId}`, payload)
    },
    deleteState (stateId) {
      return axios.delete(`/api/states/${stateId}`)
    },
    getStateCommittees (stateId) {
      return axios.get(`/api/states/${stateId}/committees`)
    },
    getStateLegislators (stateId) {
      return axios.get(`/api/states/${stateId}/legislators`)
    },
    getStateUnelectedDistricts (stateId) {
      return axios.get(`/api/states/${stateId}/unelected-districts`)
    },
    getNewStateLegislators (stateId) {
      return axios.get(`/api/states/${stateId}/new-legislators`)
    }
  }
}
