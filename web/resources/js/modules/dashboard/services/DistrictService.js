export default {
  computed: {
    selectedModalDistrict: {
      get() {
        return this.appState.selectedModalDistrict
      },
      set(newValue) {
        this.$store.dispatch('appStore/setSelectedModalDistrict', {'district':newValue})
      }
    },
    districtModalEnabled: {
      get() {
        return this.appState.districtModalEnabled
      },
      set(newValue) {
        this.$store.dispatch('appStore/setDistrictModalEnabled', {'status':newValue})
      }
    }
  },
  methods: {
    searchDistrictByAddress (payload) {
      return axios.post('/api/districts/search-by-address', payload)
    },


    openDistrictModal(district){
      if(typeof district == "undefined" ) return
      this.$store.dispatch('appStore/setSelectedModalDistrict', {'district':district})
      this.$store.dispatch('appStore/setDistrictModalEnabled', {'status':true})
    },
  }
}
