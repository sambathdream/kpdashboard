export default {
  methods: {
    getTasks () {
      return axios.get('/api/tasks')
    },
    createTask (payload) {
      return axios.post('/api/tasks', payload)
    },
    updateTask (payload) {
      return axios.put(`/api/tasks/${payload.id}`, payload)
    },
    deleteTask (taskId) {
      return axios.delete(`/api/tasks/${taskId}`)
    },
    getCommonTasks () {
      return axios.get('/api/tasks/common')
    },
    addTaskResult (payload) {
      return axios.post(`/api/tasks/${payload.task_id}/result`, payload)
    }
  }
}
