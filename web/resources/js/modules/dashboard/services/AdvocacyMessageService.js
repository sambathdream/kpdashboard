export default {
  methods: {
    createAdvocacyMessage (payload) {
      return axios.post('/api/advocacy-messages', payload)
    },
    updateAdvocacyMessage (payload) {
      return axios.put(`/api/advocacy-messages/${payload.id}`, payload)
    },
    deleteAdvocacyMessage (advocacyMessageId) {
      return axios.delete(`/api/advocacy-messages/${advocacyMessageId}`)
    },
    sendAdvocacyMessage (advocacyMessageId) {
      return axios.post(`/api/advocacy-messages/${advocacyMessageId}/send`)
    },
    remindAdvocacyMessage (advocacyMessageId, payload) {
      return axios.post(`/api/advocacy-messages/${advocacyMessageId}/remind`, payload)
    },
    searchMembers (payload) {
      return axios.post('/api/advocacy-messages/search-members', payload)
    },
    getRemainingLegislators (advocacyMessageId, params) {
      return axios.get(`/api/advocacy-messages/${advocacyMessageId}/remaining-legislators`, { params })
    },
    getStats (advocacyMessageId) {
      return axios.get(`/api/advocacy-messages/${advocacyMessageId}/stats`)
    },
    getReplyStats (advocacyMessageId, params) {
      return axios.get(`/api/advocacy-messages/${advocacyMessageId}/reply-stats`, { params })
    },
    getFormattedSubject (advocacyMessageId, params) {
      return axios.get(`/api/advocacy-messages/${advocacyMessageId}/formatted-subject`, { params })
    },
    getFormattedMessage (advocacyMessageId, params) {
      return axios.get(`/api/advocacy-messages/${advocacyMessageId}/formatted-message`, { params })
    },
    sendReplyAdvocacyMessage (advocacyMessageId, payload) {
      return axios.post(`/api/advocacy-messages/${advocacyMessageId}/send-reply`, payload)
    }
  }
}
