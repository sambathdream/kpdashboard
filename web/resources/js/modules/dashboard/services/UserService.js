export default {
  methods: {
    getUsers () {
      return axios.get('/api/users')
    },
    createUser (payload) {
      return axios.post('/api/users', payload)
    },
    updateUser (payload) {
      return axios.put(`/api/users/${payload.id}`, payload)
    },
    deleteUser (userId) {
      return axios.delete(`/api/users/${userId}`)
    },
    verifyPassword (payload) {
      return axios.post('/api/users/verify-password', payload)
    },
    verifyPhone (payload) {
      return axios.post(`/api/users/verify-phone`, payload)
    },
    verifyCode (payload) {
      return axios.post(`/api/users/verify-code`, payload)
    },
    disable2fa (payload) {
      return axios.post(`/api/users/disable-2fa`, payload)
    },
    verifyUserTwoFactorCode(payload) {
      return axios.post('/api/users/verify-user-login-two-factor', payload)
    },
    sendDeactivateCode () {
      return axios.post(`/api/users/send-two-factor-deactivate-code`)
    },
    deactivateTwoFactorCode (payload) {
      return axios.post(`/api/users/deactivate-two-factor-code`, payload)
    }
  }
}
