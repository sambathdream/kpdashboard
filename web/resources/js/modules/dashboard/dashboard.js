/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('../../bootstrap')

import Vue from 'vue'
import Vuetify from 'vuetify'
import VueClipboard2 from 'vue-clipboard2'
import VueCurrencyFilter from 'vue-currency-filter'
import * as VueGoogleMaps from 'vue2-google-maps'
import VueIntercom from 'vue-intercom'
import toastr from 'toastr'
import * as AgGridEnterprise from 'ag-grid-enterprise'

import store from '../../store'
import router from './dashboard-router'
import AppMixin from '../../mixins/AppMixin'

import 'vuetify/dist/vuetify.min.css'
import 'toastr/build/toastr.min.css'
import 'ag-grid-community/dist/styles/ag-grid.css'
import 'ag-grid-community/dist/styles/ag-theme-balham.css'
import 'quill/dist/quill.core.css'
import 'quill/dist/quill.snow.css'
import 'quill/dist/quill.bubble.css'

window.toastr = toastr

AgGridEnterprise.LicenseManager.setLicenseKey('Parker_Technology_Services,_LLC_KP_Dashboard_1Devs_1Deployment_8_July_2020__MTU5NDE2MjgwMDAwMA==23836b505ea64c36d0bc08755b365779')

Vue.use(Vuetify, {
    theme: {
        // primary: '#374760',
        // secondary: '#8b97af',
        // success: '#39c65d',
        // danger: '#f86c6b',
        // warning: '#e7a621',
        // info: '#5eb1ec',
        // purple: '#b4a2d9',
        // accent: '#374760',
    }
})
Vue.use(VueClipboard2)
Vue.use(VueCurrencyFilter, {
    symbol: '$',
    thousandsSeparator: ',',
    fractionCount: 2,
    fractionSeparator: '.',
    symbolPosition: 'front',
    symbolSpacing: true
})
Vue.use(VueGoogleMaps, {
    load: {
        key: 'AIzaSyCjbF1Y4btcjHD-Yc7ckgcnVik9Fp9uQOI',
        libraries: ['places', 'directions'],
    },
})
Vue.use(VueIntercom, { appId: 'yryk1hew' })

Vue.mixin(AppMixin)

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i)
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

const app = new Vue({
    el: '#app',
    store,
    router,
    // vuetify: new Vuetify()
})
