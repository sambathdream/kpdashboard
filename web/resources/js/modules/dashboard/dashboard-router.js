import Vue from 'vue'
import VueRouter from 'vue-router'

import store from '../../store'

const userRole = store.state.authStore.userData.role
const serviceType = store.state.authStore.userData.serviceType

const defaultRoutes = {
  'Super Admin': '/home',
  'VIP': '/home',
  'Coordinator': '/my-kps',
  'KP': serviceType === 'adv' ? '/assigned-tasks' : '/my-tasks',
  'Member': '/assigned-tasks',
}

const routeMiddleware = (allowedRoleList, next) => {
  if (allowedRoleList.includes(userRole)) return next()
  return next(defaultRoutes[userRole])
}

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    component: require('./layouts/DashboardLayout.vue').default,
    children: [
      {
        path: '',
        name: 'status-page',
        component: require('./pages/StatusPage.vue').default,
        beforeEnter(to, from, next) {
          routeMiddleware(['VIP'], next)
        }
      },
      {
        path: 'home',
        name: 'home-page',
        component: require('./pages/HomePage.vue').default,
        beforeEnter(to, from, next) {
          routeMiddleware(['Super Admin', 'VIP'], next)
        }
      },
      {
        path: 'kp-leaderboard',
        name: 'kp-leaderboard-page',
        component: require('./pages/KPLeaderboard.vue').default,
        beforeEnter(to, from, next) {
          routeMiddleware(['Super Admin', 'VIP'], next)
        }
      },
      {
        path: 'clients',
        name: 'clients-page',
        component: require('./pages/ClientsPage.vue').default,
        beforeEnter(to, from, next) {
          routeMiddleware(['Super Admin'], next)
        }
      },
      {
        path: 'committees',
        name: 'committees-page',
        component: require('./pages/CommitteesPage.vue').default,
        beforeEnter(to, from, next) {
          routeMiddleware(['Super Admin'], next)
        }
      },
      {
        path: 'user-sessions',
        name: 'user-sessions-page',
        component: require('./pages/UserSessionsPage.vue').default,
        beforeEnter(to, from, next) {
          routeMiddleware(['Super Admin'], next)
        }
      },
      {
        path: 'find-legislators',
        name: 'find-legislators-page',
        component: require('./pages/FindLegislatorsPage.vue').default,
        beforeEnter(to, from, next) {
          routeMiddleware(['Super Admin'], next)
        }
      },
      {
        path: 'users',
        name: 'users-page',
        component: require('./pages/UsersPage.vue').default,
        beforeEnter(to, from, next) {
          routeMiddleware(['Super Admin', 'VIP'], next)
        }
      },
      {
        path: 'sessions',
        name: 'sessions-page',
        component: require('./pages/SessionsPage.vue').default,
        beforeEnter(to, from, next) {
          routeMiddleware(['Super Admin', 'VIP'], next)
        }
      },
      {
        path: 'my-kps',
        name: 'my-kps-page',
        component: require('./pages/MyKPPage.vue').default,
        beforeEnter(to, from, next) {
          routeMiddleware(['VIP', 'Coordinator'], next)
        }
      },
      {
        path: 'members',
        name: 'members-page',
        component: require('./pages/MembersPage.vue').default,
        beforeEnter(to, from, next) {
          routeMiddleware(['Super Admin', 'VIP'], next)
        }
      },
      {
        path: 'legislators',
        name: 'legislators-page',
        component: require('./pages/LegislatorsPage.vue').default,
        beforeEnter(to, from, next) {
          routeMiddleware(['Super Admin', 'VIP'], next)
        }
      },
      {
        path: 'assignments',
        name: 'assignments-page',
        component: require('./pages/AssignmentsPage.vue').default,
        beforeEnter(to, from, next) {
          routeMiddleware(['Super Admin', 'VIP'], next)
        }
      },
      {
        path: 'dashboard',
        name: 'kp-tasks-page',
        component: require('./pages/KPTasksPage.vue').default,
        beforeEnter(to, from, next) {
          routeMiddleware(['Super Admin', 'VIP', 'Coordinator'], next)
        }
      },
      {
        path: 'district-lookup',
        name: 'district-lookup-page',
        component: require('./pages/DistrictLookupPage.vue').default,
        beforeEnter(to, from, next) {
          routeMiddleware(['Super Admin', 'VIP', 'Coordinator', 'KP'], next)
        }
      },
      {
        path: 'messages',
        name: 'messages-page',
        component: require('./pages/MessagesPage.vue').default,
        beforeEnter(to, from, next) {
          routeMiddleware(['VIP', 'Coordinator'], next)
        }
      },
      {
        path: 'advocacy',
        name: 'advocacy-page',
        component: require('./pages/AdvocacyPage.vue').default,
        beforeEnter(to, from, next) {
          routeMiddleware(['VIP'], next)
        }
      },
      {
        path: 'my-tasks',
        name: 'my-tasks-page',
        component: require('./pages/MyTasksPage.vue').default,
        beforeEnter(to, from, next) {
          routeMiddleware(['KP'], next)
        }
      },
      {
        path: 'assigned-tasks',
        name: 'assigned-tasks-page',
        component: require('./pages/AssignedTasksPage.vue').default,
        beforeEnter(to, from, next) {
          routeMiddleware(['KP', 'Member'], next)
        }
      },
      {
        path: 'how-to',
        name: 'how-to-page',
        component: require('./pages/HowToPage.vue').default,
        beforeEnter(to, from, next) {
          routeMiddleware(['Super Admin', 'VIP', 'Coordinator', 'KP'], next)
        }
      },
    ]
  },
  {
    path: '*',
    redirect: '/',
  }
]

const router = new VueRouter({
  mode: 'history',
  routes
})

export default router
