import moment from 'moment'

const state = {
  userData: null,
  refreshedAt: null,
}

const mutations = {
  setUserData (state, payload) {
    state.userData = payload
  },
  refreshSession (state) {
    state.refreshedAt = moment().format("DD/MM/YYYY HH:mm:ss")
  }
}

const actions = {
  setUserData (context, payload) {
    context.commit('setUserData', payload)
  },
  refreshSession (context) {
    context.commit('refreshSession')
  }
}

const getters = {
  userId: state => state.userData ? state.userData.id : false,
  userClientId: state => state.userData ? state.userData.client_id : false,
  userClient: state => state.userData ? state.userData.client : null,
  userName: state => state.userData ? `${state.userData.firstname} ${state.userData.lastname}` : '',
  userEmail: state => state.userData ? state.userData.email : '',
  userRole: state => state.userData ? state.userData.role : '',
  userMailingAddress: state => state.userData ? state.userData.mailing_address : '',
  userCellPhone: state => state.userData ? state.userData.cell_phone : '',
  userPermissions: state => state.userData ? state.userData.permissions : [],
  userAuthyId: state => state.userData ? state.userData.twilio_authy_id : [],
  userTwofactorEnabled: state => state.userData ? state.userData.two_factor_verified : [],
}

export default {
  namespaced: true,
  state,
  mutations,
  actions,
  getters
}
