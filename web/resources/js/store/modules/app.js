const state = {
  clientStatus: {},
  isMenuSidebarOpen: null,
  isProfileSidebarOpen: null,
  isClientSettingsSidebarOpen: null,
  unseenMessageCount: 0,

  //Tariq
  selectedModalDistrict : 'xc',
  districtModalEnabled : false

}

const mutations = {
  setClientStatus (state, payload) {
    state.clientStatus = payload
  },
  setMenuSidebarOpen (state, payload) {
    state.isMenuSidebarOpen = payload
  },
  setProfileSidebarOpen (state, payload) {
    state.isProfileSidebarOpen = payload
  },
  setClientSettingsSidebarOpen (state, payload) {
    state.isClientSettingsSidebarOpen = payload
  },
  setUnseenMessageCount (state, payload) {
    state.unseenMessageCount = payload
  },

  setSelectedModalDistrict (state, payload) {
    state.selectedModalDistrict = payload.district
  },
  setDistrictModalEnabled (state, payload) {
    state.districtModalEnabled = payload.status
  }

}

const actions = {
  setClientStatus (context, payload) {
    context.commit('setClientStatus', payload)
  },
  setMenuSidebarOpen (context, payload) {
    context.commit('setMenuSidebarOpen', payload)
  },
  setProfileSidebarOpen (context, payload) {
    context.commit('setProfileSidebarOpen', payload)
  },
  setClientSettingsSidebarOpen (context, payload) {
    context.commit('setClientSettingsSidebarOpen', payload)
  },
  setUnseenMessageCount (context, payload) {
    context.commit('setUnseenMessageCount', payload)
  },


  setSelectedModalDistrict (context, payload) {
    context.commit('setSelectedModalDistrict', payload)
  },
  setDistrictModalEnabled (context, payload) {
    context.commit('setDistrictModalEnabled', payload)
  },
}

const getters = {
  getSSS:(state)=>{
   return state.selectedModalDistrict
  }
}

export default {
  namespaced: true,
  state,
  mutations,
  actions,
  getters
}
