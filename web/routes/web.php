<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/ez/{token}', 'Auth\LoginController@showLoginAsMemberPage');
Route::post('/login-as-member', 'Auth\LoginController@loginAsMember');
Route::post('/verifyTwoFactorCode', 'Auth\LoginController@verifyTwoFactorCode');

Route::get('password/email', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('password.email');
Route::post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail');

Route::post('password/reset', 'Auth\ResetPasswordController@reset');
Route::post('sendKPLoginLink', 'Auth\LoginController@sendKPLoginLink');

Route::get('register/{token}', 'Auth\RegisterController@index');
Route::post('register', 'Auth\RegisterController@register');
Route::get('/', 'HomeController@index');
