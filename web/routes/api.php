<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('/mailgun/notify', 'Api\MailgunController@notify');
Route::post('/mailgun/permanent-failure', 'Api\MailgunController@permanentFailure');
Route::post('/mailgun/upload', 'Api\MailgunController@upload');
Route::post('/twilio/notify', 'Api\TwilioController@notify');

Route::middleware('auth:api')->group(function() {
    Route::post('/app/formatted-message', 'Api\AppController@getFormattedMessage');

    Route::get('/clients/{client}/users', 'Api\ClientController@getUsers');
    // Route::get('/clients/{client}/districts', 'Api\ClientController@getDistricts');
    Route::get('/clients/{client}/sessions', 'Api\ClientController@getSessions');
    Route::get('/clients/{client}/active-session', 'Api\ClientController@getActiveSession');
    Route::get('/clients/{client}/members', 'Api\ClientController@getMembers');
    Route::get('/clients/{client}/legislators', 'Api\ClientController@getLegislators');
    Route::get('/clients/{client}/assignments', 'Api\ClientController@getAssignments');
    Route::get('/clients/{client}/kp-tasks', 'Api\ClientController@getKPTasks');
    Route::post('/clients/{client}/disable', 'Api\ClientController@disable');
    Route::post('/clients/{client}/enable', 'Api\ClientController@enable');
    Route::get('/clients/{client}/message-members', 'Api\ClientController@getMessageMembers');
    Route::get('/clients/{client}/messages', 'Api\ClientController@getMessages');
    Route::get('/clients/{client}/unseen-message-count', 'Api\ClientController@getUnseenMessageCount');
    Route::post('/clients/{client}/send-email', 'Api\ClientController@sendEmail');
    Route::post('/clients/{client}/send-text', 'Api\ClientController@sendText');
    Route::post('/clients/{client}/import-members', 'Api\ClientController@importMembers');
    Route::get('/clients/{client}/status', 'Api\ClientController@getStatus');
    Route::get('/clients/{client}/advocacy-messages', 'Api\ClientController@getAdvocacyMessages');
    Route::get('/clients/{client}/user-sessions', 'Api\ClientController@getUserSessions');
    Route::post('/clients/{client}/update-districts', 'Api\ClientController@updateDistricts');
    Route::get('/clients/{client}/kp-leaderboard-data', 'Api\ClientController@getKPLeaderboardData');
    Route::get('/clients/{client}/my-kps', 'Api\ClientController@getMyKPs');

    Route::post('/districts/search-by-address', 'Api\DistrictController@searchDistrictByAddress');

    Route::get('/sessions/{session}/kp-tasks', 'Api\SessionController@getKPTasks');
    Route::get('/sessions/{session}/tasks', 'Api\SessionController@getTasks');
    Route::post('/sessions/{session}/tasks', 'Api\SessionController@addTasks');
    Route::get('/sessions/{session}/dashboard-data', 'Api\SessionController@getDashboardData');

    Route::get('/states/{state}/legislators', 'Api\StateController@getLegislators');
    Route::get('/states/{state}/unelected-districts', 'Api\StateController@getUnelectedDistricts');
    Route::get('/states/{state}/new-legislators', 'Api\StateController@getNewLegislators');
    Route::get('/states/{state}/committees', 'Api\StateController@getCommittees');

    Route::get('/legislators/{legislator}/vote-history', 'Api\LegislatorController@getVoteHistory');
    Route::get('/legislators/{legislator}/kp-history', 'Api\LegislatorController@getKPHistory');
    Route::get('/legislators/{legislator}/donation-history', 'Api\LegislatorController@getDonationHistory');
    Route::post('/legislators/{legislator}/data', 'Api\LegislatorController@addLegData');
    Route::put('/legislators/{legislator}/data', 'Api\LegislatorController@updateLegData');
    Route::post('/legislators/{legislator}/kp', 'Api\LegislatorController@addKP');
    Route::post('/legislators/import-from-google', 'Api\LegislatorController@importFromGoogle');
    Route::post('/legislators/replace-with-new', 'Api\LegislatorController@replaceWithNew');
    Route::post('/legislators/ignore', 'Api\LegislatorController@ignore');

    Route::get('/members/{member}/kp-history', 'Api\MemberController@getKPHistory');
    // Route::post('/members/search', 'Api\MemberController@search');
    Route::get('/members/{member}/assigned-legislators', 'Api\MemberController@getAssignedLegislators');
    Route::get('/members/{member}/advocacy-message-history', 'Api\MemberController@getAdvocacyMessageHistory');

    Route::get('/tasks/common', 'Api\TaskController@getCommonTasks');
    Route::post('/tasks/{task}/result', 'Api\TaskController@addTaskResult');

    Route::post('/users/{user}/saw-message', 'Api\UserController@sawMessage');
    Route::post('/users/verify-password', 'Api\UserController@verifyPassword');
    Route::post('/users/verify-phone', 'Api\UserController@sendSMSVerificationCode');
    Route::post('/users/verify-code', 'Api\UserController@verifyTwoFactorCode');
    Route::post('/users/verify-user-login-two-factor', 'Api\UserController@verifyLoginTwoFactorCode');
    Route::post('/users/disable-2fa', 'Api\UserController@disableTwoFactor');
    Route::post('/users/send-two-factor-deactivate-code', 'Api\UserController@sendDeactivateCode');
    Route::post('/users/deactivate-two-factor-code', 'Api\UserController@deactivateTwoFactorCode');
    Route::apiResources([
        'states' => 'Api\StateController',
        'occupations' => 'Api\OccupationController',
        // 'clients' => 'Api\ClientController',
        'committees' => 'Api\CommitteeController',
        'sessions' => 'Api\SessionController',
        'members' => 'Api\MemberController',
        'legislators' => 'Api\LegislatorController',
        'tasks' => 'Api\TaskController',
        'users' => 'Api\UserController',
        'user-sessions' => 'Api\UserSessionController',
    ]);
});


Route::apiResources([
    'clients' => 'Api\ClientController',
    'advocacy-messages' => 'Api\AdvocacyMessageController',
]);
Route::get('/clients/{client}/districts', 'Api\ClientController@getDistricts');

Route::get('/members/{member}/tasks', 'Api\MemberController@getKPTasks');
Route::get('/members/{member}/advocacy-messages', 'Api\MemberController@getAdvocacyMessages');
Route::post('/members/{member}/task-results', 'Api\MemberController@addKPTaskResults');
Route::post('/members/search-by-district', 'Api\MemberController@searchByDistrict');

Route::get('/advocacy-messages/{advocacyMessage}/remaining-legislators', 'Api\AdvocacyMessageController@getRemainingLegislators');
Route::get('/advocacy-messages/{advocacyMessage}/formatted-subject', 'Api\AdvocacyMessageController@getFormattedSubject');
Route::get('/advocacy-messages/{advocacyMessage}/formatted-message', 'Api\AdvocacyMessageController@getFormattedMessage');
Route::get('/advocacy-messages/{advocacyMessage}/stats', 'Api\AdvocacyMessageController@getStats');
Route::get('/advocacy-messages/{advocacyMessage}/reply-stats', 'Api\AdvocacyMessageController@getReplyStats');
Route::post('/advocacy-messages/search-members', 'Api\AdvocacyMessageController@searchMembers');
Route::post('/advocacy-messages/{advocacyMessage}/send', 'Api\AdvocacyMessageController@send');
Route::post('/advocacy-messages/{advocacyMessage}/remind', 'Api\AdvocacyMessageController@remind');
Route::post('/advocacy-messages/{advocacyMessage}/send-reply', 'Api\AdvocacyMessageController@sendReply');
