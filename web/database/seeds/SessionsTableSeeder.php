<?php

use Illuminate\Database\Seeder;

class SessionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $sql = "
            INSERT INTO `sessions`
                (`client_id`, `name`, `notes`, `start_at`, `end_at`)
            VALUES
                (1, '2019', '2019', '2019-01-01', '2019-06-09');
        ";
        DB::connection()->getPdo()->exec($sql);
    }
}
