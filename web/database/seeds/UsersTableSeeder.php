<?php

use Illuminate\Database\Seeder;
use App\Models\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user1 = User::create([
            'firstname' => 'Adam',
            'lastname' => 'Parker',
            'nickname' => 'Adam',
            'email' => 'adamparkerod@gmail.com',
            'password' => bcrypt('changeme'),
        ]);
        $user1->assignRole('Super Admin');

        $user2 = User::create([
            'client_id' => 1,
            'firstname' => 'Noah',
            'lastname' => 'Hinrichs',
            'nickname' => 'Noah',
            'email' => 'elite.developer1055@gmail.com',
            'password' => bcrypt('changeme'),
        ]);
        $user2->assignRole('VIP');
    }
}
