<?php

use Illuminate\Database\Seeder;
use App\Models\State;

class StatesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $sql = "
            INSERT INTO `states` 
                (`id`, `state`, `state_abbrev`, `district_total`, `district_label_s`, `district_label_p`, `house_districts`, `sen_districts`, `house_health_comm_name`, `sen_health_comm_name`, `house_sub_comm`, `sen_sub_comm`, `are_there_sub_comms`)
            VALUES
                (1, 'Nebraska', 'ne', 49, 'District', 'Districts', 0, '49', '', 'Health and Human Services Committee', '', '', 0),
                (2, 'Louisiana', 'la', 144, 'Parish', 'Parishes', 105, '39', 'Health and Welfare Committee', 'Health and Welfare Committee', '', '', 1),
                (3, 'Virginia', 'va', 140, 'District', 'Districts', 100, '40', 'Health, Welfare and Institutions Committee', 'Education and Health Committee', 'Health, Welfare and Institutions Subcommittee', 'Health Professions Committee', 1);
        ";
        DB::connection()->getPdo()->exec($sql);
    }
}
