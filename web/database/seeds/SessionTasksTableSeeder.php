<?php

use Illuminate\Database\Seeder;

class SessionTasksTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $sql = "
            INSERT INTO `session_tasks`
                (`session_id`, `task_type_id`, `order`, `name`, `tooltip`, `notes`, `deadline`)
            VALUES
                (0, 1, null, 'Contact', 'Did KP make initial contact?', '', null),
                (0, 1, null, 'Meet1', 'Did KP have initial meeting?', '', null),
                (0, 1, null, 'Meet2', 'Did KP have a second meeting?', '', null),
                (0, 4, null, 'KP1 Donations', 'KP1\'s personal donations to this legislator', '', null),
                (0, 4, null, 'Other Donations', 'Donations from other sources other than KP1 or PAC such as other members', '', null),
                (0, 4, null, 'PAC Donations', 'Total PAC Donations', '', null),
                (0, 5, null, 'P-Vote', 'Predicted vote from this legislator', '', null),
                (0, 6, null, 'F-Vote', 'Final Vote from this legislator', '', null),
                (1, 3, null, 'Notes', '', '', null),
                (1, 1, null, 'Contact', 'Did KP make initial contact with legislator?', '', '2019-06-09'),
                (1, 1, null, 'Meet1', 'Did KP meet to discuss bill and ask for commitmenton vote?', '', '2019-06-09'),
                (1, 1, null, 'Followup', 'Did KP follow up after initial contact to ask for commitmenton vote?', '', '2019-06-09'),
                (1, 1, null, 'Invite', 'Did KP personally invite legislator to NOA legislative luncheaon?', '', '2019-06-09'),
                (1, 4, null, 'Donations', 'Total donatiosn to this legislator', '', '2019-06-09'),
                (1, 1, null, 'OS-Contact', 'Did KP make off-season contact for friendly conversation?', '', '2019-06-09'),
                (1, 5, null, 'P-Vote', 'Predicted vote from this legislator?', '', '2019-06-09'),
                (1, 6, null, 'F-Vote', 'Final Vote from this legislator?', '', '2019-06-09');
        ";
        DB::connection()->getPdo()->exec($sql);
    }
}
