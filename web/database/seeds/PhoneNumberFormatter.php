<?php

use Illuminate\Database\Seeder;
use App\Models\Client;
use App\Models\Legislator;
use App\Models\Member;
use App\Models\Message;
use App\Models\User;


class PhoneNumberFormatter extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $clients = Client::get();
        foreach($clients as $client) {
            $client->save();
        }
        $legislators = Legislator::get();
        foreach($legislators as $legislator) {
            $legislator->save();
        }
        $members = Member::get();
        foreach($members as $member) {
            $member->save();
        }
        $messages = Message::get();
        foreach($messages as $message) {
            if ($message->is_sms) {
                $message->save();
            }
        }
        $users = User::get();
        foreach($users as $user) {
            $user->save();
        }
    }
}
