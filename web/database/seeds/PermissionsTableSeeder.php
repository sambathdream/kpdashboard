<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;

class PermissionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Permission::create(['name' => 'manage clients']);
        Permission::create(['name' => 'manage users']);
        Permission::create(['name' => 'manage members']);
        Permission::create(['name' => 'manage own users']);
        Permission::create(['name' => 'manage own members']);
        Permission::create(['name' => 'view own members']);
    }
}
