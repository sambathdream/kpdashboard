<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Cache;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Cache::forget('spatie.permission.cache');

        $super_admin = Role::create(['name' => 'Super Admin']);
    
        $vip = Role::create(['name' => 'VIP']);
    
        $coordinator = Role::create(['name' => 'Coordinator']);
    
        $user = Role::create(['name' => 'KP']);
    }
}
