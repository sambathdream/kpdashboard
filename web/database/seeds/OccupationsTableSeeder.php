<?php

use Illuminate\Database\Seeder;
use App\Models\Occupation;

class OccupationsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Occupation::truncate();
        $occupationList = file_get_contents('database/seeds/occupations.txt');
        $occupationList = json_decode($occupationList);
        foreach ($occupationList as $occupation) {
            Occupation::create(['name' => $occupation]);
        }
    }
}
