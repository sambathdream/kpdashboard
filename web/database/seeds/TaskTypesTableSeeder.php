<?php

use Illuminate\Database\Seeder;

class TaskTypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $sql = "
            INSERT INTO `task_types`
                (`client_id`, `name`, `data_type`, `options`)
            VALUES
                (0, 'Yes/No', 'Boolean', '[]'),
                (0, 'String', 'String', '[]'),
                (0, 'Notes', 'Notes', '[]'),
                (0, 'Currency', 'Currency', '[]'),
                (1, 'Select', 'Select', '[\"Supporting\", \"Inclined to Support\", \"Neutral/Unknown\", \"Inclined to Oppose\", \"Opposing\"]'),
                (1, 'Select', 'Select', '[\"Yea\", \"Nay\", \"Abstain\"]');
        ";
        DB::connection()->getPdo()->exec($sql);
    }
}
