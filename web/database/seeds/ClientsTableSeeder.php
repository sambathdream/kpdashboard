<?php

use Illuminate\Database\Seeder;

class ClientsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $sql = "
            INSERT INTO `clients`
                (`id`, `active`, `association`, `assoc_abbrev`, `profession`, `reply_to_email`, `state_id`, `uses_regions`, `uses_coordinators`, `uses_kps`, `uses_2nd_work`, `uses_3rd_work`, `uses_4th_work`)
            VALUES
                (1, 1, 'Nebraska Optometric Association', 'NOA', 'Optometry', 'info@noalegislative.net', 1, 0, 0, 5, 1, 1, 1);
        ";
        DB::connection()->getPdo()->exec($sql);
    }
}
