<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
            PermissionsTableSeeder::class,
            RolesTableSeeder::class,
            StatesTableSeeder::class,
            OccupationsTableSeeder::class,
            DistrictsTableSeeder::class,
            ClientsTableSeeder::class,
            UsersTableSeeder::class,
            MembersTableSeeder::class,
            LegislatorsTableSeeder::class,
            TaskTypesTableSeeder::class,
            SessionsTableSeeder::class,
            SessionTasksTableSeeder::class,
        ]);
    }
}
