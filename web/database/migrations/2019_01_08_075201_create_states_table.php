<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('states', function (Blueprint $table) {
            $table->increments('id');
            $table->string('state');
            $table->string('state_abbrev');
            $table->unsignedInteger('district_total')->default(0);
            $table->string('district_label_s')->default('District');
            $table->string('district_label_p')->nullable();
            $table->unsignedInteger('house_districts')->default(0);
            $table->string('sen_districts')->nullable();
            $table->boolean('has_subcommittees')->default(false);
            $table->boolean('has_joint_committees')->default(true);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('states');
    }
}
