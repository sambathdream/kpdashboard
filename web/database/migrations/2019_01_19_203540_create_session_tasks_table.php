<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSessionTasksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('session_tasks', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('session_id');
            $table->unsignedInteger('task_type_id');
            $table->tinyInteger('order')->nullable();
            $table->string('name');
            $table->string('tooltip')->nullable();
            $table->string('notes', 5000)->nullable();
            $table->date('deadline')->nullable();
            $table->boolean('hide_from_kp')->default(false);
            $table->timestamps();
            $table->softDeletes();

            // $table->foreign('session_id')->references('id')->on('sessions')->onDelete('CASCADE')->onUpdate('RESTRICT');
            $table->foreign('task_type_id')->references('id')->on('task_types')->onDelete('CASCADE')->onUpdate('RESTRICT');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('session_tasks');
    }
}
