<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateImportedLegislatorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('imported_legislators', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('state_id')->nullable();
            $table->unsignedInteger('district_id')->nullable();
            $table->boolean('is_federal')->default(false);
            // General
            $table->string('firstname');
            $table->string('lastname');
            $table->string('nickname');
            $table->string('fullname');
            $table->string('party')->nullable();
            // Official
            $table->string('official_email')->nullable();
            $table->string('official_phone')->nullable();
            $table->string('official_fax')->nullable();
            $table->string('official_address')->nullable();
            $table->string('state_room')->nullable();
            $table->string('aide_name')->nullable();
            $table->string('official_image', 500)->nullable();
            // Local
            $table->string('occupation')->nullable();
            $table->string('personal_email')->nullable();
            $table->string('local_phone')->nullable();
            $table->string('local_address')->nullable();
            // Dates
            $table->date('elected_at')->nullable();
            $table->date('abdicated_at')->nullable();
            // Social
            $table->string('website_url', 500)->nullable();
            $table->string('biography_url', 500)->nullable();
            $table->string('ballotpedia_url', 500)->nullable();
            $table->string('campaign_url', 500)->nullable();
            $table->string('twitter_url', 500)->nullable();
            $table->string('facebook_url', 500)->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('imported_legislators');
    }
}
