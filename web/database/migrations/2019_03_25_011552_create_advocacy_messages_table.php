<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdvocacyMessagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('advocacy_messages', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('client_id');
            $table->string('subject');
            $table->text('message', 102400);
            $table->json('legislators');
            $table->json('members');
            $table->boolean('is_kp_only')->default(false);
            $table->dateTime('sent_at')->nullable();
            $table->boolean('active')->default(true);
            $table->date('deadline')->nullable();
            $table->string('invitation_subject');
            $table->text('invitation_message', 102400);
            $table->boolean('via_sms')->default(false);
            $table->timestamps();

            $table->foreign('client_id')->references('id')->on('clients')->onDelete('CASCADE')->onUpdate('RESTRICT');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('session_tasks');
    }
}
