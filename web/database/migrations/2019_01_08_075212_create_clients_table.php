<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClientsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('clients', function (Blueprint $table) {
            $table->increments('id');
            $table->boolean('active')->default(true);
            $table->string('association');
            $table->string('assoc_abbrev');
            $table->string('avatar_url', 2084)->nullable();
            $table->string('profession')->nullable();
		    $table->boolean('is_forward_email_required')->default(false);
		    $table->string('reply_to_email')->nullable();
		    $table->string('mailgun_email')->nullable();
		    $table->string('forward_email')->nullable();
		    $table->boolean('is_forward_text_required')->default(false);
            $table->string('twilio_number')->nullable();
            $table->string('forward_number')->nullable();
            $table->unsignedInteger('state_id');
            $table->boolean('uses_regions')->default(false);
            $table->tinyInteger('region_count')->default(0);
            $table->boolean('uses_coordinators')->default(false);
		    $table->boolean('show_all')->default(false);
            $table->tinyInteger('uses_kps')->nullable();
            $table->boolean('uses_dob')->default(false);
            $table->boolean('uses_grad_year')->default(false);
            $table->boolean('uses_2nd_work')->default(false);
            $table->boolean('uses_3rd_work')->default(false);
            $table->boolean('uses_4th_work')->default(false);
            $table->string('formal_house')->nullable();
            $table->string('formal_senate')->nullable();
            $table->json('house_committees');
            $table->json('senate_committees');
            $table->json('joint_committees');
            $table->timestamps();

            $table->foreign('state_id')->references('id')->on('states')->onDelete('CASCADE')->onUpdate('RESTRICT');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('clients');
    }
}
