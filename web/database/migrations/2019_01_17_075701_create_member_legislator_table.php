<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMemberLegislatorTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('member_legislator', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('leg_id');
            $table->unsignedInteger('client_id');
            $table->unsignedInteger('member_id');
            $table->tinyInteger('kp_position');
            $table->json('kp_relationship');
            $table->string('notes', 5000)->nullable();
            $table->date('start_date')->nullable();
            $table->date('end_date')->nullable();
            $table->timestamps();

            $table->foreign('leg_id')->references('id')->on('legislators')->onDelete('CASCADE')->onUpdate('RESTRICT');
            $table->foreign('client_id')->references('id')->on('clients')->onDelete('CASCADE')->onUpdate('RESTRICT');
            $table->foreign('member_id')->references('id')->on('members')->onDelete('CASCADE')->onUpdate('RESTRICT');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('member_legislator');
    }
}
