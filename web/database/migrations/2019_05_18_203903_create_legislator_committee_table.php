<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLegislatorCommitteeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('legislator_committee', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('legislator_id');
            $table->unsignedInteger('committee_id');
            $table->json('memberships');
            $table->timestamps();

            $table->foreign('legislator_id')->references('id')->on('legislators')->onDelete('CASCADE')->onUpdate('RESTRICT');
            $table->foreign('committee_id')->references('id')->on('committees')->onDelete('CASCADE')->onUpdate('RESTRICT');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('legislator_committee');
    }
}
