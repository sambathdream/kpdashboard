<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdvocacyMemberMessagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('advocacy_member_messages', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('advocacy_message_id');
            $table->unsignedInteger('legislator_id');
            $table->unsignedInteger('member_id');
            $table->string('subject');
            $table->text('message', 102400);
            $table->timestamps();

            $table->foreign('advocacy_message_id')->references('id')->on('advocacy_messages')->onDelete('CASCADE')->onUpdate('RESTRICT');
            $table->foreign('legislator_id')->references('id')->on('legislators')->onDelete('CASCADE')->onUpdate('RESTRICT');
            $table->foreign('member_id')->references('id')->on('members')->onDelete('CASCADE')->onUpdate('RESTRICT');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('advocacy_member_messages');
    }
}
