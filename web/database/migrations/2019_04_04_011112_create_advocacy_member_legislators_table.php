<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdvocacyMemberLegislatorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('advocacy_member_legislators', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('advocacy_message_id');
            $table->unsignedInteger('member_id');
            $table->json('legislators');
            $table->timestamps();

            $table->foreign('advocacy_message_id')->references('id')->on('advocacy_messages')->onDelete('CASCADE')->onUpdate('RESTRICT');
            $table->foreign('member_id')->references('id')->on('members')->onDelete('CASCADE')->onUpdate('RESTRICT');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('advocacy_member_legislators');
    }
}
