<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMembersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('members', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('client_id');
            $table->boolean('active')->default(false);
            $table->string('firstname');
            $table->string('lastname');
            $table->string('nickname');
            $table->string('cell')->nullable();
            $table->string('office_phone')->nullable();
            $table->string('email');
            $table->string('avatar_url', 2084)->nullable();
            $table->boolean('eligible')->default(false);
            $table->integer('coordinator')->nullable();
            $table->date('dob')->nullable();
            $table->integer('grad_year')->nullable();
            $table->string('notes')->nullable();
            $table->string('home_address')->nullable();
            $table->float('home_lat', 10, 6)->nullable();
            $table->float('home_lng', 10, 6)->nullable();
            $table->string('home_house_district')->nullable();
            $table->string('home_sen_district')->nullable();
            $table->string('home_con_district')->nullable();
            $table->string('home_federal_senate')->nullable();
            $table->string('work_address')->nullable();
            $table->float('work_lat', 10, 6)->nullable();
            $table->float('work_lng', 10, 6)->nullable();
            $table->string('work_house_district')->nullable();
            $table->string('work_sen_district')->nullable();
            $table->string('work_con_district')->nullable();
            $table->string('work_federal_senate')->nullable();
            $table->string('work2_address')->nullable();
            $table->float('work2_lat', 10, 6)->nullable();
            $table->float('work2_lng', 10, 6)->nullable();
            $table->string('work2_house_district')->nullable();
            $table->string('work2_sen_district')->nullable();
            $table->string('work2_con_district')->nullable();
            $table->string('work2_federal_senate')->nullable();
            $table->string('work3_address')->nullable();
            $table->float('work3_lat', 10, 6)->nullable();
            $table->float('work3_lng', 10, 6)->nullable();
            $table->string('work3_house_district')->nullable();
            $table->string('work3_sen_district')->nullable();
            $table->string('work3_con_district')->nullable();
            $table->string('work3_federal_senate')->nullable();
            $table->string('work4_address')->nullable();
            $table->float('work4_lat', 10, 6)->nullable();
            $table->float('work4_lng', 10, 6)->nullable();
            $table->string('work4_house_district')->nullable();
            $table->string('work4_sen_district')->nullable();
            $table->string('work4_con_district')->nullable();
            $table->string('work4_federal_senate')->nullable();
		    $table->string('token')->nullable();
		    $table->string('password')->nullable();
		    $table->dateTime('token_used_at')->nullable();
            $table->timestamps();

            $table->foreign('client_id')->references('id')->on('clients')->onDelete('CASCADE')->onUpdate('RESTRICT');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('members');
    }
}
