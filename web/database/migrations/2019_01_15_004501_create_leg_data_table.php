<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLegDataTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('leg_data', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('leg_id')->nullable();
            $table->unsignedInteger('client_id')->nullable();
            $table->text('vip_notes', 10240)->nullable();
            $table->text('leg_notes', 102400)->nullable();
            $table->tinyInteger('leg_support')->nullable();
            $table->tinyInteger('leg_region')->nullable();
            $table->timestamps();

            $table->foreign('leg_id')->references('id')->on('legislators')->onDelete('CASCADE')->onUpdate('RESTRICT');
            $table->foreign('client_id')->references('id')->on('clients')->onDelete('CASCADE')->onUpdate('RESTRICT');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('leg_data');
    }
}
