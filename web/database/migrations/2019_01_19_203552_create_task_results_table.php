<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTaskResultsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('task_results', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('task_id');
            $table->unsignedInteger('legislator_id');
            $table->unsignedInteger('kp_id')->nullable();
            $table->string('result', 5000)->nullable();
            $table->timestamps();

            $table->foreign('task_id')->references('id')->on('session_tasks')->onDelete('CASCADE')->onUpdate('RESTRICT');
            $table->foreign('legislator_id')->references('id')->on('legislators')->onDelete('CASCADE')->onUpdate('RESTRICT');
            $table->foreign('kp_id')->references('id')->on('members')->onDelete('CASCADE')->onUpdate('RESTRICT');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('task_results');
    }
}
