# KP Dashboard v2

## Getting started

```
# Install Docker
https://docs.docker.com/install/

# Dowload AWS CLI
Linux: https://docs.aws.amazon.com/cli/latest/userguide/install-linux.html
Windows: https://docs.aws.amazon.com/cli/latest/userguide/install-windows.html
Mac: https://docs.aws.amazon.com/cli/latest/userguide/install-macos.html

# Configure AWS CLI. Run below command into terminal
aws configure
# It will ask below information
  AWS Access Key ID [None]: AKIAUJQ2D7OH7CVCZ5N5
  AWS Secret Access Key [None]: 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
  Default region name [None]: us-east-1
  Default output format [None]: json

# Login into docker repository ( ECR )
# Run this command and you will get docker login full coammnd. That that output again.
aws ecr get-login --region us-east-1 --no-include-email

git clone https://gitlab.com/tariqul/kp_dashboard_v2
cd kp_dashboard_v2
docker-compose -f docker-compose-local.yml up

# Login into php container
docker exec -it kp_dashboard_php_1 /bin/bash

# Run the following command
composer install
npm install
php artisan key:generate
php artisan migrate
php artisan db:seed
yarn
yarn prod


# PHPMyadmin
# Please check .env file for get/set user and password
http://localhost:8080

# App
http://localhost

npm run dev
# or
npm run watch
# or for a production build of assets
npm run prod
# see Laravel Mix documentation
```
