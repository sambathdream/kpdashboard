#!/bin/sh

envsubst '$NGINX_HOST,$NGINX_PORT' < /kpdashboard.template > /etc/nginx/conf.d/default.conf

exec "$@"
