FROM php:7.3-fpm

# Copy composer.lock and composer.json
COPY ./web/composer.lock ./web/composer.json /var/www/
# Set working directory
WORKDIR /var/www

# Install Nodejs repo
RUN curl -sL https://deb.nodesource.com/setup_12.x | bash -
# Install Yarn
RUN curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | apt-key add -
RUN echo "deb https://dl.yarnpkg.com/debian/ stable main" | tee /etc/apt/sources.list.d/yarn.list
RUN apt-get update && apt-get install -y \
        wget \
        git \
        yarn \
        unzip \
        mariadb-client \
        nodejs \
        libjpeg-dev \
        libpng-dev \
        libfreetype6-dev \
        libxml2 \
        libzip-dev \
        libxml2-dev \
        libc-client-dev \
        libkrb5-dev \
        libgmp-dev \
        gettext-base \
    && docker-php-ext-configure gd --with-freetype-dir=/usr/include/ --with-jpeg-dir=/usr/include/ \
    && docker-php-ext-configure imap --with-kerberos --with-imap-ssl \
    && docker-php-ext-install gd pdo_mysql zip soap imap gmp

# Install composer
ENV COMPOSER_HOME /composer
ENV PATH ./vendor/bin:/composer/vendor/bin:$PATH
ENV COMPOSER_ALLOW_SUPERUSER 1
RUN curl -s https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin/ --filename=composer
# Add user for laravel application
RUN groupadd -g 1000 www
RUN useradd -u 1000 -ms /bin/bash -g www www

RUN rm -rf /var/lib/apt/lists/*
COPY ./php.ini $PHP_INI_DIR/php.ini
# Copy existing application directory contents
COPY ./web /var/www
RUN chown www.www /var/www -R

# Change current user to www
USER www
# Expose port 9000 and start php-fpm server
EXPOSE 9000
CMD ["php-fpm"]
